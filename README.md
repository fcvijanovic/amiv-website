# amiv Website

This is the home of the amiv website.

## 🚀 Quick start

1. **Development**

   Navigate into the project directory, install all dependencies and run the development server using the following commands:

   ```shell
   yarn install
   yarn run dev
   ```

   You can visit the website now at http://localhost:3000

2. **Development with Devcontainer (VSCode)**

   You can run the prepared development environment with docker and VSCode without the need to install anything project-specific on your system.

   Requirements:

   * *(Windows only)* [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/)
   * [Docker Engine](https://docs.docker.com/engine/install/)
   * [Visual Studio Code](https://code.visualstudio.com/)(VSCode)
   * [Remote Development Extension Pack for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)

   Open the project directory with the prepared devcontainer by clicking the green button in the left button corner of the VSCode window and select `Remote-Containers: Open Folder in Container...`.

   On startup, `yarn install` is run automatically. You can start the gatsby development process with `yarn run dev`.

   You can visit the website now at http://localhost:3000

   **IMPORTANT:** You also have to run the backend project on your machine! It is not possible to use an externally hosted backend.

3. **Linting & Formatting**

   We use `eslint` and `prettier` for linting and formatting of the javascript code. You can use the following commands:

   ```shell
   yarn run eslint
   yarn run format
   ```

4. **Production Build**

   You can prepare a production build manually.

   ```shell
   yarn run build
   ```

   For more information about deployment, read the section about deployment below.

## 🧐 What's inside?

A quick look at the most important files and directories of the project.

```ascii
.
├── next.config.js
├── middleware.js
├── config.js
├── content/
├── public/
├── locales/
├── pages/
└── lib
    ├── components
    ├── content
    ├── context
    ├── hooks
    ├── images
    ├── intl
    ├── pages
    ├── store
    ├── utils
    └── theme.js
```

1. **`next.config.js`**: This file is where Next.js is looking for its configuration. See [next.config.js Reference](https://nextjs.org/docs/pages/api-reference/next-config-js).

2. **`middleware.js`**: This file allows to run code before a request is completed. See [Middleware Reference](https://nextjs.org/docs/app/building-your-application/routing/middleware).

3. **`config.js`**: This file is where the configuration of the website project is located.

4. **`/content`**: This directory contains Markdown and image files used within the project. The files are either referenced directly or loaded while building the project dynamically.

5. **`/public`**: All files within this directory are directly copied to the build output directory.

6. **`/locales`**: Translation files used by `react-intl`.

7. **`/pages`**: Every `js` file represents a page which path is exactly like the folder structure and filename.

      **Note**: Every file starting with an underscore (`_`) in the pages directory is ignored. Those pages are mostly being used for client-only routes. Therefore no page route is created and no server-side rendering performed.

      _Example: The file `/src/pages/amiv/about.js` will serve the component defined in that file as a page at the path `/amiv/about`._

8. **`/lib`**: This directory contains all source code.

   1. **`/lib/components`**: Contains all components used somewhere on the website.
   2. **`/lib/context`**: Custom [React Context](https://reactjs.org/docs/context.html)
   3. **`/lib/hooks`**: Custom React hooks.
   4. **`/lib/icons`**: Custom icon components.
   5. **`/lib/store`**: This directory contains all files related to data handling using `react-redux`.
   6. **`/lib/utils`**: Collection of utility functions.
   7. **`/lib/theme.js`**: Theme definition for light/dark mode (dimensions, colors, etc.)

## 💫 Deployment

TODO: Add information about the deployment process of the project.

## ⚙ Technologies

### Frontend Frameworks & Libraries

- [Next.JS](https://nextjs.org/)
- [React](https://reactjs.org/)
- [react-intl](https://github.com/formatjs/react-intl)
- [React-Redux](https://react-redux.js.org/)

### Backend

- [AMIV API](https://github.com/amiv-eth/amivapi)

### Build Tools

- [yarn](https://yarnpkg.com/)

### Development Tools

- [ESlint](https://github.com/eslint/eslint)
- [Prettier](https://github.com/prettier/prettier)

  Most IDEs have plugins for those tools. VS Code is the recommended IDE.
