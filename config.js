/** Public website runtime configuration */

const dateFormatOptions = {
  hour: 'numeric',
  minute: 'numeric',
  hour12: false,
  timeZone: 'Europe/Zurich',
}

module.exports = {
  // RuntimeConfig should not be loaded directly from this file!
  // Use getPublicConfig from lib/utils/config instead.
  runtimeConfig: {
    title: 'site.title',
    description: 'site.description',
    author: 'AMIV an der ETH, Sandro Lutz',
    siteUrl: `https://amiv.ethz.ch`,
    apiUrl: process.env.NEXT_PUBLIC_API_DOMAIN
      ? `https://${process.env.NEXT_PUBLIC_API_DOMAIN}`
      : 'https://api-dev.amiv.ethz.ch',
    OAuthId: process.env.NEXT_PUBLIC_OAUTH_ID || 'Local Tool',
  },
  // Load the dateFormatter config directly from this file.
  dateFormatterConfig: {
    timeZone: dateFormatOptions.timeZone,
    dateFormatter: {
      en: {
        short: new Intl.DateTimeFormat('en-GB', {
          ...dateFormatOptions,
          timeZoneName: 'short',
        }),
        weekday: new Intl.DateTimeFormat('en-GB', {
          ...dateFormatOptions,
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          weekday: 'long',
        }),
        weekdayTimezone: new Intl.DateTimeFormat('en-GB', {
          ...dateFormatOptions,
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          weekday: 'long',
          timeZoneName: 'short',
        }),
      },
      de: {
        short: new Intl.DateTimeFormat('de-DE', {
          ...dateFormatOptions,
          timeZoneName: 'short',
        }),
        weekday: new Intl.DateTimeFormat('de-DE', {
          ...dateFormatOptions,
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          weekday: 'long',
        }),
        weekdayTimezone: new Intl.DateTimeFormat('de-DE', {
          ...dateFormatOptions,
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          weekday: 'long',
          timeZoneName: 'short',
        }),
      },
    },
  },
}
