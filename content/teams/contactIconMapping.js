import WebsiteIcon from '@material-ui/icons/Public'
import EmailIcon from '@material-ui/icons/Email'
import PhoneIcon from '@material-ui/icons/Phone'
import CalendarIcon from '@material-ui/icons/Event'
import InstagramIcon from '@material-ui/icons/Instagram'
import FacebookIcon from '@material-ui/icons/Facebook'
import LinkedInIcon from '@material-ui/icons/LinkedIn'

/**
 * This specifies a mapping of the string value to the actual icon
 *
 * Have a look at https://material-ui.com/components/material-icons/ for more icons!
 */
const contactIconMapping = {
  phone: PhoneIcon,
  email: EmailIcon,
  website: WebsiteIcon,
  calendar: CalendarIcon,
  instagram: InstagramIcon,
  facebook: FacebookIcon,
  linkedin: LinkedInIcon,
}

export default contactIconMapping
