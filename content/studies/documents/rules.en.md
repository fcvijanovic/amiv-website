# Rules

1. This platform is based on a give-and-take principle, so please consider investing a few minutes to see whether you could contribute anything yourself. It does not take much time.

2. Some of the listed documents are subject to the [BOT](https://rechtssammlung.sp.ethz.ch/_layouts/15/start.aspx#/default.aspx) as well as copyright and serve only as internal documentation according to [Bundesgesetz SR 231.1, Art. 19.1c](https://www.admin.ch/opc/de/classified-compilation/19920251/index.html#a19) and must not be distributed to non-ETH members. For these reasons Login is mandatory.

3. Plagiarism may have dire consequences. Do not promote work of others as your own! Please read [this page](https://www.ethz.ch/studierende/de/studium/leistungskontrollen/plagiate.html).

4. If you have any feedback or spotted some issues, tell us by mail on info@amiv.ethz.ch.

On behalf of all students we would like to thank those who contribute with their own documents and summaries!
