import { useState, useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { animateScroll } from 'react-scroll'
import { useIntl } from 'react-intl'
import { useTheme } from '@material-ui/core'

import { loadItem } from '../store/common/actions'

const PATH_UPDATE_DELAY = 250 // in milliseconds

/**
 * useFilteredList hook - common logic for FilteredListPages
 *
 * Handles the path changes when an item is expanded/collapsed so that
 * the animation is visible and the page does not scroll unpredictably.
 *
 * Also handles pinned items and whether it should scroll to the
 * active item.
 *
 * @param {string} resource API resource name
 * @param {string} pathPrefix prefix of the resource path
 *                            (between language code and item id)
 * @param {array} lists array of lists
 * @param {object} items dictionary of items accessible by their id.
 * @param {string} itemId id of the item originally requested by the page.
 * @param {bool} itemsReady if `true`, ready to check if requested item is loaded.
 */
const useFilteredList = (
  resource,
  pathPrefix,
  lists,
  items,
  itemId,
  itemsReady = true
) => {
  const dispatch = useDispatch()

  // Set to the selected item id on initial page load.
  const [storedItemId, setStoredItemId] = useState(null)
  const [navigateTimeout, setNavigateTimeout] = useState(null)
  const [pinnedId, setPinnedId] = useState(null)
  const [shouldScroll, setShouldScroll] = useState(!!itemId)
  const theme = useTheme()
  const ref = useRef(null)
  const intl = useIntl()

  useEffect(() => {
    setStoredItemId(itemId)
    setShouldScroll(true)
  }, [itemId])

  // Handle set of pinned item on initial load.
  useEffect(() => {
    if (!storedItemId || !itemsReady) return

    // Load item if not already loaded.
    if (!items[storedItemId]) {
      dispatch(loadItem(resource, { id: storedItemId }))
    }

    // Check if item already part of a list. If not, make it pinned at the top of the page.
    if (lists.every(list => !list.items.includes(storedItemId))) {
      setPinnedId(storedItemId)
    }
  }, [storedItemId, itemsReady])

  // Handle initial scroll to active element
  useEffect(() => {
    if (itemsReady && ref.current && shouldScroll) {
      setShouldScroll(false)
      animateScroll.scrollTo(
        ref.current.getBoundingClientRect().top - theme.shape.headerHeight,
        {
          duration: 250,
          smooth: 'easeInOutQuint',
        }
      )
    }
  }, [itemsReady, ref.current, shouldScroll])

  const itemExpandHandler = ({ id, expanded }) => {
    let path = null
    if (expanded) {
      path = `/${intl.locale}/${pathPrefix}/${id}`
      setStoredItemId(id)
    } else {
      path = `/${intl.locale}/${pathPrefix}`
      setStoredItemId(null)
    }

    // The timeout is needed so that the animation is performed before switching
    // the page. Otherwise the CSS animations will not trigger.
    if (navigateTimeout) {
      clearTimeout(navigateTimeout)
    }
    setNavigateTimeout(
      setTimeout(() => {
        if (window.location.pathname !== path) {
          // eslint-disable-next-line no-restricted-globals
          history.pushState(null, '', path)
        }
      }, PATH_UPDATE_DELAY)
    )
  }

  return [ref, pinnedId, storedItemId, itemExpandHandler]
}

export default useFilteredList
