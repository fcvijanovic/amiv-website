import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'

import { createTheme, defaultTheme } from '../theme'

const defaultContextState = {
  type: 'light',
  theme: {},
  toggleDarkTheme: () => {},
}

const ThemeContext = React.createContext(defaultContextState)

const ThemeProvider = ({ children }) => {
  const [theme, setThemeState] = useState(createTheme(defaultTheme))
  const [isThemeLoaded, setThemeLoaded] = useState(false)

  const themeTypeStorageKey = 'theme'

  const setTheme = type => {
    const newTheme = createTheme(type)
    setThemeState(newTheme)
    localStorage.setItem(themeTypeStorageKey, type)
  }

  const loadDarkThemeFromLocalStorage = () => {
    let localStorageTheme = null

    try {
      localStorageTheme = localStorage.getItem(themeTypeStorageKey)
    } catch (err) {
      // error while reading localStorage. Key might not exist yet.
    }

    if (!localStorageTheme) {
      // no stored theme configuration found.
      const preferDarkQuery = '(prefers-color-scheme: dark)'
      const mql = window.matchMedia(preferDarkQuery)
      const supportsColorSchemeQuery = mql.media === preferDarkQuery

      if (supportsColorSchemeQuery) {
        // use system preference
        localStorageTheme = mql.matches ? 'dark' : 'light'
        localStorage.setItem(themeTypeStorageKey, localStorageTheme)
      } else {
        // use default theme
        localStorageTheme = defaultTheme
        localStorage.setItem(themeTypeStorageKey, localStorageTheme)
      }
    }

    if (localStorageTheme !== theme.palette.type) {
      setTheme(localStorageTheme)
    }
  }

  const toggleDarkTheme = () => {
    const newType = theme.palette.type === 'dark' ? 'light' : 'dark'
    setTheme(newType)
  }

  useEffect(() => {
    if (!isThemeLoaded) {
      loadDarkThemeFromLocalStorage()
      setThemeLoaded(true)
    }
  })

  return (
    <MuiThemeProvider theme={theme}>
      <ThemeContext.Provider
        value={{
          type: theme.palette.type,
          theme,
          toggleDarkTheme,
        }}
      >
        {children}
      </ThemeContext.Provider>
    </MuiThemeProvider>
  )
}

ThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
}

const useTheme = () => React.useContext(ThemeContext)

export default ThemeContext
export { ThemeProvider, useTheme }
