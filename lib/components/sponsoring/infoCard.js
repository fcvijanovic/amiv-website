import React from 'react'
import { FormattedMessage } from 'react-intl'
import {
  Button,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Grid,
  Typography,
} from '@material-ui/core'
import { Email, OpenInNew } from '@material-ui/icons'

import PropTypes from 'prop-types'

const InfoCard = ({
  infoTitle,
  infoWebsite,
  infoHyperlink,
  infoContent,
  infoContact,
  infoContactButton,
}) => {
  return (
    <Grid item xs={12} sm={6} md={4} style={{ display: 'flex' }}>
      <Card
        elevation={3}
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'column',
        }}
      >
        <CardHeader title={infoTitle} />
        <CardContent>
          <Typography>{infoContent}</Typography>
        </CardContent>
        <CardActions>
          {infoWebsite && (
            <Button
              component="a"
              target="_blank"
              href={infoHyperlink}
              endIcon={<OpenInNew />}
            >
              <FormattedMessage id="sponsoring.website" />
            </Button>
          )}
          {infoContact && (
            <Button
              component="a"
              href={`mailto:${infoContact}`}
              endIcon={<Email />}
            >
              {infoContactButton}
            </Button>
          )}
        </CardActions>
      </Card>
    </Grid>
  )
}

InfoCard.propTypes = {
  infoTitle: PropTypes.string,
  infoWebsite: PropTypes.string,
  infoHyperlink: PropTypes.string,
  infoContent: PropTypes.string,
  infoContact: PropTypes.string,
  infoContactButton: PropTypes.element,
}

export default InfoCard
