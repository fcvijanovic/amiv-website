import React from 'react'
import { useIntl } from 'react-intl'
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Avatar,
  Chip,
  Grid,
  Paper,
  Typography,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'

import PropTypes from 'prop-types'

import Image from '../general/staticImage'

const useStyles = makeStyles(
  {
    image: {
      width: '100%',
      borderRadius: '.5rem',
      overflow: 'hidden',
    },
  },
  { name: 'merchItem' }
)

const MerchItem = ({ item }) => {
  const classes = useStyles()
  const intl = useIntl()

  const concatenate = arr => arr.join(', ')

  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography variant="h6">
          {item.title[intl.locale]}{' '}
          {item.price && (
            <Chip
              avatar={
                <Avatar>
                  <AttachMoneyIcon />
                </Avatar>
              }
              label={item.price}
              variant="outlined"
            />
          )}
        </Typography>
      </AccordionSummary>
      <AccordionDetails style={{ display: 'block' }}>
        {item.description && (
          <Typography style={{ marginBottom: '10px' }}>
            {item.description[intl.locale]}
          </Typography>
        )}
        {item.sizes && (
          <Typography style={{ marginBottom: '10px' }}>
            Grössen: {concatenate(item.sizes)}
          </Typography>
        )}
        {item.colors && (
          <Typography style={{ marginBottom: '10px' }}>
            Farben: {concatenate(item.colors[intl.locale])}
          </Typography>
        )}

        <Grid container spacing={2} style={{ justifyContent: 'center' }}>
          {item.images.map(image => (
            <Grid item xs={12} sm={6} md={4} key={image}>
              <Paper elevation={3} style={{ borderRadius: '.5rem' }}>
                <Image
                  className={classes.image}
                  ratioX={1}
                  ratioY={1}
                  src={image}
                />
              </Paper>
            </Grid>
          ))}
        </Grid>
      </AccordionDetails>
    </Accordion>
  )
}

MerchItem.propTypes = {
  item: PropTypes.object.isRequired,
}

export default MerchItem
