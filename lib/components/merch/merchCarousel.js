import React from 'react'
import { useIntl } from 'react-intl'
import Carousel from 'react-material-ui-carousel'

import PropTypes from 'prop-types'

import { Grid, Paper } from '@material-ui/core'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { makeStyles } from '@material-ui/core/styles'

import Image from '../general/staticImage'

const useStyles = makeStyles(
  {
    image: {
      width: '100%',
      borderRadius: '.5rem',
      overflow: 'hidden',
      '& img': {
        width: '100%',
        height: '100%',
      },
    },
  },
  { name: 'merchCarousel' }
)

const MerchCarousel = ({ data }) => {
  const classes = useStyles()
  const intl = useIntl()
  const md = useMediaQuery(theme => theme.breakpoints.up('md'))
  const sm = useMediaQuery(theme => theme.breakpoints.up('sm'))

  const getNumImages = () => {
    if (md) return 3
    if (sm) return 2
    return 1
  }

  const getImages = item => {
    const num = getNumImages()
    if (item.images.length <= num) return item.images
    return item.images.slice(0, num)
  }

  return (
    <div style={{ marginTop: '20px', marginBottom: '20px' }}>
      <Carousel swipe={false}>
        {data.map(item => (
          <React.Fragment key={item}>
            <Grid container spacing={2} style={{ justifyContent: 'center' }}>
              {getImages(item).map(image => (
                <Grid item xs={12} sm={6} md={4} key={image}>
                  <Paper elevation={3} style={{ borderRadius: '.5rem' }}>
                    <Image
                      className={classes.image}
                      ratioX={1}
                      ratioY={1}
                      src={image}
                    />
                  </Paper>
                </Grid>
              ))}
            </Grid>
            <div style={{ textAlign: 'center' }}>
              <h2>{item.title[intl.locale]}</h2>
            </div>
          </React.Fragment>
        ))}
      </Carousel>
    </div>
  )
}

MerchCarousel.propTypes = {
  data: PropTypes.object.isRequired,
}

export default MerchCarousel
