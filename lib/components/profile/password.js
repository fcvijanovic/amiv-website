import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { useIntl, FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Collapse, TextField, Button } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { HelpTooltip } from 'amiv-react-components'

import { USER_ACTION_PASSWORD } from '../../store/user/constants'
import { changePassword } from '../../store/user/actions'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
    description: {
      fontWeight: 800,
      flexGrow: 0.5,
      width: 'calc(50% - 1em)',
      margin: '0 .5em',
      textAlign: 'right',
    },
    status: {
      flexGrow: 0.5,
      width: 'calc(50% - 1em)',
      margin: '0 .5em',
      textAlign: 'left',
    },
    noPassword: {
      fontStyle: 'italic',
    },
    action: {
      cursor: 'pointer',
    },
    content: {
      maxWidth: '500px',
      borderRadius: '4px',
      padding: '16px',
      backgroundColor: theme.palette.common.grey,
      marginTop: '.5em',
      textAlign: 'left',

      '& > *': {
        width: '100%',
      },
    },
    field: {
      margin: '.5em 0',
    },
    buttons: {
      display: 'flex',
      marginTop: '.5em',

      '& > *': {
        flexGrow: 1,
      },
    },
    ldapButton: {
      marginRight: '1em',
    },
  }),
  { name: 'password' }
)

const ProfilePassword = ({ className, ...props }) => {
  const [expanded, setExpanded] = useState(false)
  const [currentPassword, setCurrentPassword] = useState(null)
  const [newPassword, setNewPassword] = useState(null)
  const [newRepeated, setNewRepeated] = useState(null)
  const { data: user, isPending, action, error } = useSelector(
    state => state.user
  )
  const dispatch = useDispatch()
  const classes = useStyles()
  const intl = useIntl()

  const reset = () => {
    setCurrentPassword(null)
    setNewPassword(null)
    setNewRepeated(null)
  }

  const handleRevertToLdapClick = () => {
    dispatch(changePassword(currentPassword, null))
    reset()
  }

  const handleUpdateClick = () => {
    dispatch(changePassword(currentPassword, newPassword))
    reset()
  }

  const isCurrentPasswordValid = currentPassword && currentPassword.length > 0
  const isNewPasswordValid =
    newPassword && newPassword.length >= 7 && newPassword.length <= 100
  const isNewRepeatedValid = newRepeated === newPassword

  const hasPatchError = !isPending && action === USER_ACTION_PASSWORD && error
  const canRevertToLdap = user.password_set
  const hasPasswordSet = user.password_set
  const disableRevertToLdap = !isCurrentPasswordValid
  const disablePasswordChange =
    !isCurrentPasswordValid || !isNewPasswordValid || !isNewRepeatedValid

  let notification = null

  if (hasPatchError) {
    const wrongPassword =
      error && error.response && error.response.status === 401
    notification = (
      <Alert variant="filled" severity="error">
        <FormattedMessage
          id={
            wrongPassword
              ? 'profile.password.errors.current'
              : 'profile.password.errors.unknown'
          }
        />
      </Alert>
    )
  } else {
    notification = (
      <Alert variant="filled" severity="info">
        <FormattedMessage id="profile.password.explanation" />
      </Alert>
    )
  }

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      <div className={classes.description}>
        <FormattedMessage id="profile.password" />
        <HelpTooltip
          title={intl.formatMessage({ id: 'profile.password.explanation' })}
        />
      </div>
      <div className={classes.status}>
        <span className={!user.rfid ? classes.noPassword : undefined}>
          {user.password_set ? (
            '************'
          ) : (
            <FormattedMessage id="profile.password.notSet" />
          )}
        </span>
        &nbsp;
        <a className={classes.action} onClick={() => setExpanded(!expanded)}>
          <FormattedMessage id={expanded ? 'close' : 'change'} />
        </a>
      </div>
      <Collapse in={expanded}>
        <div className={classes.content}>
          {notification}
          <TextField
            name="currentPassword"
            type="password"
            className={classes.field}
            value={currentPassword || ''}
            label={intl.formatMessage({ id: 'profile.password.current' })}
            onChange={e => setCurrentPassword(e.target.value || null)}
          />
          <TextField
            name="newPassword"
            type="password"
            className={classes.field}
            value={newPassword || ''}
            label={intl.formatMessage({ id: 'profile.password.new' })}
            onChange={e => setNewPassword(e.target.value || null)}
            error={!isNewPasswordValid && !!newPassword}
            helperText={intl.formatMessage({
              id: 'profile.password.requirements',
            })}
          />
          <TextField
            name="newRepeatedPassword"
            type="password"
            value={newRepeated || ''}
            className={classes.field}
            label={intl.formatMessage({ id: 'profile.password.repeatNew' })}
            onChange={e => setNewRepeated(e.target.value || null)}
            error={!isNewRepeatedValid}
            helperText={
              !isNewRepeatedValid &&
              intl.formatMessage({
                id: 'profile.password.errors.notEqual',
              })
            }
          />
          <div className={classes.buttons}>
            {canRevertToLdap && (
              <Button
                disabled={disableRevertToLdap}
                className={classes.ldapButton}
                onClick={handleRevertToLdapClick}
              >
                <FormattedMessage id="profile.password.revertToLdap" />
              </Button>
            )}
            <Button
              disabled={disablePasswordChange}
              onClick={handleUpdateClick}
              variant="contained"
              color="primary"
            >
              <FormattedMessage
                id={
                  hasPasswordSet
                    ? 'profile.password.change'
                    : 'profile.password.set'
                }
              />
            </Button>
          </div>
        </div>
      </Collapse>
    </div>
  )
}

ProfilePassword.propTypes = {
  /** @ignore */
  className: PropTypes.string,
}

export default ProfilePassword
