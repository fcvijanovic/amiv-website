import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useIntl } from 'react-intl'
import { Button } from '@material-ui/core'

import {
  loadOtherSessions,
  clearOtherSessions,
} from 'lib/store/sessions/actions'

const ProfileSessionInfo = props => {
  const { items: sessions, isPending, error } = useSelector(
    state => state.sessions
  )
  const dispatch = useDispatch()
  const intl = useIntl()

  useEffect(() => {
    dispatch(loadOtherSessions())
  }, [])

  const handleClick = () => {
    if (sessions && sessions.length > 1) {
      dispatch(clearOtherSessions())
    }
  }

  const disabled = isPending || !sessions || sessions.length < 2
  let label = null

  if (sessions && sessions.length > 1) {
    label = intl.formatMessage(
      { id: 'profile.sessions.terminateOthers' },
      { count: sessions.length - 1 }
    )
  } else if (isPending) {
    label = intl.formatMessage({ id: 'profile.sessions.loading' })
  } else if (error) {
    label = intl.formatMessage({ id: 'profile.sessions.error' })
  } else {
    label = intl.formatMessage({ id: 'profile.sessions.none' })
  }

  return (
    <div {...props}>
      <Button color="secondary" disabled={disabled} onClick={handleClick}>
        {label}
      </Button>
    </div>
  )
}

export default ProfileSessionInfo
