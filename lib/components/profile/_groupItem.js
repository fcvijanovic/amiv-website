import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'

import { enrollToGroup } from '../../store/groups/actions'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      textAlign: 'left',
      padding: '0.5em',
      borderBottom: `1px solid ${theme.palette.common.grey}`,
      alignItems: 'center',

      '&:last-child': {
        borderBottom: 'none',
      },
    },
    name: {
      flexGrow: 1,
      verticalAlign: 'middle',
      marginRight: '.5em',
    },
  }),
  { name: 'groupItem' }
)

const ProfileGroupItem = ({ item, className, ...props }) => {
  const [isPending, setIsPending] = useState(false)
  const classes = useStyles()
  const dispatch = useDispatch()

  const handleEnrollClick = () => {
    setIsPending(true)
    dispatch(enrollToGroup(item)).catch(() => setIsPending(false))
  }

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      <span className={classes.name}>{item.name}</span>
      <Button
        variant="contained"
        color="primary"
        disabled={isPending}
        onClick={handleEnrollClick}
      >
        <FormattedMessage id="button.enroll" />
      </Button>
    </div>
  )
}

ProfileGroupItem.propTypes = {
  /** Group object from API */
  item: PropTypes.object.isRequired,
  /** @ignore */
  className: PropTypes.string,
}

export default ProfileGroupItem
