import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useIntl } from 'react-intl'

import GroupList from './_groupList'
import GroupMembershipItem from './_groupMembershipItem'
import { loadMemberships } from '../../store/groups/actions'

const ProfileGroupMemberships = props => {
  const { items } = useSelector(state => state.groups.memberships)
  const dispatch = useDispatch()
  const intl = useIntl()

  useEffect(() => {
    dispatch(loadMemberships())
  }, [])

  const itemSearchTest = (item, search) => {
    return item.group.name.toLowerCase().includes(search.toLowerCase())
  }

  return (
    <GroupList
      title={intl.formatMessage({ id: 'profile.groups.searchEnrolled' })}
      items={items}
      itemSearchTest={itemSearchTest}
      itemComponent={GroupMembershipItem}
      {...props}
    />
  )
}

export default ProfileGroupMemberships
