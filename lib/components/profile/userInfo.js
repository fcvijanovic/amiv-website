import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Alert from '@material-ui/lab/Alert'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      position: 'relative',
      paddingBottom: '2em !important',
      marginBottom: '2em !important',

      '&::after': {
        content: '" "',
        display: 'block',
        position: 'absolute',
        backgroundColor: theme.palette.common.grey,
        height: '4px',
        bottom: 0,
        right: 0,
        left: 0,
        borderRadius: '2px',
        overflow: 'hidden',
      },

      '& > span': {
        display: 'inline-block',

        '&::after': {
          content: "'/'",
          color: theme.palette.secondary.main,
          margin: '0 1em',
          fontSize: '1em',
          fontWeight: 800,
        },
        '&:last-of-type::after': {
          content: "''",
          margin: 0,
        },
      },
    },
    name: {
      fontWeight: 700,
      fontSize: '1.2em',
    },
  }),
  { name: 'userinfo' }
)

const ProfileUserInfo = ({ className, ...props }) => {
  const user = useSelector(state => state.user.data)
  const classes = useStyles()

  // Do not render anything if user is not loaded.
  if (!user) {
    return null
  }

  const showRfidWarning =
    user.membership !== 'none' && (!user.rfid || user.rfid.length !== 6)

  return (
    <React.Fragment>
      <div className={[className, classes.root].join(' ')} {...props}>
        <span className={classes.name}>
          {user.firstname} {user.lastname}
        </span>
        <span>{user.email}</span>
        <span>
          <FormattedMessage id={`membership.${user.membership}`} />
        </span>
      </div>
      {showRfidWarning && (
        <Alert severity="info">
          <FormattedMessage id="profile.rfidWarning" />
        </Alert>
      )}
    </React.Fragment>
  )
}

ProfileUserInfo.propTypes = {
  /** @ignore */
  className: PropTypes.string,
}

export default ProfileUserInfo
