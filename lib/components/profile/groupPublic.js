import React, { useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useIntl } from 'react-intl'

import GroupList from './_groupList'
import GroupItem from './_groupItem'
import { loadPublicGroups } from '../../store/groups/actions'

const ProfileGroupPublic = props => {
  const { items: membershipItems } = useSelector(
    state => state.groups.memberships
  )
  const { items: groups } = useSelector(state => state.groups.publicGroups)
  const dispatch = useDispatch()
  const intl = useIntl()

  useEffect(() => {
    dispatch(loadPublicGroups())
  }, [])

  const items = useCallback(() => {
    return groups.filter(group => {
      let hasMembership = false
      membershipItems.forEach(membership => {
        hasMembership = hasMembership || membership.group._id === group._id
      })
      return !hasMembership
    })
  }, [membershipItems, groups])

  const itemSearchTest = (item, search) => {
    return item.name.toLowerCase().includes(search.toLowerCase())
  }

  return (
    <GroupList
      title={intl.formatMessage({ id: 'profile.groups.searchPublic' })}
      items={items()}
      itemSearchTest={itemSearchTest}
      itemComponent={GroupItem}
      {...props}
    />
  )
}

export default ProfileGroupPublic
