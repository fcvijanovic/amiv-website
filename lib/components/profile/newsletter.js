import React from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'

import { USER_ACTION_NEWSLETTER } from '../../store/user/constants'
import { toggleNewsletterSubscription } from '../../store/user/actions'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignItems: 'baseline',
    },
    description: {
      fontWeight: 800,
      flexGrow: 0.5,
      width: 'calc(50% - 1em)',
      margin: '0 .5em',
      textAlign: 'right',
    },
    status: {
      flexGrow: 0.5,
      width: 'calc(50% - 1em)',
      margin: '0 .5em',
      textAlign: 'left',
    },
    action: {
      cursor: 'pointer',
    },
    actionDisabled: {
      cursor: 'default',
      color: theme.palette.info.main,
    },
  }),
  { name: 'newsletter' }
)

const ProfileNewsletter = ({ className, ...props }) => {
  const { data: user, isPending, action } = useSelector(state => state.user)
  const dispatch = useDispatch()
  const classes = useStyles()

  const disabled = isPending && action === USER_ACTION_NEWSLETTER

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      <div className={classes.description}>
        <FormattedMessage
          id="profile.newsletter.description"
          values={{ name: 'amiv Announce' }}
        />
      </div>
      <div className={classes.status}>
        <FormattedMessage
          id={
            user.send_newsletter
              ? 'profile.newsletter.subscribed'
              : 'profile.newsletter.notSubscribed'
          }
        />
        &nbsp;
        <a
          className={[
            classes.action,
            disabled ? classes.actionDisabled : undefined,
          ].join(' ')}
          onClick={() => {
            dispatch(toggleNewsletterSubscription())
          }}
        >
          <FormattedMessage id="change" />
        </a>
      </div>
    </div>
  )
}

ProfileNewsletter.propTypes = {
  /** @ignore */
  className: PropTypes.string,
}

export default ProfileNewsletter
