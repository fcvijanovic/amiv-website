import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'

import { withdrawFromGroup } from '../../store/groups/actions'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      textAlign: 'left',
      padding: '0.5em',
      borderBottom: `1px solid ${theme.palette.common.grey}`,
      alignItems: 'center',

      '& > *': {
        marginRight: '.5em',
      },

      '& > *:last-child': {
        marginRight: 0,
      },
      '&:last-child': {
        borderBottom: 'none',
      },
    },
    name: {
      flexGrow: 1,
      verticalAlign: 'middle',
    },
  }),
  { name: 'groupMembershipItem' }
)

const ProfileGroupMembershipItem = ({ item, className, ...props }) => {
  const [confirm, setConfirm] = useState(false)
  const [isPending, setIsPending] = useState(false)
  const classes = useStyles()
  const dispatch = useDispatch()

  const handleConfirmClick = () => {
    setIsPending(true)
    dispatch(withdrawFromGroup(item)).catch(() => setIsPending(false))
  }

  let buttons = null

  if (confirm) {
    buttons = (
      <React.Fragment>
        <Button
          variant="contained"
          color="primary"
          disabled={isPending}
          onClick={handleConfirmClick}
        >
          <FormattedMessage id="button.confirm" />
        </Button>
        <Button disabled={isPending} onClick={() => setConfirm(false)}>
          <FormattedMessage id="button.cancel" />
        </Button>
      </React.Fragment>
    )
  } else {
    buttons = (
      <Button
        variant="contained"
        color="primary"
        disabled={isPending}
        onClick={() => setConfirm(true)}
      >
        <FormattedMessage id="button.withdraw" />
      </Button>
    )
  }

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      <span className={classes.name}>{item.group.name}</span>
      {item.expiry && (
        <span>
          <FormattedMessage
            id="profile.groups.expires"
            values={{ date: item.expiry }}
          />
        </span>
      )}
      {buttons}
    </div>
  )
}

ProfileGroupMembershipItem.propTypes = {
  /** groupmembership object from API */
  item: PropTypes.object.isRequired,
  /** @ignore */
  className: PropTypes.string,
}

export default ProfileGroupMembershipItem
