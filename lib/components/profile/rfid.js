import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { useIntl, FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Collapse, TextField, Button } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { HelpTooltip } from 'amiv-react-components'

import { USER_ACTION_RFID } from '../../store/user/constants'
import { updateRfid } from '../../store/user/actions'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
    description: {
      fontWeight: 800,
      flexGrow: 0.5,
      width: 'calc(50% - 1em)',
      margin: '0 .5em',
      textAlign: 'right',
    },
    status: {
      flexGrow: 0.5,
      width: 'calc(50% - 1em)',
      margin: '0 .5em',
      textAlign: 'left',
    },
    noRfid: {
      fontStyle: 'italic',
    },
    action: {
      cursor: 'pointer',
    },
    content: {
      width: '100%',
      borderRadius: '4px',
      padding: '16px',
      backgroundColor: theme.palette.common.grey,
      marginTop: '.5em',

      '& > *': {
        margin: '0 .5em',
      },
    },
  }),
  { name: 'rfid' }
)

const ProfileRfid = ({ className, ...props }) => {
  const [expanded, setExpanded] = useState(false)
  const [rfid, setRfid] = useState(null)
  const [isValid, setIsValid] = useState(true)
  const { data: user, isPending, action, error } = useSelector(
    state => state.user
  )
  const dispatch = useDispatch()
  const classes = useStyles()
  const intl = useIntl()

  useEffect(() => {
    setRfid(user.rfid)
  }, [user])

  const handleChange = event => {
    const newValue = event.target.value || null
    const newIsValid = newValue === null || newValue.length === 6
    setRfid(newValue)
    setIsValid(newIsValid)
  }

  const handleClick = () => {
    dispatch(updateRfid(rfid))
  }

  const disabled = !isValid || (isPending && action === USER_ACTION_RFID)
  const hasPatchError = !isPending && action === USER_ACTION_RFID && error

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      <div className={classes.description}>
        <FormattedMessage id="profile.rfid" />
        <HelpTooltip
          title={intl.formatMessage({ id: 'profile.rfid.why-and-where' })}
        />
      </div>
      <div className={classes.status}>
        <span className={!user.rfid ? classes.noRfid : undefined}>
          {user.rfid || <FormattedMessage id="profile.rfid.notSet" />}
        </span>
        &nbsp;
        <a className={classes.action} onClick={() => setExpanded(!expanded)}>
          <FormattedMessage id={expanded ? 'close' : 'change'} />
        </a>
      </div>
      <Collapse in={expanded}>
        <div className={classes.content}>
          {hasPatchError && (
            <Alert variant="filled" severity="error">
              <FormattedMessage id="profile.rfid.patchError" />
            </Alert>
          )}
          <TextField
            name="rfid"
            autoComplete="nope"
            value={rfid || ''}
            onChange={handleChange}
            inputProps={{ maxLength: 6 }}
          />
          <Button disabled={disabled} onClick={handleClick}>
            <FormattedMessage id="button.confirm" />
          </Button>
        </div>
      </Collapse>
    </div>
  )
}

ProfileRfid.propTypes = {
  /** @ignore */
  className: PropTypes.string,
}

export default ProfileRfid
