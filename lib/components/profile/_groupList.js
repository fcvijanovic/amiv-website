import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { SearchField } from 'amiv-react-components'

const useStyles = makeStyles(
  {
    root: {
      width: '100%',
    },
    row: {
      display: 'flex',
      width: '100%',
    },
    noItems: {
      textAlign: 'center',
      padding: '3em 1em',
    },
  },
  { name: 'profileGroupList' }
)

const ProfileGroupList = ({
  title,
  items,
  itemSearchTest,
  itemComponent: ItemComponent,
  ...props
}) => {
  const [search, setSearch] = useState('')
  const classes = useStyles()

  const handleSearchChange = event => {
    setSearch(event.target.value)
  }

  let counter = 0

  return (
    <div {...props}>
      <SearchField
        name="search"
        elevation={2}
        placeholder={title}
        onChange={handleSearchChange}
      />
      {items.map(item => {
        if (!itemSearchTest(item, search)) return null
        counter += 1
        return (
          <ItemComponent key={item._id} className={classes.row} item={item} />
        )
      })}
      {counter === 0 && (
        <div className={classes.noItems}>
          <FormattedMessage id="profile.groups.noneFound" />
        </div>
      )}
    </div>
  )
}

ProfileGroupList.propTypes = {
  /** Title written as a placeholder in the search field */
  title: PropTypes.string.isRequired,
  /** Component type for rendering an item in the list */
  itemComponent: PropTypes.elementType.isRequired,
  /** Item array holding groups or groupmemberships */
  items: PropTypes.array.isRequired,
  /**
   * Function to check whether the search applies to this item.
   *
   * @param {any} item
   * @param {RegEx} search search regex */
  itemSearchTest: PropTypes.func.isRequired,
}

export default ProfileGroupList
