import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/styles'
import Skeleton from '@material-ui/lab/Skeleton'

import { getPublicConfig } from 'lib/utils/config'
import { translateMessage } from 'lib/utils'
import Img from '../general/dynamicImage'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'grid',
      gridTemplateAreas: "'image content'",
      gridTemplateColumns: '182px 1fr',
      width: '100%',
      minHeight: '100px',
      [theme.breakpoints.down('sm')]: {
        gridTemplateAreas: "'content'",
        gridTemplateColumns: '1fr',
      },
    },
    image: {
      gridArea: 'image',
      display: 'block',
      margin: '16px',
      width: 'calc(100% - 32px)',
      backgroundColor: 'transparent',
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
    content: {
      gridArea: 'content',
      textAlign: 'left',
      padding: '1em',
    },
    title: {
      padding: 0,
      margin: 0,
      marginBottom: '.25em',
    },
  }),
  { name: 'eventSummary' }
)

const JobofferSummary = ({ jobofferId, ...props }) => {
  const { apiUrl } = getPublicConfig()
  const joboffer = useSelector(state => state.joboffers.items[jobofferId])
  const classes = useStyles()
  const intl = useIntl()

  if (!joboffer || !joboffer.data) {
    // TODO: Adapt skeleton for this layout
    return (
      <div className={classes.root} {...props}>
        <Img className={classes.image} ratioX={6} ratioY={1} />
        <div className={classes.content}>
          <Skeleton
            className={classes.title}
            height="2.5em"
            width="70%"
            variant="text"
            animation="wave"
          />
          <Skeleton height="2em" width="50%" variant="text" animation="wave" />
        </div>
      </div>
    )
  }

  const { data } = joboffer
  const imageUrl = data.logo
    ? `${apiUrl}${data.logo.file}`
    : '/logos/amiv-wheel.svg'
  const title = translateMessage({ en: data.title_en, de: data.title_de }, intl)

  const dayFactor = 1000 * 3600 * 24
  // Set createdDay to timestamp with hour set to 0 to ensure
  // correct time phrase rendering (today, yesterday, etc.)
  const createdDay = Math.floor(Date.parse(data._created) / dayFactor)
  const daysPassed = Math.floor(Date.now() / dayFactor) - createdDay

  return (
    <div className={classes.root} {...props}>
      <Img
        className={classes.image}
        src={imageUrl}
        ratioX={6}
        ratioY={1}
        alt={data.company}
      />
      <div className={classes.content}>
        <h2 className={classes.title}>{title}</h2>
        <div>
          <FormattedMessage id="jobs.published" values={{ days: daysPassed }} />
        </div>
      </div>
    </div>
  )
}

JobofferSummary.propTypes = {
  /** Joboffer id. */
  jobofferId: PropTypes.string,
}

export default JobofferSummary
