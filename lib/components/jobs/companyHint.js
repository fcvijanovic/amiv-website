import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/styles'
import Alert from '@material-ui/lab/Alert'

const useStyles = makeStyles(
  {
    root: {
      marginBottom: '2em',
    },
    link: {
      cursor: 'pointer',
    },
    dialogContent: {
      padding: '0 24px 16px 24px',
    },
    div: {
      textAlign: 'left',
    },
  },
  { name: 'jobsCompanyHint' }
)

const JobsCompanyHint = ({ className, ...props }) => {
  const classes = useStyles()

  return (
    <Alert
      className={[classes.root, className].join(' ')}
      severity="info"
      {...props}
    >
      <div>
        <b>
          <FormattedMessage id="jobs.companyHintCatchphrase" />
        </b>
        &nbsp;
        <FormattedMessage id="jobs.companyHintEmail" />
        <a href="mailto:jobboerse@amiv.ethz.ch" className={classes.link}>
          jobboerse@amiv.ethz.ch
        </a>
        .
      </div>
    </Alert>
  )
}

JobsCompanyHint.propTypes = {
  /** @ignore */
  className: PropTypes.string,
}

export default JobsCompanyHint
