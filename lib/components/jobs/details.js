import React from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/styles'
import { Toolbar, Button, NoSsr } from '@material-ui/core'
import { CopyButton } from 'amiv-react-components'

import { getPublicConfig } from 'lib/utils/config'

import TranslatedContent from '../general/translatedContent'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-evenly',
      alignContent: 'space-between',
      alignItems: 'center',
      width: '100%',
    },
    description: {
      flexGrow: 1,
      padding: '1em',
      textAlign: 'left',
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
    toolbar: {
      width: '100%',
      padding: '0 .4em',

      '& > *': {
        marginLeft: '.5em',

        '&:last-child': {
          marginRight: '.5em',
        },
      },
    },
    toolbarSeparator: {
      flexGrow: 1,
    },
  }),
  { name: 'jobofferDetails' }
)

const JobofferDetails = ({ jobofferId, ...props }) => {
  const { apiUrl } = getPublicConfig()
  const joboffer = useSelector(state => state.joboffers.items[jobofferId])
  const classes = useStyles()
  const intl = useIntl()

  // Do not render anything when joboffer is not loaded (yet)
  if (!joboffer || !joboffer.data) return null

  const { data } = joboffer

  return (
    <div className={[classes.root].join(' ')} {...props}>
      <TranslatedContent
        parseMarkdown
        className={classes.description}
        content={{ en: data.description_en, de: data.description_de }}
      />
      <Toolbar className={classes.toolbar} variant="dense">
        {/* Buttons on the LEFT side */}
        <span className={classes.toolbarSeparator} />
        {/* Buttons on the RIGHT side */}
        {data.pdf && (
          <Button onClick={() => window.open(apiUrl + data.pdf.file, '_blank')}>
            <FormattedMessage id="jobs.downloadAsPdf" />
          </Button>
        )}
        <NoSsr>
          <CopyButton
            value={`${window.location.origin}/${intl.locale}/jobs/${data._id}`}
          >
            <FormattedMessage id="copyDirectLink" />
          </CopyButton>
        </NoSsr>
      </Toolbar>
    </div>
  )
}

JobofferDetails.propTypes = {
  /** Joboffer id */
  jobofferId: PropTypes.string,
}

export default JobofferDetails
