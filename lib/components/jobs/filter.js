import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import debounce from 'debounce'
import { Button } from '@material-ui/core'
import { SearchField } from 'amiv-react-components'

import { JOBOFFERS } from 'lib/store/joboffers/constants'
import { setQueryFromFilterValues } from 'lib/store/joboffers/actions'
import { setFilterValue, resetFilterValues } from 'lib/store/common/actions'

import FilterView from '../filteredListPage/filter'

const EventsFilter = ({ debounceTime, ...props }) => {
  const values = useSelector(state => state.joboffers.filterValues)
  const dispatch = useDispatch()
  const router = useRouter()
  const intl = useIntl()

  const setPathIfNeeded = () => {
    router.push('/jobs')
  }

  const debouncedSetQueryFromFilterValues = React.useCallback(
    debounce(() => {
      dispatch(setQueryFromFilterValues())
      setPathIfNeeded()
    }, debounceTime),
    [dispatch, debounceTime]
  )

  const filterViewChangeHandler = ({ name, value }) => {
    dispatch(setFilterValue(JOBOFFERS, { name, value }))
    debouncedSetQueryFromFilterValues()
  }

  const filterViewResetHandler = () => {
    dispatch(resetFilterValues(JOBOFFERS))
    dispatch(setQueryFromFilterValues(JOBOFFERS))
    setPathIfNeeded()
  }

  return (
    <FilterView
      values={values}
      onChange={filterViewChangeHandler}
      onReset={filterViewResetHandler}
      {...props}
    >
      <SearchField
        name="search"
        elevation={2}
        placeholder={intl.formatMessage({ id: 'jobs.search' })}
      />
      <Button name="reset" type="reset">
        <FormattedMessage id="reset" />
      </Button>
    </FilterView>
  )
}

EventsFilter.propTypes = {
  /** Debounce time applied on filter updates */
  debounceTime: PropTypes.number,
}

EventsFilter.defaultProps = {
  debounceTime: 500,
}

export default EventsFilter
