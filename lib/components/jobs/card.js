import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Skeleton from '@material-ui/lab/Skeleton'
import { Paper } from '@material-ui/core'
import { Spinner } from 'amiv-react-components'

import { getPublicConfig } from 'lib/utils/config'
import Image from '../general/dynamicImage'
import Link from '../general/link'
import TranslatedContent from '../general/translatedContent'

const useStyles = makeStyles(
  theme => ({
    root: {
      borderRadius: '4px',
      overflow: 'hidden',
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      alignContent: 'space-between',
      height: '100%',
      padding: '1em',
    },
    title: {
      color: theme.palette.text.primary,
    },
    titleSkeleton: {
      margin: '0 auto',
    },
    logo: {
      marginTop: '1em',
      background: 'transparent',
    },
    logoSkeleton: {
      marginTop: '1em',
    },
    placeholderText: {
      textAlign: 'center',
      width: '65%',
      '& > div': {
        display: 'inline-block',
        height: '2.5em',
      },
    },
  }),
  { name: 'jobsCard' }
)

const JobsCard = ({ joboffer, elevation, className, ...props }) => {
  const { apiUrl } = getPublicConfig()
  const classes = useStyles()
  const theme = useTheme()

  const logoLoadingPlaceholder = (
    <React.Fragment>
      <Skeleton width="100%" height="100%" variant="rect" animation="pulse" />
      <Spinner centered background={theme.palette.common.white} elevation={2} />
    </React.Fragment>
  )

  return (
    <Paper elevation={2} className={[classes.root].join(' ')} {...props}>
      {joboffer ? (
        <Link href={`/jobs/${joboffer._id}`} className={classes.container}>
          <TranslatedContent
            className={classes.title}
            content={{ en: joboffer.title_en, de: joboffer.title_de }}
            noHint
          />
          <Image
            className={classes.logo}
            type="dynamic"
            src={`${apiUrl}${joboffer.logo.file}`}
            ratioX={4}
            ratioY={1}
            alt={joboffer.company}
            loadingPlaceholder={logoLoadingPlaceholder}
          />
        </Link>
      ) : (
        <div className={classes.container}>
          <Skeleton
            className={classes.titleSkeleton}
            width="80%"
            height="2em"
            variant="text"
            animation="pulse"
          />
          <Skeleton
            className={classes.logoSkeleton}
            width="100%"
            height="6em"
            variant="rect"
            animation="pulse"
          />
        </div>
      )}
    </Paper>
  )
}

JobsCard.propTypes = {
  /** Joboffer object */
  joboffer: PropTypes.object,
  /** Specifies the size of the shadow around the card */
  elevation: PropTypes.number,
  /** Additional class for the outermost box */
  className: PropTypes.string,
}

JobsCard.defaultProps = {
  elevation: 0,
}

export default JobsCard
