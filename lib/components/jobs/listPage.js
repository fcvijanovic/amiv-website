import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'

import { listLoadNextPage, listLoadAllPages } from 'lib/store/common/actions'
import { JOBOFFERS } from 'lib/store/joboffers/constants'
import useFilteredList from 'lib/hooks/useFilteredList'

import Layout from '../layout'
import JobsCompanyHint from './companyHint'
import JoboffersFilter from './filter'
import FilteredListLayout from '../filteredListPage/layout'
import FilteredList from '../filteredListPage/list'
import FilteredListItem from '../filteredListPage/listItem'
import JobofferSummary from './summary'
import JobofferDetails from './details'

const useStyles = makeStyles(
  {
    root: {
      textAlign: 'center',
      paddingTop: '2em',
    },
  },
  { name: 'jobs' }
)

const JobsListPage = ({ jobofferId: jobofferIdProp }) => {
  // Selectors for all joboffer lists
  const defaultList = useSelector(state => state.joboffers.default)
  // Selector for selected item (loaded from path)
  const joboffers = useSelector(state => state.joboffers.items)

  const dispatch = useDispatch()
  const itemsReady = defaultList.totalPages > 0
  const [ref, pinnedId, jobofferId, itemExpandHandler] = useFilteredList(
    JOBOFFERS,
    'jobs',
    [defaultList],
    joboffers,
    jobofferIdProp,
    itemsReady
  )
  const { locale } = useRouter()
  const classes = useStyles()

  useEffect(() => {
    if (defaultList.totalPages === 0 && !defaultList.isPending) {
      dispatch(listLoadAllPages(JOBOFFERS, { listName: 'default' }))
    }
  }, [defaultList])

  // Helper functions for shown lists and list items
  const isActive = item => item === jobofferId
  const hasMorePagesToLoad = list => list.lastPageLoaded < list.totalPages
  const loadMoreLabel = list => (list.error ? 'loadMoreError' : 'loadMore')
  const loadMore = list => {
    dispatch(listLoadNextPage(JOBOFFERS, { listName: list }))
  }

  const placeholder = (
    <React.Fragment>
      {Array.from(Array(3)).map((_, i) => (
        <FilteredListItem key={i} disabled>
          <JobofferSummary jobofferId={null} />
        </FilteredListItem>
      ))}
    </React.Fragment>
  )

  return (
    <Layout className={classes.root} seoProps={{ title: 'jobs.title' }}>
      <FilteredListLayout>
        <JoboffersFilter />

        {/* Company hint */}
        <JobsCompanyHint />

        {/* Pinned joboffer */}
        {pinnedId && (
          <FilteredList>
            <FilteredListItem
              key={pinnedId}
              id={pinnedId}
              ref={isActive(pinnedId) ? ref : undefined}
              expanded={isActive(pinnedId)}
              onChange={itemExpandHandler}
              href={`/${locale}/jobs${
                !isActive(pinnedId) ? `/${pinnedId}` : ''
              }`}
            >
              <JobofferSummary jobofferId={pinnedId} />
              <JobofferDetails jobofferId={pinnedId} />
            </FilteredListItem>
          </FilteredList>
        )}

        {/* All current joboffers */}
        <FilteredList
          placeholder={placeholder}
          showPlaceholder={defaultList.isPending}
          loadMoreLabel={loadMoreLabel(defaultList)}
          loadMore={
            hasMorePagesToLoad(defaultList) ? () => loadMore('default') : null
          }
        >
          {defaultList.items.map(item => (
            <FilteredListItem
              key={item}
              id={item}
              ref={isActive(item) ? ref : undefined}
              expanded={isActive(item)}
              onChange={itemExpandHandler}
              href={`/${locale}/jobs${!isActive(item) ? `/${item}` : ''}`}
            >
              <JobofferSummary jobofferId={item} />
              <JobofferDetails jobofferId={item} />
            </FilteredListItem>
          ))}
        </FilteredList>
      </FilteredListLayout>
    </Layout>
  )
}

JobsListPage.propTypes = {
  /** Joboffer id from path. */
  jobofferId: PropTypes.string,
}

export default JobsListPage
