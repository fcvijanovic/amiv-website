import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { ExternalLink, Spinner } from 'amiv-react-components'

import { loadItem } from 'lib/store/common/actions'
import { STUDYDOCUMENTS } from 'lib/store/studydocuments/constants'
import {
  loadEditFormSummary,
  postStudydocumentItem,
  patchStudydocumentItem,
} from 'lib/store/studydocuments/actions'

import TitleField from './_titleField'
import SelectTextField from './_selectTextField'
import TypeField from './_typeField'
import DepartmentField from './_departmentField'
import CourseYearField from './_courseYearField'
import SemesterField from './_semesterField'
import FileList from './_fileList'
import FileUploadButton from './_fileUploadButton'

const useStyles = makeStyles(
  {
    root: {
      textAlign: 'left',

      '& > *': {
        width: '100%',
        marginBottom: '1em',
      },
    },
    selectRow: {
      display: 'flex',
      '& > *': {
        width: '25%',
      },
    },
    submitButton: {
      marginTop: '2em',
      marginBottom: 0,
    },
  },
  { name: 'studydocumentForm' }
)

const ERROR_NOT_FOUND = 404
const ERROR_LOAD_FAILED = 500

const StudydocumentForm = ({ studydocumentId, className, ...props }) => {
  const [isLoaded, setIsLoaded] = useState(!studydocumentId)
  const [invalidFields, setInvalidFields] = useState([])
  // const [isValid, setIsValid] = useState(false)
  const [isPending, setIsPending] = useState(false)
  const [uploadError, setUploadError] = useState(false)
  const [error, setError] = useState(null)
  const [values, setValues] = useState({})
  const [existingFiles, setExistingFiles] = useState([])
  const [files, setFiles] = useState([])
  const [filesChanged, setFilesChanged] = useState(false)

  // Selector for selected item (loaded from path)
  const studydocument = useSelector(
    state => state.studydocuments.items[studydocumentId]
  )
  const {
    summary,
    isPending: isSummaryPending,
    error: summaryError,
  } = useSelector(state => state.studydocuments.editForm)

  const dispatch = useDispatch()
  const classes = useStyles()
  const router = useRouter()
  const theme = useTheme()
  const intl = useIntl()

  // Update the summmary values at first
  useEffect(() => {
    dispatch(loadEditFormSummary())
  }, [])

  // Load document if not loaded yet.
  useEffect(() => {
    if (!studydocumentId) return

    // Load studydocument if not already loaded.
    if (!studydocument) {
      dispatch(loadItem(STUDYDOCUMENTS, { id: studydocumentId }))
    }
  }, [studydocumentId])

  useEffect(() => {
    let documentDataLoaded = false
    let summaryDataLoaded = false

    if (isLoaded) return

    if (studydocument && !studydocument.isPending) {
      if (studydocument.error) {
        setError(ERROR_NOT_FOUND)
      } else if (studydocument.data) {
        const { files: filesData, ...data } = studydocument.data
        // remove all non-editable fields
        const cleanedData = Object.keys(data)
          .filter(key => !key.startsWith('_') && key !== 'uploader')
          .reduce((obj, key) => {
            return { ...obj, [key]: data[key] }
          }, {})

        setValues(cleanedData)
        if (filesData) {
          setExistingFiles(filesData.map(item => ({ info: item, file: null })))
        } else {
          setExistingFiles([])
        }
      }
      documentDataLoaded = true
    }

    if (summary && !isSummaryPending) {
      if (summaryError) {
        setError(ERROR_LOAD_FAILED)
      }
      summaryDataLoaded = true
    }

    if (documentDataLoaded && summaryDataLoaded) {
      setIsLoaded(true)
    }
  }, [studydocument, summary, isSummaryPending, summaryError])

  const isValid =
    invalidFields.length === 0 &&
    (files.length > 0 || existingFiles.length > 0) &&
    values.title !== undefined &&
    values.title !== ''

  const submit = async () => {
    if (!isValid || isPending) return

    setIsPending(true)
    setUploadError(false)

    try {
      const finalFiles = filesChanged ? files.map(item => item.file) : null
      const response = studydocumentId
        ? await dispatch(
            patchStudydocumentItem({
              id: studydocumentId,
              etag: studydocument.data._etag,
              data: values,
              files: finalFiles,
            })
          )
        : await dispatch(
            postStudydocumentItem({ data: values, files: finalFiles })
          )
      setFilesChanged(false)
      setIsPending(false)
      router.push(`/studies/documents/${response._id}`)
    } catch (e) {
      console.log(e)
      setIsPending(false)
      setUploadError(true)
    }
  }

  const handleChange = ({ name, value, isValid: isFieldValid = true }) => {
    setValues({ ...values, [name]: value || null })
    if (isFieldValid) {
      if (invalidFields.includes(name)) {
        setInvalidFields(invalidFields.filter(field => field !== name))
      }
    } else {
      setInvalidFields([...invalidFields, name])
    }
  }

  const handleFileSelected = event => {
    const newFiles = []
    for (let i = 0; i < event.target.files.length; i += 1) {
      newFiles.push({ info: null, file: event.target.files[i] })
    }
    setFiles([...files, ...newFiles])
    setFilesChanged(true)
  }

  const handleFileRemove = file_index => {
    setFiles(files.filter((file, index) => index !== file_index))
    setFilesChanged(true)
  }

  if (error) {
    return (
      <div className={[className, classes.root].join(' ')} {...props}>
        <h1>
          <FormattedMessage id="error.title" />
        </h1>
        <p>
          <FormattedMessage
            id={
              error === ERROR_NOT_FOUND ? 'error.notFound' : 'error.loadingPage'
            }
          />
        </p>
      </div>
    )
  }

  if (!isLoaded) {
    return (
      <div className={[className, classes.root].join(' ')} {...props}>
        <div>
          <Spinner
            centered
            elevation={2}
            background={theme.palette.common.white}
          />
        </div>
      </div>
    )
  }

  return (
    <form className={[className, classes.root].join(' ')} {...props}>
      {uploadError && (
        <Alert className={classes.notification} severity="error">
          <FormattedMessage id="studydocuments.uploadError" />
        </Alert>
      )}

      <TitleField value={values.title} onChange={handleChange} />
      <SelectTextField
        name="author"
        value={values.author}
        onChange={handleChange}
      />
      <SelectTextField
        name="lecture"
        value={values.lecture}
        onChange={handleChange}
      />
      <SelectTextField
        name="professor"
        value={values.professor}
        onChange={handleChange}
      />

      <div className={classes.selectRow}>
        <TypeField value={values.type} onChange={handleChange} />
        <DepartmentField value={values.department} onChange={handleChange} />
        <CourseYearField value={values.course_year} onChange={handleChange} />
        <SemesterField value={values.semester} onChange={handleChange} />
      </div>

      {existingFiles && existingFiles.length > 0 && (
        <React.Fragment>
          <FileList
            files={existingFiles}
            renderActions={item => {
              if (!item.info) return null
              const fileUrl = item.info.file
              const filename = item.info ? item.info.name : item.file.name
              return (
                <ExternalLink
                  noIcon
                  href={`${fileUrl}/${filename}`}
                  download={filename}
                >
                  <FormattedMessage id="studydocuments.actions.download" />
                </ExternalLink>
              )
            }}
          />
          <Alert severity="info">
            <FormattedMessage id="studydocuments.notice.existingFiles" />
          </Alert>
        </React.Fragment>
      )}
      <FileList
        files={files}
        placeholderText={intl.formatMessage({
          id: 'studydocuments.noFilesForUpload',
        })}
        renderActions={(item, index) => {
          return (
            <Button color="secondary" onClick={() => handleFileRemove(index)}>
              <FormattedMessage id="studydocuments.actions.delete" />
            </Button>
          )
        }}
      />

      <FileUploadButton onChange={handleFileSelected} />
      <Button
        className={classes.submitButton}
        variant="contained"
        color="primary"
        disabled={!isValid || isPending}
        onClick={submit}
      >
        <FormattedMessage
          id={isPending ? 'studydocuments.uploading' : 'studydocuments.upload'}
        />
      </Button>
    </form>
  )
}

StudydocumentForm.propTypes = {
  /** studydocument id, if available */
  studydocumentId: PropTypes.string,
  /** @ignore */
  className: PropTypes.string,
}

export default StudydocumentForm
