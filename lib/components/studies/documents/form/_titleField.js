import React from 'react'
import PropTypes from 'prop-types'
import { useIntl } from 'react-intl'
import { TextField } from '@material-ui/core'

const StudydocumentFormTitleField = ({ value, onChange, ...props }) => {
  const intl = useIntl()

  return (
    <TextField
      required
      name="title"
      label={intl.formatMessage({ id: 'title' })}
      value={value || ''}
      onChange={e =>
        onChange({
          name: 'title',
          value: e.target.value,
          isValid: e.target.value.length > 0,
        })
      }
      {...props}
    />
  )
}

StudydocumentFormTitleField.propTypes = {
  /** Value of the field. */
  value: PropTypes.string,
  /** Callback when the field value has changed. */
  onChange: PropTypes.func.isRequired,
}

export default StudydocumentFormTitleField
