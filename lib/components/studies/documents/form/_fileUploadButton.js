import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(
  theme => ({
    root: {
      margin: '2em 0',
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'baseline',
      [theme.breakpoints.down('sm')]: {
        placeContent: 'center',
      },
    },
    hint: {
      flexGrow: 1,
      marginLeft: '1em',
      textAlign: 'center',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        marginTop: '1em',
        marginLeft: 'unset',
      },
    },
  }),
  { name: 'studydocumentFormFileUploadField' }
)

const StudydocumentFormFileUploadButton = ({
  className,
  onChange,
  ...props
}) => {
  const classes = useStyles()

  return (
    <div className={[className, classes.root].join(' ')} {...props}>
      <input
        className={classes.input}
        style={{ display: 'none' }}
        id="studydocument-file-field"
        onChange={onChange}
        value={[]}
        type="file"
        multiple
      />
      <label htmlFor="studydocument-file-field">
        <Button variant="contained" color="secondary" component="span">
          <FormattedMessage id="studydocuments.uploadFileButton" />
        </Button>
      </label>
      <span className={classes.hint}>
        <FormattedMessage id="studydocuments.uploadFileHint" />
      </span>
    </div>
  )
}

StudydocumentFormFileUploadButton.propTypes = {
  /** @ignore */
  className: PropTypes.string,
  /** Callback when the field value has changed. */
  onChange: PropTypes.func.isRequired,
}

export default StudydocumentFormFileUploadButton
