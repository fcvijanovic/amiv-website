import React from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { useIntl } from 'react-intl'
import { SelectTextField } from 'amiv-react-components'

const StudydocumentFormSelectTextField = ({
  name,
  value,
  onChange,
  ...props
}) => {
  const summary = useSelector(state => state.studydocuments.editForm.summary)
  const intl = useIntl()

  return (
    <SelectTextField
      name={name}
      onChange={e => {
        const { value: newValue } = e.target
        onChange({
          name,
          value: newValue,
          isValid: !newValue || newValue.length > 0,
        })
      }}
      value={value || ''}
      label={intl.formatMessage({ id: `studydocuments.${name}` })}
      createEntryText={intl.formatMessage({
        id: 'studydocuments.createNewEntry',
      })}
      createEntryTextShort={intl.formatMessage({
        id: 'studydocuments.createNewEntryLabel',
      })}
      options={Object.keys(summary[name] || {})}
      {...props}
    />
  )
}

StudydocumentFormSelectTextField.propTypes = {
  /** Name of the property. */
  name: PropTypes.string.isRequired,
  /** Value of the field. */
  value: PropTypes.string,
  /** Callback when the field value has changed. */
  onChange: PropTypes.func.isRequired,
}

export default StudydocumentFormSelectTextField
