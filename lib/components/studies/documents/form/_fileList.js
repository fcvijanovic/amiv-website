import React from 'react'
import PropTypes from 'prop-types'
import fileSize from 'filesize'
import { makeStyles } from '@material-ui/styles'
import { FileTypeIcon } from 'amiv-react-components'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
    },
    row: {
      display: 'flex',
      flexWrap: 'nowrap',
      justifyContent: 'space-between',
      padding: '1em 0',
      textAlign: 'left',
    },
    info: {
      display: 'flex',
      flexWrap: 'wrap',
      flexGrow: 0.8,
      justifyContent: 'flex-start',
    },
    documentIcon: {
      marginRight: '.5em',
    },
    documentName: {
      marginLeft: '.25em',
      textAlign: 'left',
    },
    documentSize: {
      display: 'inline-block',
      color: theme.palette.info.main,
      marginLeft: '1em',
    },
    noFiles: {
      textAlign: 'center',
      color: theme.palette.info.main,
      margin: '3em 0',
    },
  }),
  { name: 'studydocumentExistingFileList' }
)

const StudydocumentExistingFileList = ({
  files,
  className,
  placeholderText,
  renderActions,
  ...props
}) => {
  const classes = useStyles()

  return (
    <div className={[className, classes.root].join(' ')} {...props}>
      {files.length > 0
        ? files.map((item, index) => {
            const filename = item.info ? item.info.name : item.file.name
            const length = item.info ? item.info.length : item.file.size
            const content_type = item.info
              ? item.info.content_type
              : item.file.type

            return (
              <div key={filename} className={classes.row}>
                <div className={classes.info}>
                  <FileTypeIcon
                    className={classes.documentIcon}
                    mimeType={content_type}
                  />
                  <span className={classes.documentName}>{filename}</span>
                  <span className={classes.documentSize}>
                    {fileSize(length)}
                  </span>
                </div>
                {renderActions && renderActions(item, index)}
              </div>
            )
          })
        : placeholderText && (
            <div className={classes.noFiles}>{placeholderText}</div>
          )}
    </div>
  )
}

StudydocumentExistingFileList.propTypes = {
  /** Array of file objects from API. */
  files: PropTypes.array.isRequired,
  /** Placeholder text shown when no file is listed. */
  placeholderText: PropTypes.string,
  /** Function to render the actions of an item.  */
  renderActions: PropTypes.func,
  /** @ignore */
  className: PropTypes.string,
}

export default StudydocumentExistingFileList
