import React from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import { FormControl, InputLabel, Select } from '@material-ui/core'

const DEPARTMENTS = [
  'itet',
  'mavt',
  'arch',
  'baug',
  'bsse',
  'infk',
  'matl',
  'biol',
  'chab',
  'math',
  'phys',
  'erdw',
  'usys',
  'hest',
  'mtec',
  'gess',
]

const StudydocumentFormDepartmentField = ({ value, onChange, ...props }) => {
  const intl = useIntl()

  return (
    <FormControl {...props}>
      <InputLabel>
        <FormattedMessage id="studydocuments.department" />
      </InputLabel>
      <Select
        native
        name="department"
        value={value || ''}
        onChange={e => {
          const { value: newValue } = e.target
          onChange({
            name: 'department',
            value: newValue,
            isValid: true,
          })
        }}
        inputProps={{
          'aria-label': intl.formatMessage({
            id: 'studydocuments.department',
          }),
        }}
      >
        <option value=""></option>
        {DEPARTMENTS.map(department => (
          <option key={department} value={department}>
            {`D-${department.toUpperCase()}`}
          </option>
        ))}
      </Select>
    </FormControl>
  )
}

StudydocumentFormDepartmentField.propTypes = {
  /** Value of the field. */
  value: PropTypes.string,
  /** Callback when the field value has changed. */
  onChange: PropTypes.func.isRequired,
}

export default StudydocumentFormDepartmentField
