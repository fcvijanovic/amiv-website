import React from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import { FormControl, InputLabel, Select } from '@material-ui/core'

const SEMESTERS = ['1', '2', '3', '4', '5+']

const StudydocumentFormSemesterField = ({ value, onChange, ...props }) => {
  const intl = useIntl()

  return (
    <FormControl {...props}>
      <InputLabel>
        <FormattedMessage id="studydocuments.semester" />
      </InputLabel>
      <Select
        native
        name="semester"
        value={value || ''}
        onChange={e => {
          const { value: newValue } = e.target
          onChange({
            name: 'semester',
            value: newValue || null,
            isValid: true,
          })
        }}
        inputProps={{
          'aria-label': intl.formatMessage({
            id: 'studydocuments.semester',
          }),
        }}
      >
        <option value=""></option>
        {SEMESTERS.map(semester => (
          <option key={semester} value={semester}>
            {intl.formatMessage({
              id: `studydocuments.semester${semester}`,
            })}
          </option>
        ))}
      </Select>
    </FormControl>
  )
}

StudydocumentFormSemesterField.propTypes = {
  /** Value of the field. */
  value: PropTypes.string,
  /** Callback when the field value has changed. */
  onChange: PropTypes.func.isRequired,
}

export default StudydocumentFormSemesterField
