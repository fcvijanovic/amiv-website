import React from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import { FormControl, InputLabel, Select } from '@material-ui/core'

const DOCUMENT_TYPES = [
  'exams',
  'oral exams',
  'cheat sheets',
  'study guides',
  'lecture documents',
  'exercises',
]

const StudydocumentFormTypeField = ({ value, onChange, ...props }) => {
  const intl = useIntl()

  return (
    <FormControl {...props}>
      <InputLabel>
        <FormattedMessage id="studydocuments.type" />
      </InputLabel>
      <Select
        native
        name="type"
        value={value || ''}
        onChange={e => {
          const { value: newValue } = e.target
          onChange({
            name: 'type',
            value: newValue,
            isValid: true,
          })
        }}
        inputProps={{
          'aria-label': intl.formatMessage({ id: 'studydocuments.type' }),
        }}
      >
        <option value=""></option>
        {DOCUMENT_TYPES.map(type => (
          <option key={type} value={type}>
            {intl.formatMessage({ id: `studydocuments.type.${type}` })}
          </option>
        ))}
      </Select>
    </FormControl>
  )
}

StudydocumentFormTypeField.propTypes = {
  /** Value of the field. */
  value: PropTypes.string,
  /** Callback when the field value has changed. */
  onChange: PropTypes.func.isRequired,
}

export default StudydocumentFormTypeField
