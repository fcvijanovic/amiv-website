import React from 'react'
import PropTypes from 'prop-types'
import { useIntl } from 'react-intl'
import { TextField } from '@material-ui/core'

const StudydocumentFormCourseYearField = ({ value, onChange, ...props }) => {
  const intl = useIntl()

  return (
    <TextField
      name="course_year"
      label={intl.formatMessage({ id: 'studydocuments.courseYear' })}
      value={value || ''}
      type="number"
      onChange={e => {
        const { value: newValue } = e.target
        onChange({
          name: 'course_year',
          value: newValue,
          isValid: true,
        })
      }}
      {...props}
    />
  )
}

StudydocumentFormCourseYearField.propTypes = {
  /** Value of the field. */
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  /** Callback when the field value has changed. */
  onChange: PropTypes.func.isRequired,
}

export default StudydocumentFormCourseYearField
