import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import debounce from 'debounce'
import { Button } from '@material-ui/core'
import { SearchField } from 'amiv-react-components'

import { STUDYDOCUMENTS } from 'lib/store/studydocuments/constants'
import { setQueryFromFilterValues } from 'lib/store/studydocuments/actions'
import { setFilterValue, resetFilterValues } from 'lib/store/common/actions'

import FilterView from '../../filteredListPage/filter'
import SelectFilterField from '../../general/selectFilterField'

const StudydocumentsFilter = ({ debounceTime, ...props }) => {
  const values = useSelector(state => state.studydocuments.filterValues)
  const summary = useSelector(state => state.studydocuments.default.summary)
  const dispatch = useDispatch()
  const router = useRouter()
  const intl = useIntl()

  const _loadFilterOptions = (fieldSummary, itemTransformer = item => item) => {
    if (fieldSummary) {
      return Object.keys(fieldSummary).sort().map(itemTransformer)
    }
    return []
  }

  // Prepare options for the select fields
  const departmentOptions = useCallback(
    () =>
      _loadFilterOptions(summary.department, item => ({
        value: item,
        label: `D-${item.toUpperCase()}`,
      })),
    [summary.department]
  )
  const semesterOptions = useCallback(
    () =>
      _loadFilterOptions(summary.semester, item => ({
        value: item,
        label: intl.formatMessage({ id: `studydocuments.semester${item}` }),
      })),
    [summary.semester]
  )
  const lectureOptions = useCallback(
    () => _loadFilterOptions(summary.lecture),
    [summary.lecture]
  )
  const professorOptions = useCallback(
    () => _loadFilterOptions(summary.professor),
    [summary.professor]
  )
  const typeOptions = useCallback(
    () =>
      _loadFilterOptions(summary.type, item => ({
        value: item,
        label: intl.formatMessage({ id: `studydocuments.type.${item}` }),
      })),
    [summary.type]
  )

  const setPathIfNeeded = () => {
    router.push('/studies/documents')
  }

  const debouncedSetQueryFromFilterValues = React.useCallback(
    debounce(() => {
      dispatch(setQueryFromFilterValues())
      setPathIfNeeded()
    }, debounceTime),
    [dispatch, debounceTime]
  )

  const filterViewChangeHandler = ({ name, value }) => {
    dispatch(setFilterValue(STUDYDOCUMENTS, { name, value }))
    debouncedSetQueryFromFilterValues()
  }

  const filterViewResetHandler = () => {
    dispatch(resetFilterValues(STUDYDOCUMENTS))
    dispatch(setQueryFromFilterValues(STUDYDOCUMENTS))
    setPathIfNeeded()
  }

  return (
    <FilterView
      values={values}
      onChange={filterViewChangeHandler}
      onReset={filterViewResetHandler}
      {...props}
    >
      <SearchField
        name="search"
        elevation={2}
        placeholder={intl.formatMessage({ id: 'studydocuments.search' })}
      />
      <SelectFilterField
        name="department"
        options={departmentOptions()}
        label={intl.formatMessage({ id: 'studydocuments.department' })}
      />
      <SelectFilterField
        name="semester"
        options={semesterOptions()}
        label={intl.formatMessage({ id: 'studydocuments.semester' })}
      />
      <SelectFilterField
        name="lecture"
        options={lectureOptions()}
        label={intl.formatMessage({ id: 'studydocuments.lecture' })}
      />
      <SelectFilterField
        name="professor"
        options={professorOptions()}
        label={intl.formatMessage({ id: 'studydocuments.professor' })}
      />
      <SelectFilterField
        name="type"
        options={typeOptions()}
        label={intl.formatMessage({ id: 'studydocuments.type' })}
      />
      <Button name="reset" type="reset">
        <FormattedMessage id="reset" />
      </Button>
      <Button
        name="upload"
        variant="contained"
        color="primary"
        onClick={() => router.push('/studies/documents/new')}
      >
        <FormattedMessage id="studydocuments.upload" />
      </Button>
    </FilterView>
  )
}

StudydocumentsFilter.propTypes = {
  /** Debounce time applied on filter updates */
  debounceTime: PropTypes.number,
}

StudydocumentsFilter.defaultProps = {
  debounceTime: 500,
}

export default StudydocumentsFilter
