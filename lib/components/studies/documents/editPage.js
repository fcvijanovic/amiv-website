import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'

import rules_en from 'content/studies/documents/rules.en.md'
import rules_de from 'content/studies/documents/rules.de.md'
import Layout from '../../layout'
import StudydocumentForm from './form'
import TranslatedContent from '../../general/translatedContent'

const useStyles = makeStyles(
  theme => ({
    root: {
      margin: '4em 0',
    },
    title: {
      padding: '0 .42352em',
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      padding: 0,
    },
    form: {
      flexGrow: 0.5,
      width: '50%',
      padding: '2em 1em',
      position: 'relative',
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },

      '&::after': {
        content: '" "',
        display: 'block',
        position: 'absolute',
        backgroundColor: theme.palette.common.grey,
        width: '4px',
        top: '1em',
        bottom: 0,
        right: 0,
        borderRadius: '2px',
        overflow: 'hidden',
        [theme.breakpoints.down('md')]: {
          borderRadius: 0,
          width: 'unset',
          height: '4px',
          top: 'unset',
          bottom: 0,
          left: 0,
        },
      },
    },
    rules: {
      flexGrow: 0.5,
      width: '50%',
      padding: '0 2em',
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
  }),
  { name: 'studydocumentsEdit' }
)

const StudydocumentsEditPage = ({ studydocumentId }) => {
  const classes = useStyles()

  return (
    <Layout
      authenticatedOnly
      className={classes.root}
      seoProps={{ title: 'studydocuments.title' }}
    >
      <Typography className={classes.title} variant="h4">
        <FormattedMessage
          id={
            studydocumentId
              ? 'studydocuments.uploadTitle.edit'
              : 'studydocuments.uploadTitle.new'
          }
        />
      </Typography>
      <div className={classes.container}>
        <StudydocumentForm
          className={classes.form}
          studydocumentId={studydocumentId}
        />
        <TranslatedContent
          className={classes.rules}
          content={{ en: rules_en, de: rules_de }}
          noEscape
        />
      </div>
    </Layout>
  )
}

StudydocumentsEditPage.propTypes = {
  /** Studydocument id from path, if available */
  studydocumentId: PropTypes.string,
}

export default StudydocumentsEditPage
