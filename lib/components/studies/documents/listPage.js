import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import dynamic from 'next/dynamic'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'

import useFilteredList from 'lib/hooks/useFilteredList'
import { listLoadNextPage } from 'lib/store/common/actions'
import { STUDYDOCUMENTS } from 'lib/store/studydocuments/constants'

import Layout from '../../layout'
import StudydocumentsFilter from './filter'
import FilteredListLayout from '../../filteredListPage/layout'
import FilteredList from '../../filteredListPage/list'
import FilteredListItem from '../../filteredListPage/listItem'
import StudydocumentSummary from './summary'
import StudydocumentDetails from './details'
import StudydocumentsOralExamsDialog from './oralExamsDialog'
import StudydocumentsUploadHint from './documentUploadHint'

const StudydocumentsQuickFilter = dynamic(() => import('./quickFilter'), {
  ssr: false,
})

const useStyles = makeStyles(
  {
    root: {
      textAlign: 'center',
      paddingTop: '2em',
    },
  },
  { name: 'studydocuments' }
)

const StudydocumentsListPage = ({ studydocumentId: studydocumentIdProp }) => {
  // Selectors for all studydocuments lists
  const defaultList = useSelector(state => state.studydocuments.default)
  // Selector for selected item (loaded from path)
  const studydocuments = useSelector(state => state.studydocuments.items)
  const auth = useSelector(state => state.auth)

  const dispatch = useDispatch()
  const itemsReady = auth.isLoggedIn && defaultList.totalPages > 0
  const [ref, pinnedId, studydocumentId, itemExpandHandler] = useFilteredList(
    STUDYDOCUMENTS,
    'studies/documents',
    [defaultList],
    studydocuments,
    studydocumentIdProp,
    itemsReady
  )
  const classes = useStyles()

  useEffect(() => {
    if (
      auth.isLoggedIn &&
      defaultList.totalPages === 0 &&
      !defaultList.isPending
    ) {
      dispatch(listLoadNextPage(STUDYDOCUMENTS, { listName: 'default' }))
    }
  }, [defaultList, auth.isLoggedIn])

  // Helper functions for shown lists and list items
  const isActive = item => item === studydocumentId
  const hasMorePagesToLoad = list => list.lastPageLoaded < list.totalPages
  const loadMoreLabel = list => (list.error ? 'loadMoreError' : 'loadMore')
  const loadMore = list => {
    dispatch(listLoadNextPage(STUDYDOCUMENTS, { listName: list }))
  }

  const placeholder = (
    <React.Fragment>
      {Array.from(Array(3)).map((_, i) => (
        <FilteredListItem key={i} disabled>
          <StudydocumentSummary studydocumentId={null} />
        </FilteredListItem>
      ))}
    </React.Fragment>
  )

  return (
    <Layout
      authenticatedOnly
      authenticatedReason={
        <FormattedMessage id="studydocuments.accessDenied" />
      }
      className={classes.root}
      seoProps={{ title: 'studydocuments.title' }}
    >
      <FilteredListLayout>
        <StudydocumentsFilter />
        {/* QuickFilter(tm) */}
        <StudydocumentsQuickFilter />

        {/* Hint for oral exam protocols */}
        <StudydocumentsOralExamsDialog />

        {/* Hint for uploading documents */}
        <StudydocumentsUploadHint />

        {/* Pinned studydocument */}
        {pinnedId && (
          <FilteredList>
            <FilteredListItem
              key={pinnedId}
              id={pinnedId}
              ref={isActive(pinnedId) ? ref : undefined}
              expanded={isActive(pinnedId)}
              onChange={itemExpandHandler}
            >
              <StudydocumentSummary studydocumentId={pinnedId} />
              <StudydocumentDetails studydocumentId={pinnedId} />
            </FilteredListItem>
          </FilteredList>
        )}

        {/* All loaded studydocuments for given filter values. */}
        <FilteredList
          placeholder={placeholder}
          showPlaceholder={defaultList.isPending}
          loadMoreLabel={loadMoreLabel(defaultList)}
          loadMore={
            hasMorePagesToLoad(defaultList) ? () => loadMore('default') : null
          }
        >
          {defaultList.items.map(item => (
            <FilteredListItem
              key={item}
              id={item}
              ref={isActive(item) ? ref : undefined}
              expanded={isActive(item)}
              onChange={itemExpandHandler}
            >
              <StudydocumentSummary studydocumentId={item} />
              <StudydocumentDetails studydocumentId={item} />
            </FilteredListItem>
          ))}
        </FilteredList>
      </FilteredListLayout>
    </Layout>
  )
}

StudydocumentsListPage.propTypes = {
  /** Studydocument id from path. */
  studydocumentId: PropTypes.string,
}

export default StudydocumentsListPage
