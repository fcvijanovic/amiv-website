import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import Alert from '@material-ui/lab/Alert'

import oral_info_en from '../../../../content/studies/documents/oral-exams.en.md'
import oral_info_de from '../../../../content/studies/documents/oral-exams.de.md'
import TranslatedContent from '../../general/translatedContent'

const useStyles = makeStyles(
  {
    root: {
      margin: '2em 0',
    },
    link: {
      cursor: 'pointer',

      '&:hover': {
        textDecoration: 'underline',
      },
    },
    dialogContent: {
      padding: '0 24px 16px 24px',
    },
  },
  { name: 'studydocumentsOralExamsDialog' }
)

const StudydocumentsOralExamsDialog = ({ className, ...props }) => {
  const [open, setOpen] = useState(false)
  const classes = useStyles()

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <React.Fragment>
      <Alert
        className={[classes.root, className].join(' ')}
        severity="info"
        {...props}
      >
        <div>
          <b>
            <FormattedMessage id="studydocuments.oralExamsCatchphrase" />
          </b>
          &nbsp;
          <a className={classes.link} onClick={handleOpen}>
            <FormattedMessage id="studydocuments.oralExamsLink" />
          </a>
        </div>
      </Alert>
      <Dialog
        onClose={handleClose}
        aria-labelledby="oral-dialog-title"
        open={open}
      >
        <DialogTitle id="oral-dialog-title">
          <FormattedMessage id="studydocuments.oralExamProtocols" />
        </DialogTitle>
        <DialogContent>
          <TranslatedContent
            content={{ en: oral_info_en, de: oral_info_de }}
            noEscape
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            <FormattedMessage id="close" />
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}

StudydocumentsOralExamsDialog.propTypes = {
  /** @ignore */
  className: PropTypes.string,
}

export default StudydocumentsOralExamsDialog
