import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { useIntl, FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import { makeStyles } from '@material-ui/styles'
import { Toolbar, Button, NoSsr } from '@material-ui/core'
import fileSize from 'filesize'
import { FileTypeIcon, CopyButton } from 'amiv-react-components'

import { deleteItem } from 'lib/store/common/actions'
import { STUDYDOCUMENTS } from 'lib/store/studydocuments/constants'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      textAlign: 'left',
      width: '100%',
    },
    documents: {
      display: 'none',
      padding: '16px',
      [theme.breakpoints.down('md')]: {
        display: 'block',
      },
    },
    documentLink: {
      display: 'inline-block',
      color: theme.palette.text.primary,
      borderRadius: '4px',
      padding: '4px',

      '&:not(:last-child)': {
        marginRight: '1em',
      },
      '&:hover': {
        backgroundColor: theme.palette.common.grey,
      },
      '& span': {
        display: 'inline-block',
        position: 'relative',
        bottom: '-2px',
      },
    },
    documentIcon: {
      verticalAlign: 'middle',
    },
    documentName: {
      marginLeft: '.25em',
    },
    documentSize: {
      opacity: '.5',
      marginLeft: '1em',
    },
    toolbar: {
      width: '100%',
      padding: '0 .4em',

      '& > *': {
        marginLeft: '.5em',

        '&:last-child': {
          marginRight: '.5em',
        },
      },
    },
    toolbarSeparator: {
      flexGrow: 1,
    },
  }),
  { name: 'studydocumentsDetails' }
)

const StudydocumentDetails = ({ studydocumentId, ...props }) => {
  const studydocument = useSelector(
    state => state.studydocuments.items[studydocumentId]
  )
  const [confirmDeletion, setConfirmDeletion] = useState(false)
  const [deletePending, setDeletePending] = useState(false)
  const dispatch = useDispatch()
  const router = useRouter()
  const classes = useStyles()
  const intl = useIntl()

  // Do not render anything when studydocument is not loaded (yet)
  if (!studydocument || !studydocument.data) return null

  const { data } = studydocument
  const canEdit =
    data._links &&
    data._links.self &&
    data._links.self.methods &&
    data._links.self.methods.includes('PATCH')
  const canDelete =
    data._links &&
    data._links.self &&
    data._links.self.methods &&
    data._links.self.methods.includes('DELETE')

  return (
    <div className={[classes.root].join(' ')} {...props}>
      <div className={classes.documents}>
        {data.files.map(({ file, name, content_type, length }) => {
          const label =
            name.length <= 18
              ? name
              : `${name.substr(0, 10)}\u2026${name.substr(name.length - 8)}`
          return (
            <a
              key={name}
              className={classes.documentLink}
              href={`${file}/${name}`}
              target="_blank"
              rel="noopener noreferrer"
              onClick={e => e.stopPropagation()}
            >
              <FileTypeIcon
                className={classes.documentIcon}
                mimeType={content_type}
              />
              <span className={classes.documentName}>{label}</span>
              <span className={classes.documentSize}>{fileSize(length)}</span>
            </a>
          )
        })}
      </div>
      <Toolbar className={classes.toolbar} variant="dense">
        {/* Buttons on the LEFT side */}
        {canEdit && (
          <Button
            onClick={() => router.push(`/studies/documents/${data._id}/edit`)}
          >
            <FormattedMessage id="studydocuments.actions.edit" />
          </Button>
        )}
        {canDelete && !confirmDeletion && (
          <Button
            disabled={deletePending}
            onClick={() => setConfirmDeletion(true)}
          >
            <FormattedMessage id="studydocuments.actions.delete" />
          </Button>
        )}
        {canDelete && confirmDeletion && (
          <React.Fragment>
            <Button onClick={() => setConfirmDeletion(false)}>
              <FormattedMessage id="studydocuments.actions.cancel" />
            </Button>
            <Button
              color="secondary"
              variant="outlined"
              onClick={() => {
                setConfirmDeletion(false)
                setDeletePending(true)
                dispatch(
                  deleteItem(STUDYDOCUMENTS, { id: data._id, etag: data._etag })
                )
                  .then(() => {
                    setDeletePending(false)
                    router.push('/studies/documents')
                  })
                  .catch(() => {
                    setDeletePending(false)
                  })
              }}
            >
              <FormattedMessage id="studydocuments.actions.confirm" />
            </Button>
          </React.Fragment>
        )}
        <span className={classes.toolbarSeparator} />
        {/* Buttons on the RIGHT side */}
        <NoSsr>
          <CopyButton
            value={`${window.location.origin}/${intl.locale}/studies/documents/${data._id}`}
          >
            <FormattedMessage id="copyDirectLink" />
          </CopyButton>
        </NoSsr>
      </Toolbar>
    </div>
  )
}

StudydocumentDetails.propTypes = {
  /** Studydocument id */
  studydocumentId: PropTypes.string,
}

export default StudydocumentDetails
