import React from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import { useSelector } from 'react-redux'
import filesize from 'filesize'
import { makeStyles } from '@material-ui/styles'
import Skeleton from '@material-ui/lab/Skeleton'
import { FileTypeIcon } from 'amiv-react-components'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      minHeight: '100px',
      textAlign: 'left',
      padding: '1.25em',
      [theme.breakpoints.down('sm')]: {
        gridTemplateAreas: 'content',
        gridTemplateColumns: '1fr',
      },

      '& > *': {
        width: '100%',
      },
    },
    title: {
      display: 'block',
      fontSize: '1.17em',
      margin: 0,

      '& span': {
        fontWeight: 'normal',
      },
    },
    properties: {
      width: '100%',
      marginTop: '.25em',

      '& > div': {
        display: 'inline-block',
        paddingBottom: '.25em',

        '& span::after': {
          content: "':'",
          paddingRight: '.5em',
        },
        '&::after': {
          content: "'/'",
          color: theme.palette.secondary.main,
          margin: '0 1em',
          [theme.breakpoints.down('sm')]: {
            content: "','",
            color: '#000',
            margin: '0 .5em 0 0',
          },
        },
        '&:last-of-type::after': {
          content: "''",
          margin: 0,
        },
      },
    },
    documents: {
      width: 'calc(100% + 8px)',
      position: 'relative',
      left: '-4px',
      [theme.breakpoints.down('md')]: {
        display: 'none',
      },
    },
    documentLink: {
      display: 'inline-block',
      color: theme.palette.text.primary,
      borderRadius: '4px',
      padding: '4px',

      '&:not(:last-child)': {
        marginRight: '1em',
      },
      '&:hover': {
        backgroundColor: theme.palette.common.grey,
      },
      '& span': {
        display: 'inline-block',
        position: 'relative',
        bottom: '-2px',
      },
    },
    documentIcon: {
      verticalAlign: 'middle',
    },
    documentName: {
      marginLeft: '.25em',
    },
    documentSize: {
      opacity: '.5',
      marginLeft: '1em',
    },
  }),
  { name: 'studydocumentsSummary' }
)

const StudydocumentSummary = ({ studydocumentId, ...props }) => {
  const studydocument = useSelector(
    state => state.studydocuments.items[studydocumentId]
  )
  const classes = useStyles()
  const intl = useIntl()

  if (!studydocument || !studydocument.data) {
    return (
      <div className={classes.root} {...props}>
        <div className={classes.content}>
          <Skeleton
            className={classes.title}
            height="2.5em"
            width="70%"
            variant="text"
            animation="wave"
          />
          <Skeleton height="2em" width="50%" variant="text" animation="wave" />
        </div>
      </div>
    )
  }

  const { data } = studydocument
  const title =
    data.type || data.lecture
      ? `${data.lecture ? data.lecture : ''} ${
          data.type
            ? intl.formatMessage({ id: `studydocuments.name.${data.type}` })
            : ''
        }`
      : null

  return (
    <div className={classes.root} {...props}>
      <h2 className={classes.title}>
        {title}
        <span>
          {title ? ' – ' : ''}
          {data.title}
        </span>
      </h2>
      <div className={classes.properties}>
        {data.course_year && <div>{data.course_year}</div>}
        {data.professor && <div>{data.professor}</div>}
        {data.author && (
          <div>
            <span>
              <FormattedMessage id="studydocuments.author" />
            </span>
            {data.author}
          </div>
        )}
        {data.semester && (
          <div>
            <FormattedMessage id={`studydocuments.semester${data.semester}`} />
          </div>
        )}
      </div>
      <div className={classes.documents}>
        {data.files.map(({ file, name, content_type, length }) => {
          const label =
            name.length <= 18
              ? name
              : `${name.substr(0, 10)}\u2026${name.substr(name.length - 8)}`
          return (
            <a
              key={name}
              className={classes.documentLink}
              href={`${file}/${name}`}
              target="_blank"
              rel="noopener noreferrer"
              onClick={e => e.stopPropagation()}
            >
              <FileTypeIcon
                className={classes.documentIcon}
                mimeType={content_type}
              />
              <span className={classes.documentName}>{label}</span>
              <span className={classes.documentSize}>{filesize(length)}</span>
            </a>
          )
        })}
      </div>
    </div>
  )
}

StudydocumentSummary.propTypes = {
  /** Studydocument id. */
  studydocumentId: PropTypes.string,
}

export default StudydocumentSummary
