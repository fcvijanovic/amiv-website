import React, { useCallback, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import { useSelector, useDispatch } from 'react-redux'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Autocomplete from '@material-ui/lab/Autocomplete'
import {
  Accordion,
  AccordionSummary,
  Button,
  AccordionDetails,
  Typography,
  Toolbar,
  Breadcrumbs,
  TextField,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { Spinner } from 'amiv-react-components'

import {
  STUDYDOCUMENTS,
  STUDYDOCUMENTS_QUICKFILTER_STEP_DEPARTMENT,
  STUDYDOCUMENTS_QUICKFILTER_STEP_LECTURE,
  STUDYDOCUMENTS_QUICKFILTER_STEP_TYPE,
} from 'lib/store/studydocuments/constants'
import {
  resetQuickFilter,
  setQuickFilterValues,
  loadQuickFilterSummary,
  setQueryFromFilterValues,
} from 'lib/store/studydocuments/actions'
import { setFilterValues } from 'lib/store/common/actions'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      marginBottom: '1em',
      backgroundColor: theme.palette.common.grey,
      transition: 'background 300ms cubic-bezier(.4, 0, .2, 1) !important',
    },
    rounded: {
      overflow: 'hidden',
      borderRadius: '4px',
    },
    expanded: {
      backgroundColor: theme.palette.background.paper,
    },
    summaryRoot: {
      padding: '0 24px 0 0',
    },
    summaryExpanded: {
      minHeight: '0 !important',
    },
    summaryContent: {
      margin: '0 !important',
      paddingLeft: '1em',
    },
    detailsRoot: {
      borderTop: `1px dashed ${theme.palette.common.grey}`,
      flexWrap: 'wrap',
      textAlign: 'left',
      padding: '.5em 1em 1em',
      '& > *': {
        width: '100%',
      },
    },
    toolbarSeparator: {
      flexGrow: 1,
    },
    spinner: {
      width: '100%',
      height: '6em',
      position: 'relative',
    },
    stepSemester: {
      marginTop: '16px',
    },
    stepSemesterRow: {
      display: 'flex',
      alignItems: 'center',

      '& > div': {
        minWidth: '6em',
      },
    },
  }),
  { name: 'studydocumentsQuickFilter' }
)

const StudydocumentsQuickFilter = ({ ...props }) => {
  const { summary, filterValues, step, isPending, error } = useSelector(
    state => state.studydocuments.quickFilter
  )
  const [expanded, setExpanded] = useState(true)
  const dispatch = useDispatch()
  const classes = useStyles()
  const intl = useIntl()

  useEffect(() => {
    if (!summary && !isPending && !error) {
      dispatch(loadQuickFilterSummary())
    }
  }, [summary, isPending, error])

  const departments = useCallback(() => {
    if (summary && summary.department) {
      return Object.keys(summary.department).sort()
    }
    return []
  }, [summary])

  const lectures = useCallback(() => {
    if (summary && summary.lecture) {
      return Object.keys(summary.lecture).sort()
    }
    return []
  }, [summary])

  const types = useCallback(() => {
    if (summary && summary.type) {
      return Object.keys(summary.type).sort()
    }
    return []
  }, [summary])

  const DepartmentButton = ({
    department,
    values,
    ...departmentButtonProps
  }) => {
    return (
      <Button
        variant={department === 'all' ? 'outlined' : 'text'}
        onClick={() => {
          dispatch(
            setQuickFilterValues(
              {
                ...filterValues,
                department: values || [department],
              },
              STUDYDOCUMENTS_QUICKFILTER_STEP_LECTURE
            )
          )
        }}
        {...departmentButtonProps}
      >
        {department === 'all'
          ? intl.formatMessage({ id: 'studydocuments.departments.all' })
          : `D-${department.toUpperCase()}`}
      </Button>
    )
  }

  DepartmentButton.propTypes = {
    department: PropTypes.string.isRequired,
    values: PropTypes.array,
  }

  const TypeButton = ({ type, values, ...typeButtonProps }) => {
    return (
      <Button
        variant={type === 'all' ? 'outlined' : 'text'}
        onClick={() => {
          dispatch(
            setFilterValues(STUDYDOCUMENTS, {
              ...filterValues,
              type: values || [type],
            })
          )
          dispatch(setQueryFromFilterValues(STUDYDOCUMENTS))
          dispatch(resetQuickFilter())
          setExpanded(false)
        }}
        {...typeButtonProps}
      >
        {type === 'all'
          ? intl.formatMessage({ id: 'studydocuments.type.all' })
          : intl.formatMessage({ id: `studydocuments.type.${type}` })}
      </Button>
    )
  }

  TypeButton.propTypes = {
    type: PropTypes.string.isRequired,
    values: PropTypes.array,
  }

  let content = null

  if (isPending) {
    content = (
      <div className={classes.spinner}>
        <Spinner centered />
      </div>
    )
  } else if (error) {
    content = (
      <React.Fragment>
        <h3>
          <FormattedMessage id="studydocuments.quickfilter.loadingError" />
        </h3>
        <Button onClick={() => dispatch(loadQuickFilterSummary())}>
          <FormattedMessage id="retry" />
        </Button>
      </React.Fragment>
    )
  } else if (step === STUDYDOCUMENTS_QUICKFILTER_STEP_DEPARTMENT) {
    content = (
      <div>
        <DepartmentButton department="all" values={departments()} />
        {departments().map(department => (
          <DepartmentButton key={department} department={department} />
        ))}
      </div>
    )
  } else if (step === STUDYDOCUMENTS_QUICKFILTER_STEP_LECTURE) {
    content = (
      <Autocomplete
        options={lectures()}
        style={{ width: 300 }}
        onChange={(_, value) => {
          dispatch(
            setQuickFilterValues(
              {
                ...filterValues,
                lecture: [value],
              },
              STUDYDOCUMENTS_QUICKFILTER_STEP_TYPE
            )
          )
        }}
        renderInput={params => (
          <TextField
            {...params}
            label={intl.formatMessage({
              id: 'studydocuments.quickfilter.lecturePlaceholder',
            })}
            variant="outlined"
            fullWidth
          />
        )}
      />
    )
  } else if (step === STUDYDOCUMENTS_QUICKFILTER_STEP_TYPE) {
    // TODO
    content = (
      <div>
        <TypeButton type="all" values={types()} />
        {types().map(type => (
          <TypeButton key={type} type={type} />
        ))}
      </div>
    )
  } else {
    // step === STUDYDOCUMENTS_QUICKFILTER_STEP_SEMESTER is the default case
    content = (
      <div className={classes.stepSemester}>
        {['itet', 'mavt', 'other'].map(department => (
          <div key={department} className={classes.stepSemesterRow}>
            <div>
              {department !== 'other'
                ? `D-${department.toUpperCase()}`
                : department}
            </div>
            <div className={classes.setSemesterButtons}>
              {['1', '2', '3', '4', '5+'].map(semester => (
                <Button
                  key={semester}
                  onClick={() => {
                    if (department === 'other') {
                      dispatch(
                        setQuickFilterValues(
                          { semester: [semester] },
                          STUDYDOCUMENTS_QUICKFILTER_STEP_DEPARTMENT
                        )
                      )
                    } else {
                      dispatch(
                        setQuickFilterValues(
                          {
                            semester: [semester],
                            department: [department],
                          },
                          STUDYDOCUMENTS_QUICKFILTER_STEP_LECTURE
                        )
                      )
                    }
                  }}
                >
                  {semester}
                </Button>
              ))}
            </div>
          </div>
        ))}
      </div>
    )
  }

  let breadcrumbs = null

  if (step === STUDYDOCUMENTS_QUICKFILTER_STEP_DEPARTMENT) {
    breadcrumbs = (
      <Breadcrumbs>
        <Typography>
          {filterValues.semester
            .map(semester =>
              intl.formatMessage({ id: `studydocuments.semester${semester}` })
            )
            .join(', ')}
        </Typography>
        <Typography color="textPrimary">
          <FormattedMessage id="studydocuments.quickfilter.selectDepartment" />
        </Typography>
      </Breadcrumbs>
    )
  } else if (step === STUDYDOCUMENTS_QUICKFILTER_STEP_LECTURE) {
    breadcrumbs = (
      <Breadcrumbs>
        <Typography>
          {filterValues.semester
            .map(semester =>
              intl.formatMessage({ id: `studydocuments.semester${semester}` })
            )
            .join(', ')}
        </Typography>
        <Typography>
          {(() => {
            if (filterValues.department.length === 1) {
              return filterValues.department.map(
                department => `D-${department.toUpperCase()}`
              )
            }
            if (filterValues.department.length > 1) {
              return intl.formatMessage({
                id: `studydocuments.departments.otherLong`,
              })
            }
            return null
          })()}
        </Typography>
        <Typography color="textPrimary">
          <FormattedMessage id="studydocuments.quickfilter.selectLecture" />
        </Typography>
      </Breadcrumbs>
    )
  } else if (step === STUDYDOCUMENTS_QUICKFILTER_STEP_TYPE) {
    breadcrumbs = (
      <Breadcrumbs>
        <Typography>
          {filterValues.semester
            .map(semester =>
              intl.formatMessage({ id: `studydocuments.semester${semester}` })
            )
            .join(', ')}
        </Typography>
        <Typography>
          {(() => {
            if (filterValues.department.length === 1) {
              return filterValues.department.map(
                department => `D-${department.toUpperCase()}`
              )
            }
            if (filterValues.department.length > 1) {
              return intl.formatMessage({
                id: `studydocuments.departments.otherLong`,
              })
            }
            return null
          })()}
        </Typography>
        <Typography>{filterValues.lecture}</Typography>
        <Typography color="textPrimary">
          <FormattedMessage id="studydocuments.quickfilter.selectType" />
        </Typography>
      </Breadcrumbs>
    )
  } else {
    breadcrumbs = (
      <Breadcrumbs>
        <Typography color="textPrimary">
          <FormattedMessage id="studydocuments.quickfilter.selectSemester" />
        </Typography>
      </Breadcrumbs>
    )
  }

  return (
    <Accordion
      component="div"
      classes={{
        root: classes.root,
        rounded: classes.rounded,
        expanded: classes.expanded,
      }}
      expanded={expanded}
      onChange={(_, isExpanded) => setExpanded(isExpanded)}
      {...props}
    >
      <AccordionSummary
        classes={{
          root: classes.summaryRoot,
          expanded: classes.summaryExpanded,
          content: classes.summaryContent,
        }}
        expandIcon={<ExpandMoreIcon />}
      >
        <Typography>
          <FormattedMessage id="studydocuments.quickfilter.title" />
        </Typography>
      </AccordionSummary>
      <AccordionDetails classes={{ root: classes.detailsRoot }}>
        <Toolbar disableGutters>
          {breadcrumbs}
          <div className={classes.toolbarSeparator} />
          <Button onClick={() => dispatch(resetQuickFilter())}>
            <FormattedMessage id="reset" />
          </Button>
        </Toolbar>
        {content}
      </AccordionDetails>
    </Accordion>
  )
}

export default StudydocumentsQuickFilter
