import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'

import Image from '../general/staticImage'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'grid',
      marginBottom: '8em',
      gridGap: '2em',
      gridTemplateColumns: '1fr 1fr',
      gridTemplateAreas: "'title title' 'image description'",
      [theme.breakpoints.down('sm')]: {
        gridTemplateColumns: '1fr',
        gridTemplateAreas: "'title' 'image' 'description' !important",
      },

      '&:nth-child(even)': {
        gridTemplateAreas: "'title title' 'description image'",
      },
    },
    title: {
      gridArea: 'title',
      width: '100%',
      textAlign: 'center',
      marginBottom: '0.7em',
    },
    imageContainer: {
      gridArea: 'image',
    },
    image: {
      position: 'relative',
      borderRadius: '4px',
      overflow: 'hidden',
    },
    description: {
      gridArea: 'description',
      textAlign: 'justify',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
  }),
  { name: 'boardImageGroup' }
)

const BoardImageGroup = ({
  title,
  alt,
  image,
  ratioX,
  ratioY,
  children,
  className,
  ...props
}) => {
  const classes = useStyles()

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      {title && (
        <Typography variant="h3" className={classes.title}>
          {title}
        </Typography>
      )}
      <div className={classes.imageContainer}>
        <Image
          objectFit="contain"
          className={classes.image}
          src={image}
          alt={alt}
          ratioX={ratioX}
          ratioY={ratioY}
        />
      </div>
      <div className={classes.description}>{children}</div>
    </div>
  )
}

BoardImageGroup.propTypes = {
  /** Title visible above the image group */
  title: PropTypes.string,
  /** alt tag for image components */
  alt: PropTypes.string,
  /** URL to the image */
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  /** ratio of the image */
  ratioX: PropTypes.number,
  ratioY: PropTypes.number,
  /** Description element(s) */
  children: PropTypes.node,
  /** @ignore */
  className: PropTypes.string,
}

export default BoardImageGroup
