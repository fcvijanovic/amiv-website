import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Skeleton from '@material-ui/lab/Skeleton'
import { Box } from '@material-ui/core'
import { Spinner } from 'amiv-react-components'

import { getPublicConfig } from 'lib/utils/config'
import Img from '../general/dynamicImage'
import Link from '../general/link'

const useStyles = makeStyles(
  {
    root: {
      borderRadius: '4px',
      overflow: 'hidden',
    },
    placeholderText: {
      textAlign: 'center',
      width: '65%',
      '& > div': {
        display: 'inline-block',
        height: '2.5em',
      },
    },
  },
  { name: 'eventPoster' }
)

const EventPoster = ({ event, elevation, className, ...props }) => {
  const { apiUrl } = getPublicConfig()
  const classes = useStyles()
  const theme = useTheme()

  const loadingPlaceholder = (
    <React.Fragment>
      <Skeleton width="100%" height="100%" variant="rect" animation="pulse" />
      <Spinner centered background={theme.palette.common.white} elevation={2} />
    </React.Fragment>
  )

  const imageTag = (
    <Img
      src={event ? `${apiUrl}${event.img_poster.file}` : null}
      ratioX={1}
      ratioY={1.41421356237}
      alt={event ? event.title_de : null}
      loadingPlaceholder={loadingPlaceholder}
    />
  )

  return (
    <Box boxShadow={2} className={[classes.root].join(' ')} {...props}>
      {event ? <Link href={`/events/${event._id}`}>{imageTag}</Link> : imageTag}
    </Box>
  )
}

EventPoster.propTypes = {
  /** Event object */
  event: PropTypes.object,
  /** Specifies the size of the shadow around the card */
  elevation: PropTypes.number,
  /** Additional class for the outermost box */
  className: PropTypes.string,
}

EventPoster.defaultProps = {
  elevation: 0,
}

export default EventPoster
