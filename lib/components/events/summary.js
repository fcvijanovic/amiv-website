import React from 'react'
import { useIntl } from 'react-intl'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/styles'
import Skeleton from '@material-ui/lab/Skeleton'

import { getPublicConfig } from 'lib/utils/config'
import { translateMessage } from 'lib/utils'
import Img from '../general/dynamicImage'
import EventTime from './partials/time'
import EventPrice from './partials/price'
import EventFreeSpots from './partials/freeSpots'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'flex',
      width: '100%',
      minHeight: '150px',
    },
    image: {
      gridArea: 'image',
      display: 'block',
      width: '150px',
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
    content: {
      flexGrow: 1,
      textAlign: 'left',
      padding: '1em',
    },
    title: {
      padding: 0,
      margin: 0,
      marginBottom: '.25em',
    },
    catchphrase: {
      fontStyle: 'italic',
      marginBottom: '1em',
    },
    dates: {
      marginBottom: '.75em',
    },
    properties: {
      width: '100%',

      '& > div': {
        display: 'inline-block',

        '&:after': {
          content: "'/'",
          color: theme.palette.secondary.main,
          margin: '0 1em',
        },
        '&:last-of-type:after': {
          content: "''",
          margin: 0,
        },
      },
    },
  }),
  { name: 'eventSummary' }
)

const EventSummary = ({ eventId, ...props }) => {
  const { apiUrl } = getPublicConfig()
  const event = useSelector(state => state.events.items[eventId])
  const classes = useStyles()
  const intl = useIntl()

  if (!event || !event.data) {
    return (
      <div className={classes.root} {...props}>
        <div className={classes.image}>
          <Img ratioX={1} ratioY={1} />
        </div>
        <div className={classes.content}>
          <Skeleton
            className={classes.title}
            height="2.5em"
            width="70%"
            variant="text"
            animation="wave"
          />
          <Skeleton
            className={classes.catchphrase}
            height="2em"
            width="50%"
            variant="text"
            animation="wave"
          />
          <Skeleton
            className={classes.dates}
            height="1em"
            width="40%"
            variant="text"
            animation="wave"
          />
          <Skeleton
            className={classes.properties}
            height="1em"
            width="30%"
            variant="text"
            animation="wave"
          />
        </div>
      </div>
    )
  }

  const { data } = event
  const imageUrl = data.img_thumbnail
    ? `${apiUrl}${data.img_thumbnail.file}`
    : '/logos/amiv-wheel.svg'
  const title = translateMessage({ en: data.title_en, de: data.title_de }, intl)
  const catchphrase = translateMessage(
    { en: data.catchphrase_en, de: data.catchphrase_de },
    intl
  )

  return (
    <div className={classes.root} {...props}>
      <div className={classes.image}>
        <Img src={imageUrl} ratioX={1} ratioY={1} alt={title} />
      </div>
      <div className={classes.content}>
        <h2 className={classes.title}>{title}</h2>
        <div className={classes.catchphrase}>{catchphrase}</div>
        <div className={classes.dates}>
          <EventTime time_start={data.time_start} time_end={data.time_end} />
        </div>
        <div className={classes.properties}>
          <EventPrice className={classes.property} price={data.price} />
          {data.spots > 0 && (
            <EventFreeSpots
              className={classes.property}
              spots={data.spots}
              signup_count={data.signup_count}
            />
          )}
          {data.location && (
            <div className={classes.property}>{data.location}</div>
          )}
        </div>
      </div>
    </div>
  )
}

EventSummary.propTypes = {
  /** Event id. */
  eventId: PropTypes.string,
}

export default EventSummary
