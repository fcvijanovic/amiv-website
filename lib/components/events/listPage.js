import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import {
  Typography,
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from '@material-ui/core'

import { listLoadNextPage, listLoadAllPages } from 'lib/store/common/actions'
import { EVENTS } from 'lib/store/events/constants'

import useFilteredList from 'lib/hooks/useFilteredList'
import Layout from '../layout'
import EventsFilter from './filter'
import FilteredListLayout from '../filteredListPage/layout'
import FilteredList from '../filteredListPage/list'
import FilteredListItem from '../filteredListPage/listItem'
import EventSummary from './summary'
import EventDetails from './details'

const useStyles = makeStyles(
  theme => ({
    root: {
      textAlign: 'center',
      paddingTop: '2em',
    },
    calendarPanel: {
      marginBottom: '2em',
      overflow: 'hidden',
      borderRadius: '4px',
    },
    calendarSummary: {
      backgroundColor: theme.palette.common.grey,
    },
    calendarDetails: {
      padding: 0,
    },
  }),
  { name: 'events' }
)

const EventsListPage = ({ eventId: eventIdProp }) => {
  // Selectors for all event lists
  const upcomingWithOpenRegistration = useSelector(
    state => state.events.upcomingWithOpenRegistration
  )
  const upcomingWithoutOpenRegistration = useSelector(
    state => state.events.upcomingWithoutOpenRegistration
  )
  const past = useSelector(state => state.events.past)
  // Selector for selected item (loaded from path)
  const events = useSelector(state => state.events.items)

  const dispatch = useDispatch()
  const itemsReady =
    upcomingWithOpenRegistration.totalPages > 0 &&
    upcomingWithoutOpenRegistration.totalPages > 0 &&
    past.totalPages > 0
  const [ref, pinnedId, eventId, itemExpandHandler] = useFilteredList(
    EVENTS,
    'events',
    [upcomingWithOpenRegistration, upcomingWithoutOpenRegistration, past],
    events,
    eventIdProp,
    itemsReady
  )
  const { locale } = useRouter()
  const classes = useStyles()

  useEffect(() => {
    if (
      upcomingWithOpenRegistration.totalPages === 0 &&
      !upcomingWithOpenRegistration.isPending
    ) {
      dispatch(
        listLoadAllPages(EVENTS, { listName: 'upcomingWithOpenRegistration' })
      )
    }
  }, [upcomingWithOpenRegistration])

  useEffect(() => {
    if (
      upcomingWithoutOpenRegistration.totalPages === 0 &&
      !upcomingWithoutOpenRegistration.isPending
    ) {
      dispatch(
        listLoadAllPages(EVENTS, {
          listName: 'upcomingWithoutOpenRegistration',
        })
      )
    }
  }, [upcomingWithoutOpenRegistration])

  useEffect(() => {
    if (past.totalPages === 0 && !past.isPending) {
      dispatch(listLoadNextPage(EVENTS, { listName: 'past' }))
    }
  }, [past])

  // Helper functions for shown lists and list items
  const isActive = item => item === eventId
  const hasMorePagesToLoad = list => list.lastPageLoaded < list.totalPages
  const loadMoreLabel = list => (list.error ? 'loadMoreError' : 'loadMore')
  const loadMore = list => {
    dispatch(listLoadNextPage(EVENTS, { listName: list }))
  }

  const placeholder = (
    <FilteredListItem disabled>
      <EventSummary eventId={null} />
    </FilteredListItem>
  )

  return (
    <Layout className={classes.root} seoProps={{ title: 'events.title' }}>
      <FilteredListLayout>
        <EventsFilter />

        {/* Event Calendar */}
        <Accordion className={classes.calendarPanel}>
          <AccordionSummary
            classes={{
              root: classes.calendarSummary,
            }}
            expandIcon={<ExpandMoreIcon />}
          >
            <Typography className={classes.heading}>
              <FormattedMessage id="events.agenda" />
            </Typography>
          </AccordionSummary>
          <AccordionDetails classes={{ root: classes.calendarDetails }}>
            <iframe
              src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;bgcolor=%23FFFFFF&amp;src=mdk91hfvr18q8rrlh3sedlhgvo%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=Europe%2FZurich"
              style={{ width: '100%', height: '550px', borderWidth: 0 }}
              frameBorder="0"
              scrolling="no"
            />
          </AccordionDetails>
        </Accordion>

        {/* Pinned event */}
        {pinnedId && (
          <FilteredList>
            <FilteredListItem
              key={pinnedId}
              id={pinnedId}
              ref={isActive(pinnedId) ? ref : undefined}
              expanded={isActive(pinnedId)}
              onChange={itemExpandHandler}
              href={`/${locale}/events${
                !isActive(pinnedId) ? `/${pinnedId}` : ''
              }`}
            >
              <EventSummary eventId={pinnedId} />
              <EventDetails eventId={pinnedId} />
            </FilteredListItem>
          </FilteredList>
        )}

        {/* Upcoming events with open registration */}
        <FilteredList
          title="events.headers.openRegistration"
          placeholder={placeholder}
          showPlaceholder={upcomingWithOpenRegistration.isPending}
          loadMoreLabel={loadMoreLabel(upcomingWithOpenRegistration)}
          loadMore={
            hasMorePagesToLoad(upcomingWithOpenRegistration)
              ? () => loadMore('upcomingWithOpenRegistration')
              : null
          }
        >
          {upcomingWithOpenRegistration.items.map(item => (
            <FilteredListItem
              key={item}
              id={item}
              ref={isActive(item) ? ref : undefined}
              expanded={isActive(item)}
              onChange={itemExpandHandler}
              href={`/${locale}/events${!isActive(item) ? `/${item}` : ''}`}
            >
              <EventSummary eventId={item} />
              <EventDetails eventId={item} />
            </FilteredListItem>
          ))}
        </FilteredList>

        {/* Upcoming events with closed registration */}
        <FilteredList
          title="events.headers.upcoming"
          placeholder={placeholder}
          showPlaceholder={upcomingWithoutOpenRegistration.isPending}
          loadMoreLabel={loadMoreLabel(upcomingWithoutOpenRegistration)}
          loadMore={
            hasMorePagesToLoad(upcomingWithoutOpenRegistration)
              ? () => loadMore('upcomingWithoutOpenRegistration')
              : null
          }
        >
          {upcomingWithoutOpenRegistration.items.map(item => (
            <FilteredListItem
              key={item}
              id={item}
              ref={isActive(item) ? ref : undefined}
              expanded={isActive(item)}
              onChange={itemExpandHandler}
              href={`/${locale}/events${!isActive(item) ? `/${item}` : ''}`}
            >
              <EventSummary eventId={item} />
              <EventDetails eventId={item} />
            </FilteredListItem>
          ))}
        </FilteredList>

        {/* Past events */}
        <FilteredList
          title="events.headers.past"
          placeholder={placeholder}
          showPlaceholder={past.isPending}
          loadMoreLabel={loadMoreLabel(past)}
          loadMore={hasMorePagesToLoad(past) ? () => loadMore('past') : null}
        >
          {past.items.map(item => (
            <FilteredListItem
              key={item}
              id={item}
              ref={isActive(item) ? ref : undefined}
              expanded={isActive(item)}
              onChange={itemExpandHandler}
              href={`/${locale}/events${!isActive(item) ? `/${item}` : ''}`}
            >
              <EventSummary eventId={item} />
              <EventDetails eventId={item} />
            </FilteredListItem>
          ))}
        </FilteredList>
      </FilteredListLayout>
    </Layout>
  )
}

EventsListPage.propTypes = {
  /** Event id from path. */
  eventId: PropTypes.string,
}

export default EventsListPage
