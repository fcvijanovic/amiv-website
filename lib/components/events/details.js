import React, { useEffect, useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { useSelector, useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/styles'
import {
  Toolbar,
  Button,
  TextField,
  Checkbox,
  FormControlLabel,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  DialogContentText,
  NoSsr,
} from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import {
  Spinner,
  CopyButton,
  JsonSchemaForm,
  JsonSchemaFormFieldGroup,
} from 'amiv-react-components'

import { dateFormatterConfig } from 'config'

import TranslatedContent from '../general/translatedContent'
import terms_en from '../../../content/termsAndConditions.en.md'
import terms_de from '../../../content/termsAndConditions.de.md'
import { authLoginStart } from '../../store/auth/actions'
import {
  loadSignupForEvent,
  postSignup,
  patchSignup,
  deleteSignup,
} from '../../store/events/actions'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'grid',
      gridTemplateColumns: 'auto 1fr',
      gridTemplateAreas:
        "'notification notification' 'additionalFields description' 'actionbar actionbar'",
      width: '100%',
      alignContent: 'center',

      [theme.breakpoints.down('md')]: {
        gridTemplateAreas:
          "'notification notification' 'description description' 'additionalFields additionalFields' 'actionbar actionbar'",
      },
    },
    notification: {
      gridArea: 'notification',
      width: 'calc(100% - 2em)',
      margin: '1em',
    },
    notificationColored: {
      color: theme.palette.secondary.main,
    },
    additionalFields: {
      gridArea: 'additionalFields',
      transition: 'width 1s ease',

      '& > *': {
        width: '100%',
        margin: '.5em 0',
      },
    },
    additionalFieldsVisible: {
      width: '350px',
      padding: '1em',
      position: 'relative',
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },

      '&::after': {
        content: '" "',
        display: 'block',
        position: 'absolute',
        backgroundColor: theme.palette.common.grey,
        width: '4px',
        top: '1em',
        bottom: 0,
        right: 0,
        borderRadius: '2px',
        overflow: 'hidden',
        [theme.breakpoints.down('md')]: {
          borderRadius: 0,
          width: 'unset',
          height: '4px',
          bottom: 'unset',
          left: 0,
          top: 0,
        },
      },
    },
    description: {
      gridArea: 'description',
      padding: '1em',
      textAlign: 'left',
      order: 2,
      [theme.breakpoints.down('md')]: {
        width: '100%',
        order: 1,
      },
    },
    divider: {
      gridArea: 'divider',
    },
    toolbar: {
      gridArea: 'actionbar',
      width: '100%',
      padding: '0 .4em',
      order: 3,

      '& > *': {
        marginLeft: '.5em',

        '&:last-child': {
          marginRight: '.5em',
        },
      },
    },
    toolbarSeparator: {
      flexGrow: 1,
    },
  }),
  { name: 'eventDetails' }
)

const EventDetails = ({ eventId, ...props }) => {
  const { dateFormatter } = dateFormatterConfig
  const event = useSelector(state => state.events.items[eventId])
  const signup = useSelector(state => state.events.signups[eventId])
  const [additionalValues, setAdditionalValues] = useState({})
  const [isAdditionalValuesValid, setIsAdditionalValuesValid] = useState(false)
  const [isSignupLoaded, setIsSignupLoaded] = useState(false)
  const [email, setEmail] = useState('')
  const [termsAccepted, setTermsAccepted] = useState(false)
  const [termsModalOpen, setTermsModalOpen] = useState(false)
  const [isEmailValid, setIsEmailValid] = useState(true)
  const [storedNotification, setStoredNotification] = useState(null)
  const auth = useSelector(state => state.auth)

  const dispatch = useDispatch()
  const classes = useStyles()
  const intl = useIntl()

  useEffect(() => {
    if (!isSignupLoaded) {
      if (auth.isLoggedIn && (!signup || (!signup.isPending && !signup.data))) {
        dispatch(loadSignupForEvent({ eventId })).then(() =>
          setIsSignupLoaded(true)
        )
      } else if (signup && signup.data) {
        setIsSignupLoaded(true)
        setTermsAccepted(true)
      }
    }
  }, [auth.isLoggedIn, signup])

  // Do not render anything when event is not loaded (yet)
  if (!event || !event.data) return null

  const { data } = event

  const now = new Date()
  const hasOpenRegistration =
    data.time_register_start &&
    data.time_register_end &&
    now >= new Date(data.time_register_start) &&
    now <= new Date(data.time_register_end)
  const hasAdditionalFields = !!data.additional_fields
  const hasEmailField =
    hasOpenRegistration && data.allow_email_signup && !auth.isLoggedIn

  let signupButtons = null
  let additionalFields = null
  let showTermsCheckbox = false
  let emailField = null
  let termsCheckbox = null
  let termsDialog = null
  let notification = null

  // --------------------
  // Prepare notification
  if (storedNotification && storedNotification.label) {
    notification = { ...storedNotification }
  } else if (
    data.time_register_start &&
    now < new Date(data.time_register_start)
  ) {
    notification = {
      severity: 'info',
      label: (
        <span>
          <FormattedMessage id="events.registration.startsAt" />
          &nbsp;
          <span className={classes.notificationColored}>
            {dateFormatter[intl.locale].weekdayTimezone.format(
              new Date(data.time_register_start)
            )}
          </span>
          &nbsp;
          <FormattedMessage id="events.registration.endsAt" />
          &nbsp;
          <span className={classes.notificationColored}>
            {dateFormatter[intl.locale].weekdayTimezone.format(
              new Date(data.time_register_end)
            )}
          </span>
        </span>
      ),
    }
  } else if (auth.isLoggedIn && signup && signup.data && !notification) {
    let label = null
    const storedSeverity =
      storedNotification && storedNotification.severity
        ? storedNotification.severity
        : null

    if (signup.data.accepted) {
      label = <FormattedMessage id="events.signup.accepted" />
    } else if (
      signup.data.position &&
      data.spots > 0 &&
      data.selection_strategy !== 'manual'
    ) {
      label = (
        <FormattedMessage
          id="events.signup.waitingListWithSpot"
          values={{
            position: signup.data.position - data.spots,
          }}
        />
      )
    } else {
      label = <FormattedMessage id="events.signup.waitingList" />
    }

    notification = {
      severity: storedSeverity || 'info',
      label,
    }
  }

  // ---------------------------
  // Configure additional fields
  if (hasAdditionalFields) {
    const formChangeHandler = ({ values, isValid }) => {
      if (JSON.stringify(additionalValues) !== JSON.stringify(values)) {
        setAdditionalValues(values)
      }
      setIsAdditionalValuesValid(isValid)
    }

    // Show signup form when event has additional fields and there
    // is an existing signup or within the registration period.
    // Note: The form is disabled when the registration is not open.
    if (
      (!auth.isLoggedIn && hasOpenRegistration) ||
      (isSignupLoaded && (hasOpenRegistration || (signup && signup.data)))
    ) {
      // info on when the registration window closes
      notification = {
        severity: 'info',
        label: (
          <span>
            <FormattedMessage id="events.registration.isOpenEndsAt" />
            &nbsp;
            <span className={classes.notificationColored}>
              {dateFormatter[intl.locale].weekdayTimezone.format(
                new Date(data.time_register_end)
              )}
            </span>
          </span>
        ),
      }
      additionalFields = (
        <JsonSchemaForm
          schema={JSON.parse(data.additional_fields)}
          initialValues={
            signup && signup.data && signup.data.additional_fields
              ? JSON.parse(signup.data.additional_fields)
              : {}
          }
          onChange={formChangeHandler}
          renderLabel={label =>
            intl.formatMessage({
              id: `events.additionalFields.${label}`,
              defaultMessage: label,
            })
          }
        >
          <JsonSchemaFormFieldGroup
            renderAll
            disabled={
              !hasOpenRegistration ||
              (!auth.isLoggedIn && !data.allow_email_signup)
            }
          />
        </JsonSchemaForm>
      )
    }
  }

  // --------------------------------
  // Configure additional Email field
  if (hasEmailField) {
    const handleEmailChange = ({ target: { value } }) => {
      const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      setEmail(value)
      setIsEmailValid(regex.test(value))
    }

    emailField = (
      <TextField
        name="email"
        label={<FormattedMessage id="email" />}
        value={email}
        error={!isEmailValid}
        type="email"
        onChange={handleEmailChange}
      />
    )
  }

  const openTermsModal = e => {
    e.preventDefault()
    setTermsModalOpen(true)
  }

  const handleCloseModal = accept => {
    setTermsAccepted(accept)
    setTermsModalOpen(false)
  }

  termsDialog = (
    <Dialog
      open={termsModalOpen}
      scroll="paper"
      aria-labelledby={
        <FormattedMessage id="events.signup.termsAndConditions" />
      }
      aria-describedby="scroll-dialog-description"
    >
      <DialogTitle>
        <FormattedMessage id="events.signup.termsAndConditions" />
      </DialogTitle>
      <DialogContent dividers={true}>
        <DialogContentText>
          <TranslatedContent
            content={{ en: terms_en, de: terms_de }}
            noEscape
          />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleCloseModal(false)} color="primary">
          <FormattedMessage id="close" />
        </Button>
        <Button onClick={() => handleCloseModal(true)} color="secondary">
          <FormattedMessage id="button.accept" />
        </Button>
      </DialogActions>
    </Dialog>
  )

  termsCheckbox = (
    <FormControlLabel
      control={
        <Checkbox
          checked={termsAccepted}
          onChange={({ target: { checked } }) => {
            setTermsAccepted(checked)
          }}
          value="termsAccepted"
          color="secondary"
        />
      }
      label={
        <FormattedMessage
          id="events.signup.termsAccepted"
          values={{
            breakingLine: <br />,
            a: function TermsLink(...chunks) {
              return <a onClick={openTermsModal}>{chunks}</a>
            },
          }}
        />
      }
    />
  )

  // ----------------------
  // Prepare signup buttons
  if (data.time_register_start === null || data.time_register_end === null) {
    showTermsCheckbox = false
    signupButtons = (
      <Button disabled>
        <FormattedMessage id="events.registration.none" />
      </Button>
    )
  } else if (
    data.time_register_start &&
    now < new Date(data.time_register_start)
  ) {
    showTermsCheckbox = false
    signupButtons = (
      <Button disabled>
        <FormattedMessage id="events.registration.notStarted" />
      </Button>
    )
  } else if (data.time_register_end && now > new Date(data.time_register_end)) {
    showTermsCheckbox = false
    signupButtons = (
      <Button disabled>
        <FormattedMessage id="events.registration.over" />
      </Button>
    )
  } else if (auth.isLoggedIn && !isSignupLoaded) {
    showTermsCheckbox = false
    signupButtons = <Spinner size={24} />
  } else if (!auth.isLoggedIn && !data.allow_email_signup) {
    showTermsCheckbox = false
    signupButtons = (
      <React.Fragment>
        <Button onClick={() => dispatch(authLoginStart())}>
          <FormattedMessage id="login" />
        </Button>
        <Button disabled>
          <FormattedMessage id="events.restrictions.membersOnly" />
        </Button>
      </React.Fragment>
    )
  } else if (hasOpenRegistration) {
    const updateAction = () => {
      ;(signup && signup.data
        ? dispatch(
            patchSignup({
              id: signup.data._id,
              etag: signup.data._etag,
              eventId,
              additionalFields: hasAdditionalFields ? additionalValues : null,
              email: hasEmailField ? email : null,
            })
          )
        : dispatch(
            postSignup({
              eventId,
              additionalFields: hasAdditionalFields ? additionalValues : null,
              email: hasEmailField ? email : null,
            })
          )
      )
        .then(() => {
          setStoredNotification({
            severity: 'success',
            label: !auth.isLoggedIn
              ? intl.formatMessage({ id: 'events.signup.email_success' })
              : undefined,
          })
        })
        .catch(err => {
          const resp = err.response
          if (
            resp.data &&
            resp.data._issues &&
            resp.data._issues.user &&
            resp.data._issues.user.includes('blacklist')
          ) {
            setStoredNotification({
              severity: 'error',
              label: intl.formatMessage({ id: 'events.signup.blacklisted' }),
            })
          } else {
            setStoredNotification({
              severity: 'error',
              label: intl.formatMessage({ id: 'events.signup.failed' }),
            })
          }
        })
    }

    const deleteAction = () => {
      if (signup && signup.data) {
        dispatch(
          deleteSignup({
            id: signup.data._id,
            etag: signup.data._etag,
            eventId,
          })
        )
          .then(() => {
            setStoredNotification({
              severity: 'success',
              label: <FormattedMessage id="events.signoff.success" />,
            })
          })
          .catch(() => {
            setStoredNotification({
              severity: 'error',
              label: <FormattedMessage id="events.signoff.failed" />,
            })
          })
      }
    }

    showTermsCheckbox = true
    signupButtons = (
      <React.Fragment>
        {(!signup || !signup.data || (signup.data && hasAdditionalFields)) && (
          <Button
            onClick={updateAction}
            disabled={
              (hasEmailField && !isEmailValid) ||
              (hasAdditionalFields && !isAdditionalValuesValid) ||
              (signup && signup.isPending) ||
              !termsAccepted
            }
            variant="outlined"
          >
            <FormattedMessage
              id={
                auth.isLoggedIn && signup && signup.data
                  ? 'events.signup.updateAction'
                  : 'events.signup.action'
              }
            />
          </Button>
        )}
        {auth.isLoggedIn && signup && signup.data && (
          <Button onClick={deleteAction} disabled={signup.isPending}>
            <FormattedMessage id="events.signoff.action" />
          </Button>
        )}
      </React.Fragment>
    )
  }

  return (
    <div className={[classes.root].join(' ')} {...props}>
      {notification && (
        <Alert
          className={classes.notification}
          severity={notification.severity}
        >
          {notification.label}
        </Alert>
      )}
      <div
        className={[
          classes.additionalFields,
          additionalFields || emailField
            ? classes.additionalFieldsVisible
            : null,
        ].join(' ')}
      >
        {emailField}
        {additionalFields}
        {showTermsCheckbox && termsDialog}
        {showTermsCheckbox && termsCheckbox}
      </div>
      <TranslatedContent
        parseMarkdown
        className={classes.description}
        content={{ en: data.description_en, de: data.description_de }}
      />
      <Toolbar className={classes.toolbar} variant="dense">
        {/* Buttons on the LEFT side */}
        <NoSsr>{signupButtons}</NoSsr>
        <span className={classes.toolbarSeparator} />
        {/* Buttons on the RIGHT side */}
        <NoSsr>
          <CopyButton
            value={`${window?.location?.origin}/${intl.locale}/events/${data._id}`}
          >
            <FormattedMessage id="copyDirectLink" />
          </CopyButton>
        </NoSsr>
      </Toolbar>
    </div>
  )
}

EventDetails.propTypes = {
  /** Event id */
  eventId: PropTypes.string,
}

export default EventDetails
