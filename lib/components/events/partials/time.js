import React from 'react'
import PropTypes from 'prop-types'
import { useIntl } from 'react-intl'

import { dateFormatterConfig } from 'config'

const EventTime = ({ time_start, time_end, ...props }) => {
  const { timeZone, dateFormatter } = dateFormatterConfig
  const intl = useIntl()

  // Workaround to ensure that the date instance contains the
  // date with the configured time zone for our comparison.
  const date_start = new Date(
    new Date(time_start).toLocaleString('en-US', { timeZone })
  )
  const date_end = new Date(
    new Date(time_end).toLocaleString('en-US', { timeZone })
  )

  // check if the dates are valid
  if (
    date_start instanceof Date &&
    !Number.isNaN(date_start.valueOf()) &&
    date_end instanceof Date &&
    !Number.isNaN(date_end.valueOf())
  ) {
    let dates = {}
    if (
      (date_start.getDate() === date_end.getDate() &&
        date_start.getMonth() === date_end.getMonth() &&
        date_start.getFullYear() === date_end.getFullYear()) ||
      (date_start.getDate() === date_end.getDate() - 1 &&
        date_start.getMonth() === date_end.getMonth() &&
        date_start.getFullYear() === date_end.getFullYear() &&
        date_start.getHours() > date_end.getHours())
    ) {
      dates = {
        start: dateFormatter[intl.locale].weekday.format(new Date(time_start)),
        end: dateFormatter[intl.locale].short.format(new Date(time_end)),
      }
    } else {
      dates = {
        start: dateFormatter[intl.locale].weekday.format(new Date(time_start)),
        end: dateFormatter[intl.locale].weekdayTimezone.format(
          new Date(time_end)
        ),
      }
    }

    return (
      <div {...props}>
        <span>{dates.start}</span> - <span>{dates.end}</span>
      </div>
    )
  }
  return null
}

EventTime.propTypes = {
  /** Start time of the event */
  time_start: PropTypes.string,
  /** End time of the event */
  time_end: PropTypes.string,
}

export default EventTime
