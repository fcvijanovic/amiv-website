import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

const EventFreeSpots = ({ spots, signup_count, ...props }) => {
  if (spots > 0) {
    const available = Math.max(0, spots - signup_count)
    return (
      <div {...props}>
        <FormattedMessage
          id="events.spotsAvailable"
          values={{ count: available }}
        />
      </div>
    )
  }
  return null
}

EventFreeSpots.propTypes = {
  /** Number of spots at an event. */
  spots: PropTypes.number.isRequired,
  /** Number of confirmed participants. */
  signup_count: PropTypes.number.isRequired,
}

export default EventFreeSpots
