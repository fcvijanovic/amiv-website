import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import debounce from 'debounce'
import {
  RadioGroup,
  Radio,
  FormControlLabel,
  FormControl,
  FormLabel,
  Button,
  Divider,
} from '@material-ui/core'
import { SearchField } from 'amiv-react-components'

import { EVENTS } from 'lib/store/events/constants'
import { setQueryFromFilterValues } from 'lib/store/events/actions'
import { setFilterValue, resetFilterValues } from 'lib/store/common/actions'

import FilterView from '../filteredListPage/filter'

const EventsFilter = ({ debounceTime, ...props }) => {
  const values = useSelector(state => state.events.filterValues)
  const dispatch = useDispatch()
  const router = useRouter()
  const intl = useIntl()

  const setPathIfNeeded = () => {
    router.push('/events')
  }

  const debouncedSetQueryFromFilterValues = React.useCallback(
    debounce(() => {
      dispatch(setQueryFromFilterValues())
      setPathIfNeeded()
    }, debounceTime),
    [dispatch, debounceTime]
  )

  const filterViewChangeHandler = ({ name, value }) => {
    dispatch(setFilterValue(EVENTS, { name, value }))
    debouncedSetQueryFromFilterValues()
  }

  const filterViewResetHandler = () => {
    dispatch(resetFilterValues(EVENTS))
    dispatch(setQueryFromFilterValues(EVENTS))
    setPathIfNeeded()
  }

  return (
    <FilterView
      values={values}
      onChange={filterViewChangeHandler}
      onReset={filterViewResetHandler}
      {...props}
    >
      <SearchField
        name="search"
        elevation={2}
        placeholder={intl.formatMessage({ id: 'events.search' })}
      />
      <FormControl component="fieldset">
        <FormLabel component="legend">
          <FormattedMessage id="events.price" />
        </FormLabel>
        <RadioGroup
          aria-label={intl.formatMessage({ id: 'events.price' })}
          name="price"
        >
          <FormControlLabel
            value="free"
            control={<Radio />}
            label={intl.formatMessage({ id: 'events.free' })}
          />
          <FormControlLabel
            value="all"
            control={<Radio />}
            label={intl.formatMessage({ id: 'events.allEvents' })}
          />
        </RadioGroup>
      </FormControl>
      <FormControl component="fieldset">
        <FormLabel component="legend">
          <FormattedMessage id="events.restrictions.title" />
        </FormLabel>
        <RadioGroup
          aria-label={intl.formatMessage({ id: 'events.restrictions.title' })}
          name="restrictions"
        >
          <FormControlLabel
            value="all"
            control={<Radio />}
            label={intl.formatMessage({ id: 'events.restrictions.none' })}
          />
          <FormControlLabel
            value="members_only"
            control={<Radio />}
            label={intl.formatMessage({
              id: 'events.restrictions.membersOnly',
            })}
          />
        </RadioGroup>
      </FormControl>
      <Divider />
      <Button name="reset" type="reset">
        <FormattedMessage id="reset" />
      </Button>
    </FilterView>
  )
}

EventsFilter.propTypes = {
  /** Debounce time applied on filter updates */
  debounceTime: PropTypes.number,
}

EventsFilter.defaultProps = {
  debounceTime: 500,
}

export default EventsFilter
