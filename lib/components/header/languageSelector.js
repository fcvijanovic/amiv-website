import React from 'react'
import PropTypes from 'prop-types'
import { Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'
import Link from 'next/link'

const useStyles = makeStyles(theme => ({
  root: {
    textAlign: 'center',
    lineHeight: `${theme.shape.headerHeight}px`,
    [theme.breakpoints.down('sm')]: {
      borderTop: `1px solid ${theme.palette.common.grey}`,
      '& > *': {
        margin: '0 1em',
      },
    },
  },
  button: {
    borderColor: theme.palette.common.headerContrastText,
    color: theme.palette.common.headerContrastText,
    minWidth: '48px',
  },
}))

const LanguageSelector = ({ className }) => {
  const { locale, locales, asPath: currentPath } = useRouter()
  const classes = useStyles()

  return (
    <div className={[className, classes.root].join(' ')}>
      {locales
        .filter(otherLocale => otherLocale !== 'default')
        .map(otherLocale => {
          const selected = locale === otherLocale

          return (
            <Link
              passHref
              key={otherLocale}
              href={currentPath}
              locale={otherLocale}
            >
              <Button
                className={classes.button}
                key={otherLocale}
                size="medium"
                variant={selected ? 'outlined' : 'text'}
                language={otherLocale}
              >
                {otherLocale}
              </Button>
            </Link>
          )
        })}
    </div>
  )
}

LanguageSelector.propTypes = {
  className: PropTypes.string,
}

LanguageSelector.defaultProps = {
  className: '',
}

export default LanguageSelector
