import React from 'react'
import { useIntl } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { IconButton, Tooltip } from '@material-ui/core'
import Brightness4Icon from '@material-ui/icons/Brightness4'
import Brightness7Icon from '@material-ui/icons/Brightness7'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import { useRouter } from 'next/router'

import LoginIcon from 'lib/icons/login'
import LogoutIcon from 'lib/icons/logout'
import { authLoginStart, authLogout } from 'lib/store/auth/actions'
import { useTheme } from '../../context/themeContext'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'block',
      textAlign: 'center',
      lineHeight: `${theme.shape.headerHeight}px`,
      '& > *': {
        [theme.breakpoints.only('sm')]: {
          margin: '0 1em',
        },
        [theme.breakpoints.only('xs')]: {
          margin: '0 .5em',
        },
      },
    },
    buttons: {
      color: theme.palette.common.headerContrastText,
    },
    button: {
      borderColor: theme.palette.common.headerContrastText,
      color: theme.palette.common.headerContrastText,
      minWidth: '48px',
    },
  }),
  { name: 'profileMenu' }
)

const ProfileMenu = ({ className, preventMobileMenuClosing }) => {
  const auth = useSelector(state => state.auth)
  const dispatch = useDispatch()
  const classes = useStyles()
  const router = useRouter()
  const intl = useIntl()
  const { type, toggleDarkTheme } = useTheme()

  const tooltipEnterDelay = 500

  const handleToggleTheme = () => {
    preventMobileMenuClosing()
    toggleDarkTheme()
  }

  const profileButtons = auth.isLoggedIn ? (
    <React.Fragment>
      <Tooltip
        title={intl.formatMessage({ id: 'mainMenu.profile' })}
        enterDelay={tooltipEnterDelay}
      >
        <IconButton
          className={classes.buttons}
          onClick={() => router.push('/profile')}
        >
          <AccountCircleIcon />
        </IconButton>
      </Tooltip>
      <Tooltip
        title={intl.formatMessage({ id: 'logout' })}
        enterDelay={tooltipEnterDelay}
      >
        <IconButton
          className={classes.buttons}
          onClick={() => dispatch(authLogout())}
          disabled={auth.isPending}
        >
          <LogoutIcon />
        </IconButton>
      </Tooltip>
    </React.Fragment>
  ) : (
    <Tooltip
      title={intl.formatMessage({ id: 'login' })}
      enterDelay={tooltipEnterDelay}
    >
      <IconButton
        className={classes.buttons}
        onClick={() => dispatch(authLoginStart())}
      >
        <LoginIcon />
      </IconButton>
    </Tooltip>
  )

  return (
    <div className={[className, classes.root].join(' ')}>
      {profileButtons}
      <Tooltip
        title={intl.formatMessage({ id: 'mainMenu.toggleTheme' })}
        enterDelay={tooltipEnterDelay}
      >
        <IconButton className={classes.buttons} onClick={handleToggleTheme}>
          {type === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
        </IconButton>
      </Tooltip>
    </div>
  )
}

ProfileMenu.propTypes = {
  className: PropTypes.string,
  preventMobileMenuClosing: PropTypes.func.isRequired,
}

export default ProfileMenu
