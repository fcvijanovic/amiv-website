import React from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { makeStyles } from '@material-ui/core/styles'
import { ExternalLink } from 'amiv-react-components'

import Link from '../general/link'

// Main menu with all menu items and its configuration
const menuItems = [
  {
    label: 'mainMenu.about.label',
    path: '/about',
    submenu: [
      {
        label: 'mainMenu.about.about',
        path: '/about',
      },
      {
        label: 'mainMenu.about.board',
        path: '/board',
      },
      {
        label: 'mainMenu.about.teams',
        path: '/teams',
      },
      {
        label: 'mainMenu.about.merch',
        path: '/merch',
      },
      {
        label: 'mainMenu.about.statutes',
        url:
          'https://drive.google.com/drive/folders/1HMpasCtk6ufSGPqEJxZSbn4O_tRp6jt6?usp=sharing',
      },
      {
        label: 'mainMenu.about.minutes',
        url:
          'https://drive.google.com/drive/folders/1jO8edob2JRx-IPb1ETC5wXzDf3UuWPwE?usp=sharing',
      },
      {
        label: 'mainMenu.about.gv',
        url:
          'https://drive.google.com/drive/folders/1UjQHMgmZF08q-diOszK5AhcvwEOwKvHS?usp=sharing',
      },
    ],
  },
  {
    label: 'mainMenu.events',
    path: '/events',
  },
  {
    label: 'mainMenu.studies.label',
    path: '/studies/documents',
    submenu: [
      {
        label: 'mainMenu.studies.studydocuments',
        path: '/studies/documents',
      },
      {
        label: 'mainMenu.studies.howToErsti',
        path: '/studies/how-to-ersti',
      },
      {
        label: 'mainMenu.studies.studentForADay',
        path: '/studies/student-for-a-day',
      },
    ],
  },
  {
    label: 'mainMenu.jobs.label',
    path: '/jobs',
    submenu: [
      {
        label: 'mainMenu.jobs.jobs',
        path: '/jobs',
      },
      {
        label: 'mainMenu.jobs.sponsoring',
        path: '/sponsoring',
      },
      {
        label: 'mainMenu.jobs.companies',
        url: 'https://kontakt.amiv.ethz.ch/companies',
      },
    ],
  },
]

const useStyles = makeStyles(
  theme => ({
    menu: {
      [theme.breakpoints.down('sm')]: {
        transition: 'none',
        borderTop: `1px solid ${theme.palette.common.grey}`,
      },
    },
    list: {
      listStyle: 'none',
      margin: 0,
      padding: 0,
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
    menuItem: {
      display: 'inline-block',
      height: '100%',
      padding: '0 1.3em',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        height: 'auto',
        textAlign: 'center',
        padding: '0 .75em',
        borderBottom: `1px solid ${theme.palette.common.grey}`,
      },
      '&:hover $submenu': {
        visibility: 'visible',
        [theme.breakpoints.up('md')]: {
          height: theme.shape.submenuHeight,
        },
      },
      '& > a': {
        display: 'block',
        height: theme.shape.headerHeight,
        lineHeight: `${theme.shape.headerHeight}px`,
        padding: '0 .5em',
        // fontSize: '1.2em',
        color: theme.palette.common.headerContrastText,
        textDecoration: 'none',
        '&:hover': {
          color: theme.palette.secondary.main,
        },
      },
    },
    menuItemActive: {
      height: 'auto',
      backgroundColor: theme.palette.common.grey,
      transition: 'none',

      '& > a': {
        color: theme.palette.common.greyContrastText,
      },

      '& > $submenuPhantomElement': {
        height: theme.shape.submenuHeight,
        display: 'block',
        width: '100%',
        [theme.breakpoints.down('sm')]: {
          display: 'none',
        },
      },

      '& > $submenu': {
        display: 'block',
        visibility: 'visible',
        // Suppress any animation on the active submenu
        transition: 'none !important',
        transitionDuration: '0ms !important',
        transitionDelay: '0ms !important',
        zIndex: 1005,
        [theme.breakpoints.up('md')]: {
          height: theme.shape.submenuHeight,
        },
      },
    },
    submenu: {
      display: 'block',
      visibility: 'hidden',
      height: 0,
      [theme.breakpoints.up('md')]: {
        position: 'absolute',
        top: `${theme.shape.headerHeight}px`,
        left: 0,
        width: '100%',
        textAlign: 'center',
        backgroundColor: theme.palette.common.grey,
        zIndex: 1010,
        overflowY: 'hidden',
        transitionDelay: '150ms',
        transitionProperty: 'height',
        transitionDuration: '150ms',
        transformStyle: 'flat',
      },
      [theme.breakpoints.down('sm')]: {
        display: 'none',
        backgroundColor: theme.palette.common.mobileSubmenu,
        height: 'unset',
        marginBottom: '.75em',
      },
    },
    submenuPhantomElement: {
      height: '1px',
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
    submenuItem: {
      display: 'inline-block',
      height: `calc(${theme.shape.submenuHeight} - 1px)`,
      lineHeight: theme.shape.submenuHeight,
      padding: '0 1.3em',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        height: 'auto',
        textAlign: 'center',
        borderBottom: `1px solid ${theme.palette.common.grey}`,
        backgroundColor: theme.palette.common.mobileSubmenu,
      },
      '& > a': {
        display: 'inline-block',
        lineHeight: `calc(${theme.shape.submenuHeight} - 1em)`,
        color: theme.palette.text.primary,
        textDecoration: 'none',
        margin: '.5em 0',
        padding: '0 1.3em',
        '&:hover': {
          color: theme.palette.secondary.main,
        },
      },
    },
    submenuItemActive: {
      '& > a': {
        borderRadius: '24px',
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.secondary.contrastText,
        '&:hover': {
          color: theme.palette.secondary.contrastText,
        },
      },
    },
  }),
  { name: 'mainMenu' }
)

const MainMenu = ({ className, preventMobileMenuClosing, closeMobileMenu }) => {
  const classes = useStyles()
  const { pathname: currentPath } = useRouter()

  const isItemActive = ({ path, submenu }) => {
    if (!path) return false

    return (
      (path.length <= 4 && currentPath === path) ||
      (path.length > 4 && currentPath.includes(path)) ||
      (submenu && submenu.find(subitem => isItemActive(subitem)))
    )
  }

  return (
    <ul className={[className, classes.menu, classes.list].join(' ')}>
      {menuItems.map(({ path, label, submenu }) => {
        const handleClick = () => {
          if (submenu) {
            preventMobileMenuClosing()
          }
        }

        return (
          <li
            key={label}
            className={[
              classes.menuItem,
              isItemActive({ path, submenu }) && classes.menuItemActive,
            ].join(' ')}
          >
            <Link href={path} underline="none" onClick={handleClick}>
              <FormattedMessage id={label} />
            </Link>
            {(() => {
              if (!submenu) return null
              return (
                <React.Fragment>
                  <div className={classes.submenuPhantomElement} />
                  <ul className={[classes.submenu, classes.list].join(' ')}>
                    {submenu.map(subitem => (
                      <li
                        key={subitem.label}
                        className={[
                          classes.submenuItem,
                          isItemActive(subitem) && classes.submenuItemActive,
                        ].join(' ')}
                      >
                        {(() => {
                          if (subitem.path) {
                            return (
                              <Link
                                href={subitem.path}
                                underline="none"
                                onClick={closeMobileMenu}
                              >
                                <FormattedMessage id={subitem.label} />
                              </Link>
                            )
                          }
                          return (
                            <ExternalLink
                              href={subitem.url}
                              underline="none"
                              onClick={closeMobileMenu}
                            >
                              <FormattedMessage id={subitem.label} />
                            </ExternalLink>
                          )
                        })()}
                      </li>
                    ))}
                  </ul>
                </React.Fragment>
              )
            })()}
          </li>
        )
      })}
    </ul>
  )
}

MainMenu.propTypes = {
  className: PropTypes.string,
  preventMobileMenuClosing: PropTypes.func.isRequired,
  closeMobileMenu: PropTypes.func.isRequired,
}

export default MainMenu
