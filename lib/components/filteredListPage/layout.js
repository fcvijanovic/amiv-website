import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { Collapse, Button } from '@material-ui/core'

import StickyOnScroll from '../general/stickyOnScroll'

const useStyles = makeStyles(
  theme => ({
    root: {
      position: 'relative',
      minHeight: '100%',
      display: 'grid',
      gridTemplateColumns: '22.35em 1fr',
      marginBottom: '2em',
      [theme.breakpoints.only('md')]: {
        gridTemplateColumns: '19.25em 1fr',
      },
      [theme.breakpoints.down('sm')]: {
        display: 'block',
        padding: 0,
      },
    },
    filter: {
      position: 'sticky',
      padding: '1.5em',
      top: 0,
      [theme.breakpoints.down('sm')]: {
        padding: '1em',
      },
    },
    filterButton: {
      display: 'none',
      [theme.breakpoints.down('sm')]: {
        display: 'inline-block',
        marginBottom: '2em',
      },
    },
    collapse: {
      [theme.breakpoints.up('md')]: {
        height: 'auto !important',
        visibility: 'visible',
      },
    },
    list: {
      paddingTop: '2em',
      [theme.breakpoints.down('sm')]: {
        padding: '2em 1em 0',
      },
    },
  }),
  { name: 'filteredList' }
)

const FilteredListLayout = ({
  children: childrenProp,
  className,
  showFilterLabel,
  hideFilterLabel,
  ...props
}) => {
  const [showMobileFilter, setShowMobileFilter] = useState(false)
  const classes = useStyles()
  const theme = useTheme()

  const [filter, ...children] = React.Children.toArray(childrenProp)

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      <StickyOnScroll
        className={classes.filter}
        scrollTriggerHeight={theme.shape.headerHeight}
      >
        <div>
          <Button
            className={classes.filterButton}
            onClick={() => setShowMobileFilter(!showMobileFilter)}
          >
            <FormattedMessage
              id={showMobileFilter ? hideFilterLabel : showFilterLabel}
            />
          </Button>
          <Collapse className={classes.collapse} in={showMobileFilter}>
            {filter}
          </Collapse>
        </div>
      </StickyOnScroll>
      <div className={classes.list}>{children}</div>
    </div>
  )
}

FilteredListLayout.propTypes = {
  /** Child elements contained in the layout. First element is treated as filter view. */
  children: PropTypes.node.isRequired,
  /** Label for show-filter button (can also be a translation key) */
  showFilterLabel: PropTypes.string,
  /** Label for hide-filter button (can also be a translation key) */
  hideFilterLabel: PropTypes.string,
  /** Additional class applied to the outermost box. */
  className: PropTypes.string,
}

FilteredListLayout.defaultProps = {
  showFilterLabel: 'filter.show',
  hideFilterLabel: 'filter.hide',
}

export default FilteredListLayout
