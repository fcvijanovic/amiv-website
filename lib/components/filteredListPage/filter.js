import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { Switch, FormControl, Button } from '@material-ui/core'

const useStyles = makeStyles(
  {
    root: {
      display: 'grid',
      alignContent: 'start',
      gridGap: '20px',
      gridTemplateRows: 'auto auto',

      '& > *': {
        width: 'calc(100% - 1em)',
        margin: '.5em',
      },
    },
  },
  { name: 'filter' }
)

const Filter = ({
  values,
  onChange,
  onReset,
  className,
  children,
  ...props
}) => {
  const [initialized, setInitialized] = useState(false)
  const classes = useStyles()

  // Workaround so that the first client-side rendering is
  // the same as the server-side rendering.
  useEffect(() => setInitialized(true), [])

  const modifyChildren = collection =>
    React.Children.map(collection, child => {
      let fieldProps = {}
      const { name } = child.props

      if (child.type === FormControl) {
        return React.cloneElement(child, {
          disabled: child.props.disabled || !initialized,
          children: modifyChildren(child.props.children),
        })
      }

      // Component does not seem to be a filter field because it is missing the name property.
      // Non-form fields are not modified.
      if (!name) return child

      if (child.type === Switch) {
        fieldProps = {
          checked: values[name],
          onChange: e => {
            return onChange({ name, value: e.target.checked })
          },
        }
      } else if (child.type === Button) {
        if (child.props.type === 'reset') {
          fieldProps = {
            onClick: () => onReset(),
          }
        }
      } else {
        // Default modification working for most form fields.
        // Tested with: SearchField, TextField, RadioGroup, Select
        fieldProps = {
          value: values[name],
          onChange: e => onChange({ name, value: e.target.value }),
        }
      }

      return React.cloneElement(child, fieldProps)
    })

  const modifiedChildren = modifyChildren(children)

  // const modifiedChildren = children

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      {modifiedChildren}
    </div>
  )
}

Filter.propTypes = {
  /** Collection of values to be applied on the filter fields */
  values: PropTypes.object.isRequired,
  /** Callback when field value has changed */
  onChange: PropTypes.func.isRequired,
  /** Callback when filter value should be resetted to their inital value */
  onReset: PropTypes.func,
  /** Filter fields and so. */
  children: PropTypes.node.isRequired,
  /** Additional class for the outermost box */
  className: PropTypes.string,
}

Filter.defaultProps = {
  onReset: () => {},
}

export default Filter
