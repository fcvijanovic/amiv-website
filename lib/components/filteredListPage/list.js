import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  List,
  ListSubheader,
  useScrollTrigger,
  Typography,
  Button,
} from '@material-ui/core'

const useStyles = makeStyles(
  theme => ({
    root: {
      position: 'relative',
      display: 'block',
      paddingTop: '0',
    },
    title: {
      position: 'sticky',
      top: 0,
      width: '100%',
      paddingTop: '1.5em',
      transition: 'top 225ms cubic-bezier(0, 0, 0.2, 1) 0ms',

      '& > div': {
        marginTop: '.5em',
        borderRadius: '4px',
        backgroundColor: theme.palette.common.grey,
      },
    },
    titleBelowAppBar: {
      top: `${theme.shape.headerHeight}px`,
    },
    emptyList: {
      margin: '75px 0',
    },
    loadMore: {
      marginTop: '1.5em',
    },
  }),
  { name: 'filteredList' }
)

const FilteredList = ({
  className,
  title,
  children,
  placeholder,
  showPlaceholder,
  loadMore,
  loadMoreLabel,
  ...props
}) => {
  const classes = useStyles()
  const scrollTrigger = useScrollTrigger()

  return (
    <React.Fragment>
      <List className={classes.root} {...props}>
        {title && (
          <ListSubheader
            disableGutters
            className={[
              classes.title,
              !scrollTrigger && classes.titleBelowAppBar,
            ].join(' ')}
          >
            <Box boxShadow={2}>
              <FormattedMessage id={title} />
            </Box>
          </ListSubheader>
        )}
        {React.Children.count(children) > 0
          ? children
          : !showPlaceholder && (
              <Typography className={classes.emptyList}>
                <FormattedMessage id="emptyList" />
              </Typography>
            )}
        {showPlaceholder && placeholder}
      </List>
      {loadMore && !showPlaceholder && (
        <Button
          className={classes.loadMore}
          variant="outlined"
          onClick={loadMore}
        >
          <FormattedMessage id={loadMoreLabel} />
        </Button>
      )}
    </React.Fragment>
  )
}

FilteredList.propTypes = {
  /** Title of the list */
  title: PropTypes.string,
  /** List items */
  children: PropTypes.node,
  /** Placeholder element shown when list is not loaded. */
  placeholder: PropTypes.node,
  /**
   * Specifies whether to show placeholder or not.
   * If set to false and no children are provided, a empty list message is displayed.
   */
  showPlaceholder: PropTypes.bool,
  /**
   * Callback to load more entries
   * If `null`, the load more button will be hidden.
   */
  loadMore: PropTypes.func,
  /** Label for the load more button */
  loadMoreLabel: PropTypes.string,
  /** Additional class applied to the outermost container. */
  className: PropTypes.string,
}

FilteredList.defaultProps = {
  loadMoreLabel: 'loadMore',
}

export default FilteredList
