import React from 'react'
import PropTypes from 'prop-types'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(
  theme => ({
    rounded: {
      overflow: 'hidden',

      '&:last-child $summaryRoot': {
        borderBottom: 'none',
      },
    },
    expanded: {
      '&:last-child $summaryRoot': {
        borderBottom: `1px solid ${theme.palette.common.grey}`,
      },
    },
    summaryRoot: {
      padding: '0 24px 0 0',
      borderBottom: `1px solid ${theme.palette.common.grey}`,
    },
    summaryContent: {
      margin: '0 !important',
    },
    disabled: {
      backgroundColor: `${theme.palette.common.grey} !important`,
    },
    detailsRoot: {
      padding: '0 !important',
    },
  }),
  { name: 'filteredListItem' }
)

const FilteredListItem = React.forwardRef(
  ({ children: childrenProp, id, onChange, ...props }, ref) => {
    const classes = useStyles()
    const [summary, ...children] = React.Children.toArray(childrenProp)

    const changeHandler = (e, expanded) => {
      onChange({ id, expanded })
    }

    return (
      <Accordion
        component="li"
        ref={ref}
        classes={{
          rounded: classes.rounded,
          expanded: classes.expanded,
          disabled: classes.disabled,
        }}
        onChange={changeHandler}
        TransitionProps={{ unmountOnExit: true }}
        {...props}
      >
        <AccordionSummary
          classes={{
            root: classes.summaryRoot,
            content: classes.summaryContent,
          }}
          expandIcon={<ExpandMoreIcon />}
        >
          {summary}
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.detailsRoot }}>
          {children}
        </AccordionDetails>
      </Accordion>
    )
  }
)

FilteredListItem.displayName = 'FilteredListItem'

FilteredListItem.propTypes = {
  /** Unique string identifier for this item. */
  id: PropTypes.string,
  /** Child elements contained in the list item. First element is treated as summary view. */
  children: PropTypes.node.isRequired,
  /** Callback when item should be expanded or not. */
  onChange: PropTypes.func,
}

FilteredListItem.defaultProps = {
  onChange: () => {},
}

export default FilteredListItem
