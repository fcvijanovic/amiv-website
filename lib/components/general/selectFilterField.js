import React, { useCallback, useRef } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
  FormControl,
  InputLabel,
  Select,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core'
import ClearIcon from '@material-ui/icons/Clear'

import { isObject } from 'lib/utils'

const useStyles = makeStyles(
  {
    select: {
      width: '100%',
    },
    list: {
      width: '100%',
    },
  },
  { name: 'selectFilterField' }
)

const SelectFilterField = ({
  name,
  value,
  options,
  onChange,
  label: labelProp,
  disabled: disabledProp,
  'aria-label': ariaLabel,
  className,
  ...props
}) => {
  const classes = useStyles()
  const inputRef = useRef(null)

  const label = labelProp || name
  const disabled = disabledProp || (value && value.length >= options.length)

  const handleAddSelection = ({ target: { value: addValue } }) => {
    if (disabledProp) return
    const newValue = [...value, addValue]
    // Remove focus of the select input field when all fields are selected
    if (inputRef.current && newValue.length >= options.length) {
      inputRef.current.blur()
    }
    onChange({ target: { value: newValue } })
  }

  const handleRemoveSelection = removeValue => {
    if (disabledProp) return
    onChange({ target: { value: value.filter(item => item !== removeValue) } })
  }

  const optionsRemaining = useCallback(
    () =>
      options.map((option, i) => {
        const optionValue = isObject(option) ? option.value : option
        if (!value || value.indexOf(optionValue) === -1) {
          return (
            <option key={i} value={option.value || option}>
              {option.label || option}
            </option>
          )
        }
        return null
      }),
    [options, value]
  )

  const optionsSelected = useCallback(
    () =>
      options.map((option, i) => {
        const optionValue = option.value || option
        if (value && value.indexOf(optionValue) !== -1) {
          return (
            <ListItem
              key={i}
              role={undefined}
              button={!disabledProp}
              dense
              onClick={() => handleRemoveSelection(optionValue)}
            >
              <ListItemText primary={option.label || option} />
              <ClearIcon />
            </ListItem>
          )
        }
        return null
      }),
    [options, value]
  )

  return (
    <div {...props}>
      <FormControl className={classes.select}>
        <InputLabel shrink={false} disabled={disabled}>
          {label}
        </InputLabel>
        <Select
          native
          name={name}
          value={''}
          disabled={disabled}
          onChange={handleAddSelection}
          inputProps={{
            ref: inputRef,
            'aria-label': ariaLabel,
          }}
        >
          <option key="null" value={null} disabled hidden></option>
          {optionsRemaining()}
        </Select>
      </FormControl>
      {value && value.length > 0 && (
        <List className={classes.list}>{optionsSelected()}</List>
      )}
    </div>
  )
}

SelectFilterField.propTypes = {
  /** Callback when input text has changed */
  onChange: PropTypes.func,
  /** Options available for selection. */
  options: PropTypes.array.isRequired,
  /** Values currently selected. */
  value: PropTypes.array,
  /** disables the whole component. */
  disabled: PropTypes.bool,
  /** Field name */
  name: PropTypes.string.isRequired,
  /** Label for this selection field. */
  label: PropTypes.string,
  /** Label used to improve accessibility. */
  'aria-label': PropTypes.string,
  /** Additional class applied to the outermost box. */
  className: PropTypes.string,
}

SelectFilterField.defaultProps = {
  onChange: () => {},
  disabled: false,
  value: [],
}

export default SelectFilterField
