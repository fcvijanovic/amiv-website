import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import Img from 'next/image'
import { makeStyles } from '@material-ui/core/styles'
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      backgroundColor: theme.palette.common.grey,
      position: 'relative',
      verticalAlign: 'middle',
    },
    imgContainer: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      overflow: 'hidden',
      '& > span': {
        top: '50%',
        left: '50%',
        transform: 'translateX(-50%) translateY(-50%)',
      },
    },
    contain: {
      objectFit: 'contain',
    },
    cover: {
      objectFit: 'cover',
    },
    none: {
      objectFit: 'none',
    },
    error: {
      position: 'absolute !important',
      maxWidth: '100%',
      maxHeight: '100%',
      top: '50% !important',
      left: '50% !important',
      transform: 'translateX(-50%) translateY(-50%)',

      '& > *': {
        display: 'inline-block',
        verticalAlign: 'middle',
        marginLeft: '.5em',
      },
    },
  }),
  { name: 'staticImage' }
)

const StaticImage = ({
  src,
  ratioX,
  ratioY,
  alt,
  className,
  objectFit,
  onLoadingComplete,
  onLoad,
  onError,
  ...props
}) => {
  const [error, setError] = useState(false)
  const classes = useStyles()

  const hasRatio = ratioX && ratioY
  const ratio = hasRatio ? (ratioY / ratioX) * 100 : 1

  const handleImageLoaded = () => {
    setError(false)
    if (onLoadingComplete) {
      onLoadingComplete()
    }
  }

  const handleImageError = () => {
    setError(true)
    if (onError) {
      onError()
    }
  }

  if (!src) {
    return (
      <div
        className={[classes.root, className].join(' ')}
        style={hasRatio && { paddingBottom: `${ratio}%` }}
        {...props}
      >
        <div className={classes.error}>
          <ErrorOutlineIcon />
          <Typography>
            <FormattedMessage id="missing.image" />
          </Typography>
        </div>
      </div>
    )
  }

  if (error) {
    return (
      <div
        className={[classes.root, className].join(' ')}
        style={hasRatio && { paddingBottom: `${ratio}%` }}
        {...props}
      >
        <div className={classes.error}>
          <ErrorOutlineIcon />
          <Typography>
            <FormattedMessage id="error.image" />
          </Typography>
        </div>
      </div>
    )
  }

  return (
    <div
      className={[classes.root, className].join(' ')}
      style={hasRatio && { paddingBottom: `${ratio}%` }}
      {...props}
    >
      <div className={classes.imgContainer}>
        <Img
          className={classes[objectFit]}
          fill={true}
          src={src}
          alt={alt}
          onLoadingComplete={handleImageLoaded}
          onLoad={onLoad}
          onError={handleImageError}
        />
      </div>
    </div>
  )
}

StaticImage.propTypes = {
  /** Target URL */
  src: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  /** Alternative text shown when image could not be loaded */
  alt: PropTypes.string,
  /** Priority=true will load first */
  priority: PropTypes.bool,
  /** Ratio number for x direction. RatioY is also required if this value is set. */
  ratioX: PropTypes.number,
  /** Ratio number for y direction. RatioX is also required if this value is set. */
  ratioY: PropTypes.number,
  /** Value for object-fit CSS style */
  objectFit: PropTypes.oneOf(['contain', 'cover', 'none']),
  /** Callback when loading was finished */
  onLoadingComplete: PropTypes.func,
  /** Callback when loading is started */
  onLoad: PropTypes.func,
  /** Callback when image could not be loaded */
  onError: PropTypes.func,
  /** @ignore */
  className: PropTypes.string,
}

StaticImage.defaultProps = {
  objectFit: 'contain',
}

export default StaticImage
