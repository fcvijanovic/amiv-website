import React, { useRef, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { useScrollTrigger } from '@material-ui/core'

const useStyles = makeStyles(
  {
    root: {
      transition: 'top 225ms cubic-bezier(0, 0, 0.2, 1) 0ms',
    },
  },
  { name: 'stickyOnScroll' }
)

const StickyOnScroll = ({
  className,
  scrollTriggerHeight,
  children,
  ...rest
}) => {
  const [positionTop, setPositionTop] = useState(0)
  const [lastScrollPosition, setLastScrollPosition] = useState(0)
  const containerElement = useRef(null)
  const scrollTrigger = useScrollTrigger()
  const classes = useStyles()

  useEffect(() => {
    if (!containerElement.current) return () => {}

    const windowHeight = Math.max(
      document.documentElement.clientHeight,
      window.innerHeight || 0
    )
    const scrollTriggerHeightCorrection = !scrollTrigger
      ? scrollTriggerHeight
      : 0
    const oversize = windowHeight - containerElement.current.scrollHeight
    const maxPosition = Math.min(oversize, scrollTriggerHeightCorrection)

    // Do scroll position magic if content is larger than the screen.
    if (oversize < 0) {
      const handleScroll = () => {
        const { scrollTop } = document.documentElement
        const scrollDelta = scrollTop - lastScrollPosition

        const newPositionTop = Math.min(
          scrollTriggerHeightCorrection,
          Math.max(positionTop - scrollDelta, maxPosition)
        )

        setPositionTop(newPositionTop)
        setLastScrollPosition(scrollTop)
      }
      handleScroll() // Re-evaluate trigger when dependencies change
      document.addEventListener('scroll', handleScroll)

      return () => {
        document.removeEventListener('scroll', handleScroll)
      }
    }

    // Set the position value statically depending on scroll trigger only
    // if content fits on screen.
    setPositionTop(scrollTriggerHeightCorrection)

    return () => {}
  }, [scrollTrigger, containerElement])

  const child = React.Children.only(children)

  return (
    <div {...rest}>
      <div
        className={[classes.root, className].join(' ')}
        style={{
          top: `${positionTop}px`,
        }}
        ref={containerElement}
      >
        {child}
      </div>
    </div>
  )
}

StickyOnScroll.propTypes = {
  /** Additional classes for the sticky container. */
  className: PropTypes.string,
  /** Child element. Only on element allowed! */
  children: PropTypes.node.isRequired,
  /**
   * Specifies the height of the item visible depending on scroll trigger at the top of the page.
   * This is usually the AppBar or header component.
   */
  scrollTriggerHeight: PropTypes.number,
}

StickyOnScroll.defaultProps = {
  scrollTriggerHeight: 0,
}

export default StickyOnScroll
