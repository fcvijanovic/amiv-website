import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import Skeleton from '@material-ui/lab/Skeleton'
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      backgroundColor: theme.palette.common.grey,
      position: 'relative',

      '& > *': {
        maxWidth: '100%',
        maxHeight: '100%',
      },
    },
    centered: {
      position: 'absolute !important',
      top: '50%',
      left: '50%',
      transform: 'translateX(-50%) translateY(-50%)',
    },
    static: {
      width: '100%',
      height: '100%',
    },
    hidden: {
      visibility: 'hidden',
    },
    error: {
      '& > *': {
        display: 'inline-block',
        verticalAlign: 'middle',
        marginLeft: '.5em',
      },
    },
  }),
  { name: 'dynamicImage' }
)

const DynamicImage = ({
  src,
  ratioX,
  ratioY,
  alt,
  layout,
  className,
  loadingPlaceholder,
  ...props
}) => {
  const [loaded, setLoaded] = useState(false)
  const [error, setError] = useState(false)
  const [isBrowser, setIsBrowser] = useState(false)
  const classes = useStyles()
  const image = useRef()

  const ratio = (ratioY / ratioX) * 100

  const handleImageLoaded = () => {
    setLoaded(true)
    setError(false)
  }

  const handleImageError = () => {
    setLoaded(true)
    setError(true)
  }

  useEffect(() => {
    // Server-side rendering must match the first client-side rendering.
    // This workaround updates the state only after the first client-side
    // rendering.
    setIsBrowser(true)
  })

  useEffect(() => {
    if (image.current?.complete) handleImageLoaded()
  }, [])

  if (!src) {
    return (
      <div
        className={[classes.root, className].join(' ')}
        style={{ paddingBottom: `${ratio}%` }}
        {...props}
      >
        <div className={[classes.centered, classes.error].join(' ')}>
          <ErrorOutlineIcon />
          <Typography>
            <FormattedMessage id="missing.image" />
          </Typography>
        </div>
      </div>
    )
  }

  return (
    <div
      className={[classes.root, className].join(' ')}
      style={{ paddingBottom: `${ratio}%` }}
      {...props}
    >
      {/* eslint-disable-next-line @next/next/no-img-element */}
      <img
        src={src}
        alt={alt}
        ref={image}
        className={[
          classes.centered,
          isBrowser && (!loaded || error) ? classes.hidden : undefined,
        ].join(' ')}
        onLoad={handleImageLoaded}
        onError={handleImageError}
      />
      {isBrowser &&
        !loaded &&
        (loadingPlaceholder || (
          <Skeleton
            className={classes.centered}
            width="100%"
            height="100%"
            variant="rect"
            animation="wave"
          />
        ))}
      {loaded && error && (
        <div className={[classes.centered, classes.error].join(' ')}>
          <ErrorOutlineIcon />
          <Typography>
            <FormattedMessage id="error.image" />
          </Typography>
        </div>
      )}
    </div>
  )
}

DynamicImage.propTypes = {
  /** Target URL */
  src: PropTypes.string,
  /** Alternative text shown when image could not be loaded */
  alt: PropTypes.string,
  /** Priority=true will load first */
  priority: PropTypes.bool,
  /** Ratio number for x direction */
  ratioX: PropTypes.number.isRequired,
  /** Ratio number for y direction */
  ratioY: PropTypes.number.isRequired,
  /** Placeholder shown when image is loading */
  loadingPlaceholder: PropTypes.element,
  layout: PropTypes.string,
  /** @ignore */
  className: PropTypes.string,
}

export default DynamicImage
