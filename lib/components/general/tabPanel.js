import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(
  {
    root: {
      // TODO
    },
    hidden: {
      display: 'none',
    },
  },
  { name: 'tabPanel' }
)

const TabPanel = ({ value, index, className, children, ...props }) => {
  const classes = useStyles()

  const hidden = value !== index

  return (
    <div
      className={[
        classes.root,
        hidden ? classes.hidden : undefined,
        className,
      ].join(' ')}
      {...props}
    >
      {!hidden && children}
    </div>
  )
}

TabPanel.propTypes = {
  /** Currently selected value */
  value: PropTypes.any.isRequired,
  /** Index value of this item */
  index: PropTypes.any.isRequired,
  /** Content of the link (text) */
  children: PropTypes.node.isRequired,
  /** Additional class applied to the outermost box. */
  className: PropTypes.string,
}

export default TabPanel
