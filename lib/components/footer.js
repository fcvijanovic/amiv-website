import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'
import InstagramIcon from '@material-ui/icons/Instagram'
import FacebookIcon from '@material-ui/icons/Facebook'
import TwitterIcon from '@material-ui/icons/Twitter'
import { ExternalLink } from 'amiv-react-components'

import Link from './general/link'

const useVsethLogoStyles = makeStyles(
  {
    root: {
      position: 'relative',
      zIndex: 999,
      left: props => `${props.coord[0]}px`,
      top: props => `${props.coord[1]}px`,

      '& > img': {
        filter: 'invert(1)',
      },
    },
  },
  { name: 'vsethLogo' }
)

const VsethLogo = ({ className }) => {
  const [coord, setCoord] = useState([0, 0])
  const classes = useVsethLogoStyles({ coord })

  const handleClick = e => {
    const x = -Math.random() * 400
    const y = -Math.random() * 400
    setCoord([x, y])
    e.preventDefault()
    return false
  }

  return (
    <a
      className={[className, classes.root].join(' ')}
      href="https://vseth.ethz.ch/"
      onClick={handleClick}
    >
      {/* eslint-disable-next-line @next/next/no-img-element */}
      <img height={30} src="/logos/vseth_fachverein.png" alt="VSETH" />
    </a>
  )
}

VsethLogo.propTypes = {
  className: PropTypes.string,
}

VsethLogo.defaultProps = {
  className: '',
}

// Footer Component
const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
    },
    contentContainer: {
      maxWidth: theme.shape.maxContentWidth,
      margin: '0 auto',
    },
    report: {
      backgroundColor: theme.palette.common.grey,
      textAlign: 'center',
      padding: '1em',
    },
    footer: {
      backgroundColor: theme.palette.common.header,
      color: theme.palette.common.headerContrastText,
    },
    footerContent: {
      display: 'grid',
      textAlign: 'center',
      [theme.breakpoints.up('md')]: {
        gridTemplateColumns: '50% 19% 31%',
        gridTemplateAreas: "'copyright social-media-logos institution-logos'",
      },
      [theme.breakpoints.only('md')]: {
        fontSize: '.9em',
      },
      [theme.breakpoints.down('sm')]: {
        gridTemplateColumns: '1fr',
        gridTemplateAreas:
          "'copyright' 'social-media-logos' 'institution-logos'",
      },
    },
    copyright: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'left',
      gridArea: 'copyright',
      padding: '2.5em .7em',
      [theme.breakpoints.down('sm')]: {
        padding: '2.2em 0 1.2em',
        justifyContent: 'center',
        flexWrap: 'wrap',
      },
    },
    copyrightLink: {
      marginLeft: '1em',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        marginTop: '1em',
      },
    },
    socialLogoContainer: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      gridArea: 'social-media-logos',
    },
    socialLogoLink: {
      margin: '1em .7em',
    },
    socialLogo: {
      color: theme.palette.common.headerContrastText,
      width: 32,
      height: 32,
    },
    institutionLogoContainer: {
      display: 'flex',
      justifyContent: 'right',
      alignItems: 'center',
      gridArea: 'institution-logos',
      [theme.breakpoints.down('sm')]: {
        justifyContent: 'center',
      },
    },
    vsethLogo: {
      background: theme.palette.common.header,
    },
  }),
  { name: 'footer' }
)

const Footer = ({ className }) => {
  const classes = useStyles()

  return (
    <footer className={[className, classes.root].join(' ')}>
      <div className={classes.report}>
        <Typography className={classes.contentContainer}>
          <FormattedMessage id="footer.issueSpotted" />
          &nbsp;
          <ExternalLink href="https://gitlab.ethz.ch/amiv/amiv-website/-/issues/new?issuable_template=bug">
            <FormattedMessage id="footer.issueReport" />
          </ExternalLink>
        </Typography>
      </div>
      <div className={classes.footer}>
        <div
          className={[classes.contentContainer, classes.footerContent].join(
            ' '
          )}
        >
          <Typography className={classes.copyright}>
            © 1893 - {new Date().getFullYear()} AMIV an der ETH&nbsp;
            <Link
              className={classes.copyrightLink}
              color="secondary"
              href="/legal-notice"
            >
              <FormattedMessage id="contact" />
              &nbsp;/&nbsp;
              <FormattedMessage id="legal-notice.title" />
            </Link>
          </Typography>
          <div className={classes.socialLogoContainer}>
            <ExternalLink
              className={classes.socialLogoLink}
              noIcon
              href="https://www.facebook.com/AMIV.ETHZ/"
            >
              <FacebookIcon className={classes.socialLogo} />
            </ExternalLink>
            <ExternalLink
              className={classes.socialLogoLink}
              noIcon
              href="https://www.instagram.com/amiv_eth/"
            >
              <InstagramIcon className={classes.socialLogo} />
            </ExternalLink>
            <ExternalLink
              className={classes.socialLogoLink}
              noIcon
              href="https://twitter.com/amiv_ethz"
            >
              <TwitterIcon className={classes.socialLogo} />
            </ExternalLink>
          </div>
          <div className={classes.institutionLogoContainer}>
            <ExternalLink noIcon href="https://www.ethz.ch/">
              {/* eslint-disable-next-line @next/next/no-img-element */}
              <img height={64} src="/logos/eth.svg" alt="ETH Zürich" />
            </ExternalLink>
            <VsethLogo className={classes.vsethLogo} />
          </div>
        </div>
      </div>
    </footer>
  )
}

Footer.propTypes = {
  className: PropTypes.string,
}

Footer.defaultProps = {
  className: '',
}

export default Footer
