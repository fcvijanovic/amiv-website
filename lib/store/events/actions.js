import {
  EVENTS,
  EVENTS_SIGNUP_PENDING,
  EVENTS_SIGNUP_SUCCESS,
  EVENTS_SIGNUP_EMAIL_SUCCESS,
  EVENTS_SIGNUP_DELETED,
  EVENTS_SIGNUP_ERROR,
} from './constants'

import * as baseActions from '../common/actions'

const _generateQueryFromFilterValues = values => {
  const query = { $and: [] }
  Object.keys(values).forEach(key => {
    const value = values[key]

    if (key === 'price') {
      if (value === 'free') {
        query.$and.push({ $or: [{ price: null }, { price: 0 }] })
      }
    } else if (key === 'restrictions') {
      if (value === 'all') {
        query.allow_email_signup = true
      }
    } else if (key === 'search' && value.length > 0) {
      const regex = { $regex: `^(?i).*${value}.*` }
      query.$and.push({
        $or: [
          { title_en: regex },
          { title_de: regex },
          { catchphrase_en: regex },
          { catchphrase_de: regex },
          { description_en: regex },
          { description_de: regex },
        ],
      })
    }
  })
  if (query.$and.length === 0) {
    delete query.$and
  }

  return { where: query }
}

/**
 * Update stored query and clear all depending lists
 */
const setQueryFromFilterValues = () =>
  baseActions.setQueryFromFilterValues(EVENTS, {
    queryConverter: _generateQueryFromFilterValues,
  })

/**
 * Load signup data of the current user for an event
 * @param {string} eventId event id
 */
const loadSignupForEvent = ({ eventId }) => (dispatch, getState) => {
  const { auth: { session } = {} } = getState()
  return dispatch({
    types: [
      { type: EVENTS_SIGNUP_PENDING, eventId },
      { type: EVENTS_SIGNUP_SUCCESS, eventId },
      { type: EVENTS_SIGNUP_ERROR, eventId },
    ],
    resource: 'eventsignups',
    method: 'GET',
    query: {
      where: { event: eventId, user: session ? session.user : null },
      max_results: 1,
    },
  })
}

/**
 * Post signup data of the current user for an event
 * @param {string} props.event            event id
 * @param {object} props.additionalFields addtional fields values
 * @param {string} props.email            email if no user is logged in
 */
const postSignup = ({ eventId, additionalFields = null, email = null }) => (
  dispatch,
  getState
) => {
  const { auth: { session, isLoggedIn } = {} } = getState()

  console.log(isLoggedIn)

  return dispatch({
    types: [
      { type: EVENTS_SIGNUP_PENDING, eventId },
      {
        type: isLoggedIn ? EVENTS_SIGNUP_SUCCESS : EVENTS_SIGNUP_EMAIL_SUCCESS,
        eventId,
      },
      { type: EVENTS_SIGNUP_ERROR, eventId },
    ],
    resource: 'eventsignups',
    method: 'POST',
    data: {
      event: eventId,
      user: session ? session.user : undefined,
      additional_fields: additionalFields
        ? JSON.stringify(additionalFields)
        : undefined,
      email: email || undefined,
    },
    dataType: 'application/json',
  })
}

/**
 * Patch signup data for a specific signup.
 *
 * @param {string} props.id               signup id
 * @param {string} props.etag             etag value for the signup item
 * @param {string} props.eventId          event id
 * @param {object} props.additionalFields additional fields values
 */
const patchSignup = ({ id, etag, eventId, additionalFields = null }) => ({
  types: [
    { type: EVENTS_SIGNUP_PENDING, eventId },
    { type: EVENTS_SIGNUP_SUCCESS, eventId },
    { type: EVENTS_SIGNUP_ERROR, eventId },
  ],
  resource: 'eventsignups',
  itemId: id,
  method: 'PATCH',
  etag,
  data: {
    additional_fields: additionalFields
      ? JSON.stringify(additionalFields)
      : undefined,
  },
  dataType: 'application/json',
})

/**
 * Delete a specific signup.
 * @param {string} props.id    signup id
 * @param {string} props.event event id
 * @param {string} props.etag  etag value for the signup item
 */
const deleteSignup = ({ id, eventId, etag }) => ({
  types: [
    { type: EVENTS_SIGNUP_PENDING, eventId },
    { type: EVENTS_SIGNUP_DELETED, eventId },
    { type: EVENTS_SIGNUP_ERROR, eventId },
  ],
  resource: 'eventsignups',
  itemId: id,
  method: 'DELETE',
  etag,
  dataType: 'application/json',
})

// Actions in addition to the common resource actions!
export {
  setQueryFromFilterValues,
  loadSignupForEvent,
  postSignup,
  patchSignup,
  deleteSignup,
}
