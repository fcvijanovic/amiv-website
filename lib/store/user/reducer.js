import { USER_PENDING, USER_SUCCESS, USER_ERROR } from './constants'
import { AUTH_LOGIN_SUCCESS, AUTH_LOGOUT_SUCCESS } from '../auth/constants'

const initialState = {
  data: null,
  isPending: false,
  action: null,
  error: null,
}

// Reducer performing the actions and updating the state object.
const reducer = (state, action) => {
  // eslint-disable-next-line no-param-reassign
  state = state || initialState

  switch (action.type) {
    case USER_PENDING:
      return {
        ...state,
        isPending: true,
        action: action.action,
        error: null,
      }
    case USER_SUCCESS:
      return {
        data: action.data,
        isPending: false,
        action: null,
        error: null,
      }
    case USER_ERROR:
      return {
        ...state,
        isPending: false,
        error: action.error,
      }
    case AUTH_LOGOUT_SUCCESS:
      return {
        ...state,
        data: null,
        isPending: false,
        action: null,
        error: null,
      }
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        data: action.data.user,
        isPending: false,
        action: null,
        error: null,
      }
    default:
      return state
  }
}

export default reducer
