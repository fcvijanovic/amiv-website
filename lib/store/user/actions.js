import axios from 'axios'

import { getPublicConfig } from 'lib/utils/config'
import {
  USER_PENDING,
  USER_SUCCESS,
  USER_ERROR,
  USER_ACTION_NEWSLETTER,
  USER_ACTION_RFID,
  USER_ACTION_PASSWORD,
} from './constants'

/**
 * Toggle newsletter subscription
 */
const toggleNewsletterSubscription = () => (dispatch, getState) => {
  const user = getState().user.data

  return dispatch({
    types: [
      { type: USER_PENDING, action: USER_ACTION_NEWSLETTER },
      USER_SUCCESS,
      USER_ERROR,
    ],
    resource: 'users',
    itemId: user._id,
    etag: user._etag,
    method: 'PATCH',
    data: { send_newsletter: !user.send_newsletter },
  })
}

/**
 * Set the rfid
 *
 * @param {string|null} rfid rfid number (6 digits)
 */
const updateRfid = rfid => (dispatch, getState) => {
  const user = getState().user.data

  return dispatch({
    types: [
      { type: USER_PENDING, action: USER_ACTION_RFID },
      USER_SUCCESS,
      USER_ERROR,
    ],
    resource: 'users',
    itemId: user._id,
    etag: user._etag,
    method: 'PATCH',
    data: { rfid: rfid || null },
  })
}

/**
 * Change the amiv password
 *
 * @param {string} currentPassword a currently valid password (ETH password or amiv Password)
 * @param {string|null} newPassword new amiv password
 */
const changePassword = (currentPassword, newPassword) => (
  dispatch,
  getState
) => {
  const { apiUrl } = getPublicConfig()
  const user = getState().user.data

  dispatch({ type: USER_PENDING, action: USER_ACTION_PASSWORD })
  // The API only allows to change the password with a fresh session,
  // so we create a temporary new session.
  return axios
    .post(`${apiUrl}/sessions`, {
      username: user.nethz || user._id,
      password: currentPassword,
    })
    .then(response => {
      const session = response.data

      return dispatch({
        types: [null, USER_SUCCESS, USER_ERROR],
        resource: 'users',
        itemId: user._id,
        etag: user._etag,
        method: 'PATCH',
        data: { password: newPassword || null },
        token: session.token,
      })
    })
    .catch(error => {
      return dispatch({ type: USER_ERROR, error })
    })
}

export { toggleNewsletterSubscription, updateRfid, changePassword }
