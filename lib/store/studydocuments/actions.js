import {
  STUDYDOCUMENTS,
  STUDYDOCUMENTS_EDITFORM_PENDING,
  STUDYDOCUMENTS_EDITFORM_SUCCESS,
  STUDYDOCUMENTS_EDITFORM_ERROR,
  STUDYDOCUMENTS_QUICKFILTER_PENDING,
  STUDYDOCUMENTS_QUICKFILTER_SUCCESS,
  STUDYDOCUMENTS_QUICKFILTER_ERROR,
  STUDYDOCUMENTS_QUICKFILTER_RESET,
  STUDYDOCUMENTS_QUICKFILTER_SET_FILTER_VALUES,
} from './constants'

import * as baseActions from '../common/actions'
import Query from '../../utils/query'
import { ITEM_PENDING, ITEM_SUCCESS, ITEM_ERROR } from '../common/constants'

const _generateQueryFromFilterValues = values => {
  const query = {}

  Object.keys(values).forEach(key => {
    const value = values[key]

    if (Array.isArray(value) && value.length > 0) {
      query[key] = { $in: value }
    } else if (key === 'search' && value.length > 0) {
      const regex = { $regex: `^(?i).*${value}.*` }
      query.$or = [
        { title: regex },
        { lecture: regex },
        { author: regex },
        { professor: regex },
      ]
    }
  })
  return { where: query }
}

/**
 * Update stored query and clear all depending lists
 */
const setQueryFromFilterValues = () =>
  baseActions.setQueryFromFilterValues(STUDYDOCUMENTS, {
    queryConverter: _generateQueryFromFilterValues,
  })

/**
 * Load all available summary values
 */
const loadEditFormSummary = () => ({
  types: [
    STUDYDOCUMENTS_EDITFORM_PENDING,
    STUDYDOCUMENTS_EDITFORM_SUCCESS,
    STUDYDOCUMENTS_EDITFORM_ERROR,
  ],
  resource: 'studydocuments',
  method: 'GET',
  query: { max_results: 1 },
})

/** Post a new studydocument item */
const postStudydocumentItem = ({ data, files }) => dispatch => {
  // Upload documents
  const form = new FormData()
  for (let i = 0; i < files.length; i += 1) {
    form.append('files', files[i])
  }
  Object.keys(data).forEach(key => {
    if (data[key] && data[key] !== '') {
      form.append(key, data[key])
    }
  })

  return dispatch({
    types: [null, null, null],
    resource: STUDYDOCUMENTS,
    method: 'POST',
    data: form,
    dataType: 'multipart/form-data',
  })
}

/** Patch an existing studydocument item */
const patchStudydocumentItem = ({
  id,
  etag,
  data,
  files = null,
}) => async dispatch => {
  let currentEtag = etag

  // Upload documents
  if (files && files.length > 0) {
    const form = new FormData()
    for (let i = 0; i < files.length; i += 1) {
      form.append('files', files[i])
    }

    const response = await dispatch({
      types: [
        { type: ITEM_PENDING(STUDYDOCUMENTS), itemId: id },
        null,
        { type: ITEM_ERROR(STUDYDOCUMENTS), itemId: id },
      ],
      resource: STUDYDOCUMENTS,
      itemId: id,
      method: 'PATCH',
      etag: currentEtag,
      data: form,
      dataType: 'multipart/form-data',
    })

    currentEtag = response._etag
  }

  return dispatch({
    types: [
      { type: ITEM_PENDING(STUDYDOCUMENTS), itemId: id },
      { type: ITEM_SUCCESS(STUDYDOCUMENTS), itemId: id },
      { type: ITEM_ERROR(STUDYDOCUMENTS), itemId: id },
    ],
    resource: STUDYDOCUMENTS,
    itemId: id,
    method: 'PATCH',
    etag: currentEtag,
    data,
    dataType: 'application/json',
  })
}

/**
 * Load QuickFilter(tm) summary values for further filtering
 */
const loadQuickFilterSummary = () => (dispatch, getState) => {
  const { query } = getState().studydocuments.quickFilter

  return dispatch({
    types: [
      STUDYDOCUMENTS_QUICKFILTER_PENDING,
      STUDYDOCUMENTS_QUICKFILTER_SUCCESS,
      STUDYDOCUMENTS_QUICKFILTER_ERROR,
    ],
    resource: 'studydocuments',
    method: 'GET',
    query: Query.merge({ max_results: 1 }, query),
  })
}

/**
 * Reset state of the QuickFilter(tm)
 */
const resetQuickFilter = () => ({
  type: STUDYDOCUMENTS_QUICKFILTER_RESET,
})

/**
 * Set filter values and query for the QuickFilter(tm)
 * @param {object} values filter values configured with the QuickFilter(tm)
 */
const setQuickFilterValues = (values, next_step = null) => (
  dispatch,
  getState
) => {
  const previousValues = getState().studydocuments.quickFilter.filterValues
  const newValues = { ...previousValues, ...values }
  const query = _generateQueryFromFilterValues(newValues)

  dispatch({
    type: STUDYDOCUMENTS_QUICKFILTER_SET_FILTER_VALUES,
    filterValues: newValues,
    step: next_step,
    query,
  })
  return dispatch(loadQuickFilterSummary())
}

// Actions in addition to the common resource actions!
export {
  setQueryFromFilterValues,
  resetQuickFilter,
  setQuickFilterValues,
  loadQuickFilterSummary,
  loadEditFormSummary,
  postStudydocumentItem,
  patchStudydocumentItem,
}
