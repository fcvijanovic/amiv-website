import {
  GROUPS_MEMBERSHIPS_PENDING,
  GROUPS_MEMBERSHIPS_SUCCESS,
  GROUPS_MEMBERSHIPS_ERROR,
  GROUPS_PUBLIC_PENDING,
  GROUPS_PUBLIC_SUCCESS,
  GROUPS_PUBLIC_ERROR,
  GROUPMEMBERSHIPS_ENROLLED,
  GROUPMEMBERSHIPS_WITHDRAWN,
} from './constants'
import { AUTH_LOGOUT_SUCCESS } from '../auth/constants'

const initialState = {
  memberships: {
    items: [],
    isPending: false,
    error: null,
  },
  publicGroups: {
    items: [],
    isPending: false,
    error: null,
  },
}

const reducer = (state, action) => {
  // eslint-disable-next-line no-param-reassign
  state = state || initialState

  switch (action.type) {
    case GROUPS_MEMBERSHIPS_PENDING:
      return {
        ...state,
        memberships: { ...state.memberships, isPending: true, error: null },
      }
    case GROUPS_MEMBERSHIPS_ERROR:
      return {
        ...state,
        memberships: {
          ...state.memberships,
          isPending: false,
          error: action.error,
        },
      }
    case GROUPS_MEMBERSHIPS_SUCCESS:
      return {
        ...state,
        memberships: {
          ...state.memberships,
          isPending: false,
          error: null,
          items: action.data._items,
        },
      }
    case GROUPS_PUBLIC_PENDING:
      return {
        ...state,
        publicGroups: { ...state.publicGroups, isPending: true, error: null },
      }
    case GROUPS_PUBLIC_ERROR:
      return {
        ...state,
        publicGroups: {
          ...state.publicGroups,
          isPending: false,
          error: action.error,
        },
      }
    case GROUPS_PUBLIC_SUCCESS:
      return {
        ...state,
        publicGroups: {
          ...state.publicGroups,
          isPending: false,
          error: null,
          items: action.data._items,
        },
      }
    case GROUPMEMBERSHIPS_ENROLLED:
      return {
        ...state,
        memberships: {
          ...state.memberships,
          items: [
            ...state.memberships.items,
            { ...action.data, group: action.group },
          ],
        },
      }
    case GROUPMEMBERSHIPS_WITHDRAWN:
      return {
        ...state,
        memberships: {
          ...state.memberships,
          items: state.memberships.items.filter(item => item._id !== action.id),
        },
      }
    case AUTH_LOGOUT_SUCCESS:
      return initialState
    default:
      return state
  }
}

export default reducer
