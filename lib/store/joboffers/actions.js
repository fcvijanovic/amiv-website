import { JOBOFFERS } from './constants'

import * as baseActions from '../common/actions'

const _generateQueryFromFilterValues = values => {
  const query = {}
  Object.keys(values).forEach(key => {
    const value = values[key]

    if (key === 'search' && value.length > 0) {
      const regex = { $regex: `^(?i).*${value}.*` }
      query.$or = [
        { title_en: regex },
        { title_de: regex },
        { description_en: regex },
        { description_de: regex },
        { company: regex },
        { company: regex },
      ]
    }
  })

  return { where: query }
}

/**
 * Update stored query and clear all depending lists
 */
const setQueryFromFilterValues = () =>
  baseActions.setQueryFromFilterValues(JOBOFFERS, {
    queryConverter: _generateQueryFromFilterValues,
  })

// Actions in addition to the common resource actions!
export { setQueryFromFilterValues }
