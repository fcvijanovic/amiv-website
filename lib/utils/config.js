import getConfig from 'next/config'

const getPublicConfig = () => {
  const { publicRuntimeConfig } = getConfig()
  return publicRuntimeConfig
}

export { getPublicConfig }
