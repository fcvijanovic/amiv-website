import { isDevEnvironment } from './index'

/**
 * Print `log message` to the console if dev environment is active.
 *
 * @param {*} message log message to be printed to the console.
 */
export function log(message) {
  if (isDevEnvironment) console.log(message)
}

/**
 * Print `warn message` to the console if dev environment is active.
 * @param {*} message warn message to be printed to the console.
 */
export function warning(message) {
  if (isDevEnvironment) console.warn(message)
}

/**
 * Print `error message` to the console if dev environment is active.
 *
 * @param {*} message error message to be printed to the console.
 */
export function error(message) {
  if (isDevEnvironment) console.error(message)
}
