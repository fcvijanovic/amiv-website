const { marked } = require('marked')
const { runtimeConfig } = require('./config')

const markedRenderer = new marked.Renderer()

module.exports = {
  publicRuntimeConfig: runtimeConfig,
  reactStrictMode: false,
  images: {
    domains: [runtimeConfig.apiUrl.replace(/(^\w+:|^)\/\//, '')],
  },
  i18n: {
    // More details about the `default` locale can be found at
    // https://github.com/vercel/next.js/discussions/18419#discussioncomment-1561577
    locales: ['default', 'en', 'de'],
    defaultLocale: 'default',
    localeDetection: false,
  },
  eslint: {
    dirs: ['pages', 'locales', 'lib', 'content'],
  },
  async rewrites() {
    return [
      {
        source: '/media/:path([a-zA-Z0-9]*)/(.*)',
        destination: `${runtimeConfig.apiUrl}/media/:path*`, // Proxy to Backend
      },
    ]
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.md$/,
      use: [
        {
          loader: 'html-loader',
        },
        {
          loader: 'markdown-loader',
          options: {
            pedantic: true,
            renderer: markedRenderer,
          },
        },
      ],
    })
    config.module.rules.push({
      test: /\.(pdf)$/,
      use: 'file-loader?name=[name].[ext]',
    })
    // config.module.rules.push({
    //   test: /\.(JPG|jpg|png|PNG)$/,
    //   use: 'file-loader?name=[name].[ext]',
    // })

    return config
  },
}
