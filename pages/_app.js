import React from 'react'
import PropTypes from 'prop-types'
import { Provider as ReduxProvider } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { useRouter } from 'next/router'
import { CssBaseline } from '@material-ui/core'

// import all locales through barrel file
import * as locales from 'locales'
import { wrapper } from 'lib/store/createStore'
import { ThemeProvider } from 'lib/context/themeContext'

// eslint-disable-next-line react/prop-types
const MyApp = ({ Component, ...rest }) => {
  const router = useRouter()
  const { store, props } = wrapper.useWrappedStore(rest)
  const { pageProps } = props
  const { locale } = router
  const messages = { ...locales.en, ...locales[locale] }

  // if (locale === 'en') {
  //   locale = 'en-GB'
  // }

  return (
    <ThemeProvider>
      <IntlProvider locale={locale} defaultLocale="en" messages={messages}>
        <ReduxProvider store={store}>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <Component {...pageProps} />
        </ReduxProvider>
      </IntlProvider>
    </ThemeProvider>
  )
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
}

export default MyApp
