import React, { useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Tabs, Tab, Typography } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { TabPanel } from 'amiv-react-components'

import Layout from 'lib/components/layout'
import Link from 'lib/components/general/link'
import Image from 'lib/components/general/staticImage'
import ImageGroup from 'lib/components/board/imageGroup'
import TranslatedContent from 'lib/components/general/translatedContent'
import board_roles from 'content/board/board_roles.json'

const extractName = key => {
  const result = key.match(/\.\/(.*)\.[a-zA-Z]+$/)
  if (result && result.length > 1) {
    return result[1]
  }
  return key
}

const boardMembersData = (() => {
  const data = require.context(
    'content/board/current',
    false,
    /^.\/[0-9]+.*.json$/
  )
  const images = require.context(
    'content/board/current',
    false,
    /^.\/[0-9]+.*.(jpg|JPG|png|PNG)$/
  )

  const getImage = name => {
    const key = images.keys().find(item => extractName(item) === name)
    return key ? images(key) : undefined
  }

  return data.keys().map(item => {
    const name = extractName(item)
    return { ...data(item), name, image: getImage(name) }
  })
})()

const boardImage = (() => {
  const images = require.context(
    'content/board/current',
    true,
    /^.\/board.(jpg|png)$/
  )
  const key = images.keys()[0]
  return key ? images(key) : undefined
})()

const useStyles = makeStyles(
  {
    root: {
      textAlign: 'center',
      paddingTop: '5em',
    },
    alert: {
      margin: '2em auto',
    },
    boardImage: {
      borderRadius: '4px',
      overflow: 'hidden',
      marginBottom: '8em',
    },
    title: {
      textAlign: 'center',
      margin: '1em 0',
    },
    tabs: {
      width: '100%',
    },
    portraitDescription: {
      padding: '1em',
    },
  },
  { name: 'board' }
)

const BoardPage = () => {
  const [values, setValues] = useState({})
  const classes = useStyles()
  const intl = useIntl()

  const handleTabChange = (group, newValue) => {
    const nextValues = { ...values, [group]: newValue }
    setValues(nextValues)
  }

  return (
    <Layout seoProps={{ title: 'board.title' }}>
      <div>
        <Alert className={classes.alert} severity="info">
          <div>
            <FormattedMessage id="board.current.notice" />
            &nbsp;
            <Link href="/board/history">
              <FormattedMessage id="board.current.link" />
            </Link>
          </div>
        </Alert>

        <Typography className={classes.title} variant="h3">
          <FormattedMessage id="board.title" />
        </Typography>

        {boardImage && (
          <div className={classes.boardImage}>
            <Image
              src={boardImage}
              alt={intl.formatMessage({ id: 'board.title' })}
              ratioX={3}
              ratioY={2}
              // ratioX={16}
              // ratioY={6.5875}
            />
          </div>
        )}

        <div>
          {boardMembersData.map((item, groupIndex) => {
            const roles = new Set(
              item.portraits.map(portrait =>
                intl.formatMessage({ id: `board.roles.${portrait.role}` })
              )
            )
            const names = new Set(item.portraits.map(portrait => portrait.name))
            const value = values[groupIndex] || 0
            return (
              <ImageGroup
                key={groupIndex}
                title={item.showRoles ? Array.from(roles).join(' & ') : null}
                alt={Array.from(names).join(' & ')}
                image={item.image}
                ratioX={1}
                ratioY={1.336538462}
              >
                <Tabs
                  className={classes.tabs}
                  value={value}
                  variant="fullWidth"
                  onChange={(_, newValue) =>
                    handleTabChange(groupIndex, newValue)
                  }
                >
                  {item.portraits.map((portrait, portraitIndex) => (
                    <Tab key={portraitIndex} value={0} label={portrait.name} />
                  ))}
                </Tabs>
                {item.portraits.map((portrait, portraitIndex) => {
                  const role = board_roles[portrait.role]
                  return (
                    <TabPanel
                      key={portraitIndex}
                      value={value}
                      index={portraitIndex}
                      className={classes.portraitDescription}
                    >
                      <TranslatedContent
                        content={portrait.description}
                        parseMarkdown
                      />
                      {role && (
                        <React.Fragment>
                          <Typography variant="h6">
                            <FormattedMessage id="board.tasks" />
                          </Typography>
                          <TranslatedContent content={role} parseMarkdown />
                        </React.Fragment>
                      )}
                    </TabPanel>
                  )
                })}
              </ImageGroup>
            )
          })}
        </div>
      </div>
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default BoardPage
