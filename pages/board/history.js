import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

import Layout from 'lib/components/layout'
import ImageGroup from 'lib/components/board/imageGroup'
import Link from 'lib/components/general/link'

const extractName = key => {
  const result = key.match(/\.\/(.*)\.[a-zA-Z]+$/)
  if (result && result.length > 1) {
    return result[1]
  }
  return key
}

const boardHistoryData = (() => {
  const data = require.context(
    'content/board/history',
    false,
    /^.\/[0-9]{4}(hs|fs).json$/
  )
  const images = require.context(
    'content/board/history',
    false,
    /^.\/[0-9]+.*.(jpg|JPG|png|PNG)$/
  )

  const getImage = name => {
    const key = images.keys().find(item => extractName(item) === name)
    return key ? images(key) : undefined
  }

  return data
    .keys()
    .reverse()
    .map(item => {
      const name = extractName(item)
      return { ...data(item), name, image: getImage(name) }
    })
})()

const useStyles = makeStyles(
  {
    root: {
      textAlign: 'center',
      paddingTop: '5em',
    },
    alert: {
      margin: '2em auto',
    },
    title: {
      textAlign: 'center',
      margin: '1em 0',
    },
    members: {
      listStyle: 'none',
      padding: 0,
      margin: 0,
      fontSize: '1.3em',
      lineHeight: '1.75em',
    },
    name: {
      fontWeight: 'bold',
    },
    role: {
      fontStyle: 'italic',
      margin: '.25em',
    },
  },
  { name: 'boardHistory' }
)

const BoardHistoryPage = () => {
  const classes = useStyles()
  const intl = useIntl()

  return (
    <Layout seoProps={{ title: 'board.old.title' }}>
      <div>
        <Alert className={classes.alert} severity="info">
          <div>
            <FormattedMessage id="board.old.notice" />
            &nbsp;
            <Link href="/board">
              <FormattedMessage id="board.old.link" />
            </Link>
          </div>
        </Alert>

        <Typography className={classes.title} variant="h3">
          <FormattedMessage id="board.old.title" />
        </Typography>

        {boardHistoryData.map(item => {
          const [name, year, semester] = item.name.match(/([0-9]{4})(hs|fs)/)
          const title = `${intl.formatMessage({
            id: `board.old.${semester}`,
          })} ${year}`
          return (
            <ImageGroup
              key={name}
              title={title}
              alt={title}
              image={item.image}
              imageType="fluid"
              ratioX={3}
              ratioY={2}
            >
              <ul className={classes.members}>
                {item.members.map((member, index) => {
                  return (
                    <li key={index}>
                      <span className={classes.name}>{member.name}</span>
                      <span className={classes.role}>
                        (<FormattedMessage id={`board.roles.${member.role}`} />)
                      </span>
                    </li>
                  )
                })}
              </ul>
            </ImageGroup>
          )
        })}
      </div>
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default BoardHistoryPage
