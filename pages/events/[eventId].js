import React from 'react'
import { useRouter } from 'next/router'

import EventsListPage from 'lib/components/events/listPage'
import {
  listLoadAllPages,
  listLoadNextPage,
  loadItem,
} from 'lib/store/common/actions'
import { wrapper } from 'lib/store/createStore'
import { EVENTS } from 'lib/store/events/constants'

const EventDetailsPage = () => {
  const {
    query: { eventId },
  } = useRouter()

  return <EventsListPage eventId={eventId} />
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 600s (10min).
export const getStaticProps = wrapper.getStaticProps(
  store => async ({ params }) => {
    await store.dispatch(
      listLoadAllPages(EVENTS, { listName: 'upcomingWithOpenRegistration' })
    )
    await store.dispatch(
      listLoadAllPages(EVENTS, {
        listName: 'upcomingWithoutOpenRegistration',
      })
    )
    await store.dispatch(listLoadNextPage(EVENTS, { listName: 'past' }))
    if (params.eventId) {
      try {
        await store.dispatch(loadItem(EVENTS, { id: params.eventId }))
      } catch (err) {
        return { notFound: true }
      }
    }

    return { props: {}, revalidate: 600 }
  }
)

export const getStaticPaths = async () => {
  return { paths: [], fallback: 'blocking' }
}

export default EventDetailsPage
