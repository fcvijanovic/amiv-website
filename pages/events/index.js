import React from 'react'

import EventsListPage from 'lib/components/events/listPage'
import { wrapper } from 'lib/store/createStore'
import { EVENTS } from 'lib/store/events/constants'
import { listLoadAllPages, listLoadNextPage } from 'lib/store/common/actions'

const EventsPage = () => {
  return <EventsListPage />
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 600s (10min).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await store.dispatch(
    listLoadAllPages(EVENTS, { listName: 'upcomingWithOpenRegistration' })
  )
  await store.dispatch(
    listLoadAllPages(EVENTS, {
      listName: 'upcomingWithoutOpenRegistration',
    })
  )
  await store.dispatch(listLoadNextPage(EVENTS, { listName: 'past' }))

  return { props: {}, revalidate: 600 }
})

export default EventsPage
