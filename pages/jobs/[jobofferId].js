import React from 'react'
import { useRouter } from 'next/router'

import JobsListPage from 'lib/components/jobs/listPage'
import { wrapper } from 'lib/store/createStore'
import { JOBOFFERS } from 'lib/store/joboffers/constants'
import { listLoadAllPages, loadItem } from 'lib/store/common/actions'

const JobDetailsPage = () => {
  const {
    query: { jobofferId },
  } = useRouter()

  return <JobsListPage jobofferId={jobofferId} />
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 600s (10min).
export const getStaticProps = wrapper.getStaticProps(
  store => async ({ params }) => {
    await store.dispatch(listLoadAllPages(JOBOFFERS, { listName: 'default' }))
    if (params.jobofferId) {
      try {
        await store.dispatch(loadItem(JOBOFFERS, { id: params.jobofferId }))
      } catch (err) {
        return { notFound: true }
      }
    }

    return { props: {}, revalidate: 600 }
  }
)

export const getStaticPaths = async () => {
  return { paths: [], fallback: 'blocking' }
}

export default JobDetailsPage
