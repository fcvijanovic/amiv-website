import React from 'react'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'

import Layout from 'lib/components/layout'
import TeamCard from 'lib/components/teams/teamCard'

const extractName = key => {
  const result = key.match(/\.\/(.*)\.[a-zA-Z]+$/)
  if (result && result.length > 1) {
    return result[1]
  }
  return key
}

const ressortsData = (() => {
  const data = require.context(
    'content/teams/ressorts',
    false,
    /^.\/[0-9]+.*.json$/
  )
  const images = require.context(
    'content/teams/ressorts',
    false,
    /^.\/[0-9]+.*.(jpg|JPG|png|PNG)$/
  )

  const getImage = name => {
    const key = images.keys().find(item => extractName(item) === name)
    return key ? images(key) : undefined
  }

  return data.keys().map(item => {
    const name = extractName(item)
    return { ...data(item), key: name, image: getImage(name) }
  })
})()

const commissionsData = (() => {
  const data = require.context(
    'content/teams/commissions',
    false,
    /^.\/[0-9]+.*.json$/
  )
  const images = require.context(
    'content/teams/commissions',
    false,
    /^.\/[0-9]+.*.(jpg|JPG|png|PNG)$/
  )

  const getImage = name => {
    const key = images.keys().find(item => extractName(item) === name)
    return key ? images(key) : undefined
  }

  return data.keys().map(item => {
    const name = extractName(item)
    return { ...data(item), image: getImage(name) }
  })
})()

const useStyles = makeStyles(
  theme => ({
    root: {
      textAlign: 'center',
      paddingTop: '5em',
    },
    title: {
      textAlign: 'center',
      margin: '1em 0',
    },
    teams: {
      display: 'grid',
      gridTemplateColumns: '1fr 1fr 1fr',
      gridGap: '32px',
      textAlign: 'center',
      paddingBottom: '3em',
      [theme.breakpoints.down('md')]: {
        gridTemplateColumns: '1fr 1fr',
      },
      [theme.breakpoints.down('xs')]: {
        gridTemplateColumns: '1fr',
      },
    },
  }),
  { name: 'teams' }
)

const TeamsPage = () => {
  const classes = useStyles()

  return (
    <Layout seoProps={{ title: 'teams.title' }}>
      <div>
        <Typography className={classes.title} variant="h4">
          <FormattedMessage id="teams.ressorts" />
        </Typography>
        <div className={classes.teams}>
          {ressortsData.map(ressort => (
            <TeamCard key={ressort.fileName} team={ressort} />
          ))}
        </div>
      </div>
      <div>
        <Typography className={classes.title} variant="h4">
          <FormattedMessage id="teams.commissions" />
        </Typography>
        <div className={classes.teams}>
          {commissionsData.map(commission => (
            <TeamCard key={commission.fileName} team={commission} />
          ))}
        </div>
      </div>
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default TeamsPage
