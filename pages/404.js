import React from 'react'

import Error from './_error'

const NotFoundError = () => <Error statusCode={404} />

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default NotFoundError
