import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Layout from 'lib/components/layout'
import UserInfo from 'lib/components/profile/userInfo'
import SessionInfo from 'lib/components/profile/sessionInfo'
import Newsletter from 'lib/components/profile/newsletter'
import Rfid from 'lib/components/profile/rfid'
import Password from 'lib/components/profile/password'
import GroupMemberships from 'lib/components/profile/groupMemberships'
import GroupPublic from 'lib/components/profile/groupPublic'

const useStyles = makeStyles(
  theme => ({
    root: {
      textAlign: 'center',
      paddingTop: '4em',
    },
    profileOption: {
      margin: '1.5em auto',
    },
    groups: {
      display: 'flex',
      flexWrap: 'wrap',
      margin: '4em auto',
    },
    groupList: {
      width: 'calc(50% - .5em)',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        margin: '1em 0',
      },

      '&:first-child': {
        marginRight: '1em',
        [theme.breakpoints.down('sm')]: {
          marginRight: 0,
        },
      },
    },
  }),
  { name: 'profile' }
)

const ProfilePage = () => {
  const classes = useStyles()

  return (
    <Layout
      authenticatedOnly
      className={classes.root}
      seoProps={{ title: 'profile.title' }}
    >
      <UserInfo />
      <SessionInfo />
      <Newsletter className={classes.profileOption} />
      <Rfid className={classes.profileOption} />
      <Password className={classes.profileOption} />
      <div className={classes.groups}>
        <GroupMemberships className={classes.groupList} />
        <GroupPublic className={classes.groupList} />
      </div>
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default ProfilePage
