import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'

import { listLoadNextPage } from 'lib/store/common/actions'
import { EVENTS } from 'lib/store/events/constants'
import { wrapper } from 'lib/store/createStore'
import Layout from 'lib/components/layout'
import Link from 'lib/components/general/link'
import EventPoster from 'lib/components/events/eventPoster'
import { JOBOFFERS } from 'lib/store/joboffers/constants'
import JobsCard from 'lib/components/jobs/card'

const useStyles = makeStyles(
  theme => ({
    root: {
      textAlign: 'center',
      paddingTop: '5em',
      [theme.breakpoints.down('sm')]: {
        paddingTop: '1em',
      },
    },
    ribbon: {
      position: 'absolute',
      padding: 0,
      top: -6,
      right: 0,
      overflow: 'hidden',
      zIndex: 2,
      [theme.breakpoints.down('xs')]: {
        display: 'none',
      },
    },
    ribbonText: {
      position: 'absolute',
      transform: 'rotate(45deg)',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
      top: -35,
      right: -35,
      '& a': {
        padding: '0.5em',
        cursor: 'pointer',
        color: theme.palette.secondary.contrastText,
      },
      '& a:first-child': {
        marginBottom: '0.5em',
      },
      '& a:hover': {
        textDecoration: 'none',
        color: theme.palette.text.secondary,
      },
    },
    title: {
      color: theme.palette.text.primary,
    },
    cardRow: {
      marginBottom: '30px',

      // Grid declaration for sub items, 3 frame columns
      display: 'grid',
      gridTemplateColumns: 'repeat(3, 1fr)',
      gridGap: '30px',

      [theme.breakpoints.down('sm')]: {
        gridTemplateColumns: '1fr',
      },
    },
  }),
  { name: 'frontpage' }
)

const IndexPage = () => {
  const events = useSelector(state => state.events.items)
  const joboffers = useSelector(state => state.joboffers.items)
  const eventsFrontpageUpcoming = useSelector(
    state => state.events.frontpageUpcoming
  )
  const eventsFrontpagePast = useSelector(state => state.events.frontpagePast)
  const joboffersFrontpage = useSelector(state => state.joboffers.frontpage)
  const dispatch = useDispatch()
  const classes = useStyles()
  const theme = useTheme()

  // Note: the second empty array ensures that the effect is called only once after mount.
  useEffect(() => {
    if (
      eventsFrontpageUpcoming.totalPages === 0 &&
      !eventsFrontpageUpcoming.isPending
    ) {
      dispatch(listLoadNextPage(EVENTS, { listName: 'frontpageUpcoming' }))
    }
  }, [])

  useEffect(() => {
    if (
      eventsFrontpagePast.totalPages === 0 &&
      !eventsFrontpagePast.isPending
    ) {
      dispatch(listLoadNextPage(EVENTS, { listName: 'frontpagePast' }))
    }
  }, [])

  useEffect(() => {
    if (joboffersFrontpage.totalPages === 0 && !joboffersFrontpage.isPending) {
      dispatch(listLoadNextPage(JOBOFFERS, { listName: 'frontpage' }))
    }
  }, [])

  let pastEventCount = 0

  if (eventsFrontpageUpcoming.lastPageLoaded > 0) {
    pastEventCount = 3 - (eventsFrontpageUpcoming.items.length % 3)

    if (pastEventCount + eventsFrontpageUpcoming.items.length > 6) {
      pastEventCount %= 3
    }
  }

  return (
    <Layout className={classes.root} seoProps={{ title: 'home.title' }}>
      {/* Ribbon for Hopo pages */}
      <div className={classes.ribbon}>
        <svg height="250" width="250">
          <polygon
            points="75 0, 250 175, 250 111, 139 0"
            fill={theme.palette.secondary.main}
            strokeWidth="0"
          />
          <polygon
            points="0 0, 250 250, 250 185, 65 0"
            fill={theme.palette.common.orange}
            strokeWidth="0"
          />
        </svg>
        <div className={classes.ribbonText}>
          <Link variant="body1" component="a" href="/studies/how-to-ersti">
            <FormattedMessage id="home.ribbon.new" />
          </Link>
          <Link variant="body1" component="a" href="/studies/student-for-a-day">
            <FormattedMessage id="home.ribbon.visit" />
          </Link>
        </div>
      </div>

      {/* Event posters */}
      <Link href="/events">
        <Typography variant="h3" className={classes.title}>
          <FormattedMessage id="events.title" />
        </Typography>
      </Link>
      <div className={classes.cardRow}>
        {eventsFrontpageUpcoming.lastPageLoaded > 0 ? (
          <React.Fragment>
            {eventsFrontpageUpcoming.items.map(eventId => (
              <EventPoster key={eventId} event={events[eventId].data} />
            ))}
            {eventsFrontpagePast.items.map((eventId, index) => {
              if (index >= pastEventCount) return null
              return <EventPoster key={eventId} event={events[eventId].data} />
            })}
          </React.Fragment>
        ) : (
          Array.from(Array(3)).map((_, i) => (
            <EventPoster key={i} event={null} />
          ))
        )}
      </div>

      {/* Job offers */}
      <Link href="/jobs">
        <Typography variant="h3" className={classes.title}>
          <FormattedMessage id="jobs.title" />
        </Typography>
      </Link>
      <div className={classes.cardRow}>
        {joboffersFrontpage.lastPageLoaded > 0
          ? joboffersFrontpage.items.map(jobofferId => (
              <JobsCard
                key={jobofferId}
                joboffer={joboffers[jobofferId].data}
              />
            ))
          : Array.from(Array(3)).map((_, i) => (
              <JobsCard key={i} joboffer={null} />
            ))}
      </div>
    </Layout>
  )
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 600s (10min).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await Promise.allSettled([
    store.dispatch(listLoadNextPage(EVENTS, { listName: 'frontpageUpcoming' })),
    store.dispatch(listLoadNextPage(EVENTS, { listName: 'frontpagePast' })),
    store.dispatch(listLoadNextPage(JOBOFFERS, { listName: 'frontpage' })),
  ])

  return { props: {}, revalidate: 600 }
})

export default IndexPage
