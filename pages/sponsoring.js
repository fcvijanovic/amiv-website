import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

import { Button, Container, Grid } from '@material-ui/core'
import { GetApp } from '@material-ui/icons'

import InfoCard from 'lib/components/sponsoring/infoCard'
import Layout from 'lib/components/layout'
import TranslatedContent from 'lib/components/general/translatedContent'
import content_en from 'content/externalOffers/sponsoring.en.md'
import content_de from 'content/externalOffers/sponsoring.de.md'
import infoJson_en from 'content/externalOffers/infocards.en.json'
import infoJson_de from 'content/externalOffers/infocards.de.json'

const SponsoringPage = () => {
  const intl = useIntl()
  const { locale } = intl
  const infoJson = locale === 'en' ? infoJson_en : infoJson_de

  const pdfLink = '/AMIV_booklet_2023.pdf'

  return (
    <Layout seoProps={{ title: 'sponsoring.title' }}>
      <TranslatedContent
        content={{ en: content_en, de: content_de }}
        noEscape
      />
      <Container style={{ marginBottom: 20 }}>
        <div style={{ textAlign: 'center' }}>
          <Button
            variant="contained"
            component="a"
            href={pdfLink}
            download="AMIV_booklet_2023.pdf"
            endIcon={<GetApp />}
            style={{ marginBottom: 30 }}
          >
            <FormattedMessage id="sponsoring.booklet" />
          </Button>
        </div>
        <Grid container spacing={2}>
          {infoJson.map(infoCard => (
            <InfoCard
              infoTitle={infoCard.infoTitle}
              infoWebsite={infoCard.infoWebsite}
              infoHyperlink={infoCard.infoHyperlink}
              infoContent={infoCard.infoContent}
              infoBonus={infoCard.infoBonus}
              infoContact={infoCard.infoContact}
              infoContactButton={<FormattedMessage id="sponsoring.contact" />}
              key={infoCard.infoTitle}
            />
          ))}
        </Grid>
      </Container>
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default SponsoringPage
