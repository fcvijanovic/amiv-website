import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { FormattedMessage, useIntl } from 'react-intl'

import Layout from '../lib/components/layout'

const useStyles = makeStyles(
  {
    root: {
      textAlign: 'center',
      paddingTop: '5em',
    },
    title: {
      marginBottom: '1em',
    },
    text: {
      fontSize: '1.5em',
    },
  },
  { name: 'errorPage' }
)

const Error = ({ statusCode }) => {
  const classes = useStyles()
  const intl = useIntl()

  const title = intl.formatMessage({
    id: `error.${statusCode}.title`,
    defaultMessage: intl.formatMessage({ id: 'error.title' }),
  })

  return (
    <Layout className={classes.root} seoProps={{ title, noindex: true }}>
      <h1 className={classes.title}>
        <FormattedMessage id="error.title" />
      </h1>
      <p className={classes.text}>
        <FormattedMessage
          id={`error.${statusCode}.text`}
          defaultMessage={intl.formatMessage({ id: 'error.text' })}
        />
      </p>
    </Layout>
  )
}

Error.propTypes = {
  statusCode: PropTypes.number.isRequired,
}

Error.getInitialProps = ({ res, err }) => {
  let statusCode = 404
  if (res) {
    statusCode = res.statusCode
  } else if (err) {
    statusCode = err.statusCode
  }

  return { statusCode }
}

export default Error
