import React, { useEffect, useState } from 'react'
import { makeStyles, useTheme } from '@material-ui/styles'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-scroll'

import Layout from 'lib/components/layout'
import FilteredListLayout from 'lib/components/filteredListPage/layout'
import TranslatedContent from 'lib/components/general/translatedContent'
import { rateLimit } from 'lib/utils'

const data = (() => {
  const rawData = require.context(
    'content/studies/howToErsti',
    false,
    /^.\/[0-9]+.*.json$/
  )

  return rawData.keys().map(item => {
    return rawData(item)
  })
})()

const useStyles = makeStyles(
  theme => ({
    root: {
      textAlign: 'left',
      paddingTop: '2em',
    },
    filteredListLayout: {
      gridTemplateColumns: '30em 1fr',
    },
    tableOfContent: {
      '& span': {
        cursor: 'pointer',
      },
      '& ul': {
        listStyle: 'none',
      },
      '& > ul > li': {
        borderBottom: `1px dashed ${theme.palette.text.secondary}`,
        margin: '1em 0',
        paddingBottom: '1em',
        '&:last-of-type': {
          borderBottom: 'none',
        },
      },
    },
    content: {
      '& table': {
        width: '100%',
        borderCollapse: 'collapse',
        borderSpacing: 0,
      },
      '& table thead': {
        borderBottom: '1px solid black',
      },
      '& table tr:nth-child(even)': {
        background: theme.palette.common.grey,
      },
    },
    tableOfContentLinkItem: {
      color: theme.palette.text.primary,
      textDecoration: 'none',
      cursor: 'pointer',
      '&:hover': {
        color: theme.palette.secondary.main,
      },
    },
    tableOfContentActiveLinkItem: {
      color: theme.palette.secondary.main,
      fontWeigth: 600,
    },
    tableOfContentMainLinkItem: {
      fontWeight: 600,
    },
  }),
  { name: 'how-to-ersti' }
)

const HowToErstiPage = () => {
  const [activeMenuItem, setActiveMenuItem] = useState(undefined)
  const classes = useStyles()
  const theme = useTheme()

  const menuItems = []
  const contentItems = []

  const handleScroll = () => {
    const windowScrollTop =
      document.body.scrollTop || document.documentElement.scrollTop

    const checkActiveState = slug => {
      const element = document.getElementById(slug)

      return (
        element &&
        windowScrollTop < element.offsetTop - element.offsetHeight / 2
      )
    }

    const findActiveItem = items => {
      let newActiveItem

      items.forEach(item => {
        if (newActiveItem) return

        const { slug } = item
        const { subitems } = item

        if (checkActiveState(slug)) {
          newActiveItem = slug
          return
        }
        if (subitems) {
          newActiveItem = findActiveItem(subitems)
        }
      })
      return newActiveItem
    }

    const newActiveItem = findActiveItem(data)
    setActiveMenuItem(newActiveItem)
  }

  const handleScrollRateLimited = rateLimit(handleScroll, 250)

  useEffect(() => {
    window.addEventListener('scroll', handleScrollRateLimited)

    // cleanup this component
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  const createMenuItem = (hierarchyLevel, { title, slug, subitems }) => {
    if (!title) return undefined

    const classNames = [classes.tableOfContentLinkItem]
    if (hierarchyLevel <= 2) {
      classNames.push(classes.tableOfContentMainLinkItem)
    }
    if (activeMenuItem === slug) {
      classNames.push(classes.tableOfContentActiveLinkItem)
    }

    return (
      <li key={slug}>
        <TranslatedContent
          content={title}
          parseMarkdown={false}
          component={Link}
          noEscape
          noHint
          className={classNames.join(' ')}
          to={slug}
          href={`#${slug}`}
          offset={-theme.shape.headerHeight - 120}
          smooth={true}
          duration={500}
        />
        {subitems && subitems.length > 0 && (
          <ul>
            {subitems.map(subitem =>
              createMenuItem(hierarchyLevel + 1, subitem)
            )}
          </ul>
        )}
      </li>
    )
  }

  const createContentItem = (
    hierarchyLevel,
    { content, title, slug, subitems }
  ) => {
    return (
      <React.Fragment>
        {title && (
          <TranslatedContent
            id={slug}
            content={title}
            parseMarkdown={false}
            component={`h${hierarchyLevel}`}
            noHint
          />
        )}
        {content && (
          <TranslatedContent
            content={content}
            parseMarkdown={true}
            component={React.Fragment}
            noEscape
          />
        )}
        {subitems &&
          subitems.map(subitem =>
            createContentItem(hierarchyLevel + 1, subitem)
          )}
      </React.Fragment>
    )
  }

  data.forEach(({ content, title, slug, subitems }) => {
    menuItems.push(createMenuItem(2, { title, slug, subitems }))
    contentItems.push(createContentItem(2, { content, title, slug, subitems }))
  })

  return (
    <Layout
      seoProps={{ title: 'studies.howToErsti.title' }}
      className={classes.root}
    >
      <FilteredListLayout
        className={classes.filteredListLayout}
        showFilterLabel="tableOfContent.show"
        hideFilterLabel="tableOfContent.hide"
      >
        {/* Filter Component */}
        <div className={classes.tableOfContent}>
          <ul>{menuItems}</ul>
        </div>
        {/* Page Content */}
        <div className={classes.content}>
          <h1>
            <FormattedMessage id="studies.howToErsti.title" />
          </h1>
          {contentItems}
        </div>
      </FilteredListLayout>
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default HowToErstiPage
