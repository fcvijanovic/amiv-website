import React from 'react'
import { useIntl } from 'react-intl'
import { makeStyles } from '@material-ui/styles'

import Layout from 'lib/components/layout'
import Image from 'lib/components/general/staticImage'
import TranslatedContent from 'lib/components/general/translatedContent'
import featuredImage from 'content/studies/student-for-a-day.jpg'
import content_de from 'content/studies/student-for-a-day.de.md'

const useStyles = makeStyles(
  theme => ({
    image: {
      marginBottom: '2em',
      height: 225,
      backgroundColor: theme.palette.common.grey,
    },
  }),
  { name: 'studentForADay' }
)

const StudentForADayPage = () => {
  const intl = useIntl()
  const classes = useStyles()

  return (
    <Layout seoProps={{ title: 'mainMenu.studies.studentForADay' }}>
      <TranslatedContent content={{ de: content_de }} noEscape />
      <Image
        className={classes.image}
        layout="responsive"
        src={featuredImage}
        alt={intl.formatMessage({ id: 'mainMenu.studies.studentForADay' })}
        ratioX={10}
        ratioY={3}
      />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default StudentForADayPage
